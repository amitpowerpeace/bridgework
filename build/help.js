/**
 * This file is called by package.json to show list of tasks
 */

const reset = '\x1b[0m';
const bright = '\x1b[1m';
const dim = '\x1b[2m';
const underscore = '\x1b[4m';
const blink = '\x1b[5m';
const reverse = '\x1b[7m';
const hidden = '\x1b[8m';

// Foreground colors
const fgBlack = '\x1b[30m';
const fgRed = '\x1b[31m';
const fgGreen = '\x1b[32m';
const fgYellow = '\x1b[33m';
const fgBlue = '\x1b[34m';
const fgMagenta = '\x1b[35m';
const fgCyan = '\x1b[36m';
const fgWhite = '\x1b[37m';

// Background colours
const bgBlack = '\x1b[40m';
const bgRed = '\x1b[41m';
const bgGreen = '\x1b[42m';
const bgYellow = '\x1b[43m';
const bgBlue = '\x1b[44m';
const bgMagenta = '\x1b[45m';
const bgCyan = '\x1b[46m';
const bgWhite = '\x1b[47m';

console.log(`${fgCyan}

  List of tasks
  -------------
      
      yarn                            download packages
      yarn [option]                   perform specified task
      
      Options:${fgGreen}
        
        start                         start development server
        build                         build project for deployment
        clean                         delete build folder

        docs                          open docs for viewing
        docs:dev                      start docs development server
        docs:build                    build docs 
        docs:start                    start docs from build
        
        test                          run test scripts
        test:watch                    run test scripts in watch mode
        test:coverage                 check test scripts coverage
      
        lint                          check for linting errors
        lint:fix                      fix linting errors
        
        analyze                       visualize size of webpack output files with an interactive zoomable treemap
        scripts                       view list of tasks in package.json
        
        outdated                      list of outdated packages
        ${fgRed}upgrade                       ${blink}!caution${reset}${fgRed} upgrade packages ${reset}
`);
