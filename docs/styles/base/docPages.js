import { family, size, weight, lineH } from 'client/styles/base/font';
import palette from 'client/styles/base/palette';

const globalStyles = {
  root: {
    flexGrow: 1,
  },
  componentName: {
    fontFamily: family.primary,
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    marginBottom: 20,
  },
  componentDescription: {
    fontFamily: family.secondary,
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    marginTop: 5,
    marginBottom: 15,
  },
  componentSubheading: {
    fontFamily: family.secondary,
    fontWeight: weight.medium,
    fontSize: size.display3,
    color: palette.grey.darkest,
    float: 'left',
    marginBottom: 10,
    width: '100%',
  },
  componentLabel: {
    fontFamily: family.secondary,
    fontWeight: weight.medium,
    fontSize: size.body2,
    color: palette.grey.darkest,
    float: 'left',
    marginBottom: 10,
    width: '100%',
  },
  codeSpace: {
    padding: '15px 0px',
    fontFamily: family.snippet,
    fontWeight: weight.semi,
    fontSize: size.body2,
    lineHeight: lineH.code,
  },
  table: {
    minWidth: 700,
  },
  tableHeaderFont: {
    fontFamily: family.secondary,
    fontSize: size.body2,
    fontWeight: weight.bold,
  },
  tabHeaderFont: {
    fontFamily: family.secondary,
    textDecoration: 'underline',
  },
  tableBodyFont: {
    fontFamily: family.secondary,
    fontSize: size.body2,
  },
  copyBtn: {
    float: 'right',
  },
  divFullWidth: {
    width: '100%',
  },
  typography: {
    color: palette.grey.darkest,
  },
};

export default globalStyles;
