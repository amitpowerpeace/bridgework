const template = `
  let field = {
    "name": 'radioboxfield',
    "type": 'radioboxgroup',
    "label": 'Bridges Radiobox',
    "validate": [],
    "spreadType": '',
    "options": [
      {
        "label": 'radiobox 1',
        "value": '1',
      },
      {
        "label": 'radiobox 2',
        "value": '2',
      },
      {
        "label": 'radiobox 3',
        "value": '3',
      },
      {
        "label": 'radiobox 4',
        "value": '4',
      },
    ],
    "errorMessage": '',
    "grid": {
      "row": true,
      "col": 12
    }
  }

  <FormControl fullWidth>
    <label className={classes.customLabel}>
      {'Label'}{' '}
      <sup className={classes.supTextStyle}>*</sup>
    </label>

    <Field
      name={selectboxname}
      component={<BridgesSelect />}
      validate={validate}
      field={field}
    />
  </FormControl>
`;

export default template;
