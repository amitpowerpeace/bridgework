const template = `
  let fields = [{
    "name": 'radioboxfield',
    "type": 'radioboxgroup',
    "label": 'Bridges Radiobox',
    "validate": [],
    "spreadType": '',
    "options": [
      {
        "label": 'radiobox 1',
        "value": '1',
      },
      {
        "label": 'radiobox 2',
        "value": '2',
      },
      {
        "label": 'radiobox 3',
        "value": '3',
      },
      {
        "label": 'radiobox 4',
        "value": '4',
      },
    ],
    "errorMessage": '',
    "grid": {
      "row": true,
      "col": 12
    }
  }]
        
  <BridgesDynamicFields fields={fields} />  
`;

export default template;
