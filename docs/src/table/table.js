/**
 * Radiogroup Field
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import DocTab from 'docs/modules/components/DocTab';
import DocTable from 'docs/modules/components/DocTable';
// COMPONENT DEPENDENCIES
import DataTable from 'client/components/common/BridgesTable/BridgesTable';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
import properties from './properties.json';
import users from 'server/mocks/users.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesTable/BridgesTable'), 'utf8')`;

class BridgesTableField extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Bridges table
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab importFile="BridgesTable">
            <div>
              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <DataTable tableData={users} />
              </Typography>
              <Typography
                noWrap={false}
                variant="title"
                className={classes.componentDescription}
                gutterBottom={false}
                style={{ padding: '20px 15px 20px 15px' }}
              >
                Component creating json properties
              </Typography>
              <DocTable data={properties} />

            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

BridgesTableField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BridgesTableField);
