const template = `
let field = {
  "name": "datepickerfield",
  "type": "datepicker",
  "label": "Datepicker",
  "value": "",
  "validate": [],
  "grid": {
    "row": true,
    "col": 12
  }
}

<FormControl fullWidth>
  <label className={classes.customLabel}>
    {'Label'}{' '}
    <sup className={classes.supTextStyle}>*</sup>
  </label>

  <Field
    name={selectboxname}
    component={<BridgesSelect />}
    validate={validate}
    field={field}
  />
</FormControl>
`;

export default template;
