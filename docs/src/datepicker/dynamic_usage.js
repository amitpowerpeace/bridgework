const template = `
let fields = [{
  "name": "datepickerfield",
  "type": "datepicker",
  "label": "Datepicker",
  "value": "",
  "validate": [],
  "grid": {
    "row": true,
    "col": 12
  }
}]
      
<BridgesDynamicFields fields={fields} />  
`;

export default template;
