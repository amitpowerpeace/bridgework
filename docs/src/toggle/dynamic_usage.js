const template = `
let fields = [{
  "name": "togglefield",
  "type": "togglebox",
  "label": "Toggle Button",
  "required": false,
  "toggleStack": [
    {
      "title": "A",
      "value": "a"
    },
    {
      "title": "B",
      "value": "b"
    }
  ],
  "errorMessage": "",
  "grid": {
    "row": true,
    "col": 12
  }
}]
      
<BridgesDynamicFields fields={fields} />  
`;

export default template;
