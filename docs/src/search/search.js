/**
 * DYNAMIC FORM FIELD
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesSearch from 'client/components/common/BridgesSearch';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import properties from './properties.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesSearch/BridgesSearch'), 'utf8')`;

class BridgesSearchCompNt extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
          BridgesSearch
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab>
            <div>
            <Typography
                variant="title"
                className={classes.tabHeaderFont}
                gutterBottom={false}
                style={{ padding: '60px 0px 20px 15px' }}
              >
              BridgesSearch
              </Typography>

              <Typography component="div" style={{ padding: '20px 15px 5px 15px' }}>
                <BridgesSearch>
                <Typography component="div">
                  This is the BridgesSearch.
                </Typography>
                </BridgesSearch>
              </Typography>
              
              <Typography
                noWrap={false}
                variant="title"
                className={classes.componentDescription}
                gutterBottom={false}
                style={{ padding: '20px 15px 20px 15px' }}
              >
                Component creating json properties
              </Typography>
              <DocTable data={properties} />

              
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

BridgesSearchCompNt.propTypes = {
  classes: PropTypes.object.isRequired,
};



export default withStyles(styles)(BridgesSearchCompNt);
