/**
 *  FORM ACTION BUTTONS
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesFormActions from './BridgesFormActions'; // can't use react's withRouter
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import properties from './properties.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});

const componentImport = preval`module.exports = require('fs').
                              readFileSync(require.resolve('../../../client/components/common/BridgesFormActions/BridgesFormActions'), 'utf8')`;

class FormActionBar extends React.Component {
  actions = [
    {
      title: 'Next',
      type: 'next',
      url: '/fields/action-bar',
      align: 'right',
    },
  ];

  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Form Actions Bar
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab>
            <div>
              <Typography
                component="div"
                style={{ padding: '30px 15px 5px 15px' }}
                className={classes.formActionBarcover}
              >
                <BridgesFormActions actions={this.actions} />
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  Properties
                </Typography>

                <DocTable data={properties} />
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

FormActionBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormActionBar);
