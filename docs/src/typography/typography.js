/**
 * FormactionField
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import properties from './properties.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesTypography/BridgesTypography'), 'utf8')`;

class TypographyDemo extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Bridges Typography
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab importFile="BridgesTypography">
            <div>
              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography variant="headline" className={classes.typography} gutterBottom>
                  Headline
                </Typography>
                <Typography variant="subheading" className={classes.typography} gutterBottom>
                  Subheading
                </Typography>
                <Typography variant="display4" className={classes.typography} gutterBottom>
                  Display 4
                </Typography>
                <Typography variant="display3" className={classes.typography} gutterBottom>
                  Display 3
                </Typography>
                <Typography variant="display2" className={classes.typography} gutterBottom>
                  Display 2
                </Typography>
                <Typography variant="display1" className={classes.typography} gutterBottom>
                  Display 1
                </Typography>
                <Typography variant="body2" className={classes.typography} gutterBottom>
                  Body 2
                </Typography>
                <Typography variant="body1" className={classes.typography} gutterBottom>
                  Body 1
                </Typography>
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  Component properties
                </Typography>

                <DocTable data={properties} />
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

TypographyDemo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TypographyDemo);
