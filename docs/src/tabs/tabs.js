import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesTabs from 'client/components/common/BridgesTabs';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import properties from './properties.json';
import field from './field.json';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
//import field from 'server/mocks/applicationType.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesTabs/BridgesTabs'), 'utf8')`;
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

class BridgesTabsComponent extends React.Component {

   state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
     //console.log("field.tabData" +field.tabData);
     console.log("field===" +field);
    const { value } = this.state;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Bridges Tabs
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab>
            <div>
              <Typography
                component="div"
                style={{ padding: '30px 15px 5px 15px', float: 'left', width: 'auto' }}
                className={classes.formActionBarcover}
              >
              
                  <Tabs value={value} onChange={this.handleChange}>
                    {field && field.map((tabLbl,index) => (
                        <Tab label={tabLbl.tabTitle} />
                    ))}
                  </Tabs>
                {value === 0 && <TabContainer>1171</TabContainer>}
                {value === 1 && <TabContainer>SER</TabContainer>}
                {value === 2 && <TabContainer>CDC</TabContainer>}
              
              
               
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px', clear: 'left' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  Component creating json properties
                </Typography>

                <DocTable data={properties} />
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

BridgesTabsComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BridgesTabsComponent);