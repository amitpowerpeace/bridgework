const template = `
let fields = [{
  "name": "timepickerfield",
  "type": "timepicker",
  "label": "Timepicker",
  "value": "00:00 AM",
  "validate": [],
  "grid": {
    "row": true,
    "col": 12
  }
}]
      
<BridgesDynamicFields fields={fields} />  
`;

export default template;
