const template = `
let field = {
  "name": "timepickerfield",
  "type": "timepicker",
  "label": "Timepicker",
  "value": "00:00 AM",
  "validate": [],
  "grid": {
    "row": true,
    "col": 12
  }
}

<FormControl fullWidth>
  <label className={classes.customLabel}>
    {'Label'}{' '}
    <sup className={classes.supTextStyle}>*</sup>
  </label>

  <Field
    name={selectboxname}
    component={<BridgesSelect />}
    validate={validate}
    field={field}
  />
</FormControl>
`;

export default template;
