/**
 * LOADER
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesLoaderDemo from 'client/components/common/BridgesLoader/BridgesLoader';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
  formActionBarcover: {
    margin: '15px 0px 0px 0px',
    '& > div': {
      position: 'relative',
    },
  },
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesLoader/BridgesLoader'), 'utf8')`;

class BridgesLoader extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Bridges Loader
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab importFile="BridgesLoader">
            <div>
              <Typography
                component="div"
                style={{ float: 'left', width: 'auto' }}
                className={classes.formActionBarcover}
              >
                <BridgesLoaderDemo loader={1} start={() => {}} stop={() => {}} />
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

BridgesLoader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BridgesLoader);
