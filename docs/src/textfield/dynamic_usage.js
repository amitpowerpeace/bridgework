const template = `
let fields = [{
    "name": "textfield",
    "type": "text",
    "label": "test label",
    "value": "value",
    "validate": [],
    "startIcon": "",
    "grid": {
      "row": false,  
      "col": 12
    }
  }]
      
<BridgesDynamicFields fields={fields} />  
`;

export default template;
