const template = `
let field = {
  "name": "textfield",
  "type": "text",
  "label": "test label",
  "value": "value",
  "validate": [],
  "startIcon": "",
  "grid": {
    "row": false,  
    "col": 12
  }
}

<FormControl fullWidth>
  <label className={classes.customLabel}>
    {'Label'}{' '}
    <sup className={classes.supTextStyle}>*</sup>
  </label>

  <Field
    name={selectboxname}
    component={<BridgesSelect />}
    validate={validate}
    field={field}
  />
</FormControl>
`;

export default template;
