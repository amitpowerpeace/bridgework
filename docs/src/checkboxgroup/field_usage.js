const template = `
  let field = {
    "name": 'checkboxfield',
    "type": 'checkboxgroup',
    "label": 'Bridges Checkbox',
    "validate": [],
    "spreadType": '',
    "options": [
      {
        "label": 'checkbox 1',
        "value": '1',
      },
      {
        "label": 'checkbox 2',
        "value": '2',
      },
      {
        "label": 'checkbox 3',
        "value": '3',
      },
      {
        "label": 'checkbox 4',
        "value": '4',
      },
    ],
    "errorMessage": '',
    "grid": {
      "row": true,
      "col": 12
    }
  }

  <FormControl fullWidth>
    <label className={classes.customLabel}>
      {'Label'}{' '}
      <sup className={classes.supTextStyle}>*</sup>
    </label>

    <Field
      name={selectboxname}
      component={<BridgesSelect />}
      validate={validate}
      field={field}
    />
  </FormControl>
`;

export default template;
