const template = `
  let fields = [{
    "name": 'checkboxfield',
    "type": 'checkboxgroup',
    "label": 'Bridges Checkbox',
    "validate": [],
    "spreadType": '',
    "options": [
      {
        "label": 'checkbox 1',
        "value": '1',
      },
      {
        "label": 'checkbox 2',
        "value": '2',
      },
      {
        "label": 'checkbox 3',
        "value": '3',
      },
      {
        "label": 'checkbox 4',
        "value": '4',
      },
    ],
    "errorMessage": '',
    "grid": {
      "row": true,
      "col": 12
    }
  }]
        
  <BridgesDynamicFields fields={fields} />  
`;

export default template;
