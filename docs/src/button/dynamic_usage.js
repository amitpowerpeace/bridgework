const template = `
  let fields = [{
    "name": "",
    "type": "bridgesbutton",
    "title": "Bridges Button",
    "buttonIcon": "",
    "iconAlign": "",
    "buttonType": "squarebtn",
    "buttonPos":"right",
    "grid": {
      "row": false,
      "col": 3
    }
  }]
        
  <BridgesDynamicFields fields={fields} />  
`;

export default template;
