/**
 * RADIOGROUP
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesButton from 'client/components/common/BridgesButton';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import properties from './properties.json';
import dynamicDemo from './dynamic_usage';
import fieldDemo from './field_usage';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesButton/BridgesButton'), 'utf8')`;

class ButtonfieldgroupField extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Button
          </Typography>
        </Grid>
        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab>
            <div>
              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <form fullWidth style={{ marginBottom: 20 }}>
                  <BridgesButton
                    title={'square edge button'}
                    buttonIcon={'keyboard_arrow_right'}
                    buttonType={'squarebtn'}
                    iconAlign={'right'}
                    buttonPos={''}
                    buttonColor={''}
                    field={''}
                  />
                </form>
                <form fullWidth>
                  <BridgesButton
                    title={'round edge button'}
                    buttonIcon={'keyboard_arrow_left'}
                    buttonType={'roundbtn'}
                    iconAlign={'left'}
                    buttonPos={''}
                    buttonColor={''}
                    field={''}
                  />
                </form>
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  Component creating json properties
                </Typography>
                <DocTable data={properties} />
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  <b>With dynamic</b> form component
                </Typography>
                <Grid item xs={12}>
                  <MarkdownElement
                    dir="ltr"
                    className={classes.code}
                    text={`\`\`\`jsx\n${dynamicDemo}\n\`\`\``}
                  />
                </Grid>

                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  <b>Without dynamic</b> form component
                </Typography>
                <Grid item xs={12}>
                  <MarkdownElement
                    dir="ltr"
                    className={classes.code}
                    text={`\`\`\`jsx\n${fieldDemo}\n\`\`\``}
                  />
                </Grid>
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

ButtonfieldgroupField.propTypes = {
  classes: PropTypes.object.isRequired,
};

const ButtonfieldgroupFieldForm = reduxForm({
  form: 'button-field-form',
})(ButtonfieldgroupField);

export default withStyles(styles)(ButtonfieldgroupFieldForm);
