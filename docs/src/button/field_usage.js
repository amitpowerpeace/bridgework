const template = `
  let field = {
    "name": "",
    "type": "bridgesbutton",
    "title": "Bridges Button",
    "buttonIcon": "",
    "iconAlign": "",
    "buttonType": "squarebtn",
    "buttonPos":"right",
    "grid": {
      "row": false,
      "col": 3
    }
  }

  <FormControl fullWidth>
    <label className={classes.customLabel}>
      {'Label'}{' '}
      <sup className={classes.supTextStyle}>*</sup>
    </label>

    <Field
      name={selectboxname}
      component={<BridgesSelect />}
      validate={validate}
      field={field}
    />
  </FormControl>
`;

export default template;
