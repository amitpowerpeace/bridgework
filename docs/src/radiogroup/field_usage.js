const template = `
let field = {
  "name": "togglefield",
  "type": "togglebox",
  "label": "Toggle Button",
  "required": false,
  "toggleStack": [
    {
      "title": "A",
      "value": "a"
    },
    {
      "title": "B",
      "value": "b"
    }
  ],
  "errorMessage": "",
  "grid": {
    "row": true,
    "col": 12
  }
}

<FormControl fullWidth>
  <label className={classes.customLabel}>
    {'Label'}{' '}
    <sup className={classes.supTextStyle}>*</sup>
  </label>

  <Field
    name={selectboxname}
    component={<BridgesSelect />}
    validate={validate}
    field={field}
  />
</FormControl>
`;

export default template;
