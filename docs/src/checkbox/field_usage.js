const template = `
  let field = {
    "name": "checkboxname",
    "type": "singlecheckbox",
    "label": "Checkbox",
    "checked": false,
    "iconColor": "#4AA564",
    "validate": [],
    "grid": {
      "row": true,
      "col": 12
    }
  }

  <FormControl fullWidth>
    <label className={classes.customLabel}>
      {'Label'}{' '}
      <sup className={classes.supTextStyle}>*</sup>
    </label>

    <Field
      name={selectboxname}
      component={<BridgesSelect />}
      validate={validate}
      field={field}
    />
  </FormControl>
`;

export default template;
