const template = `
  let fields = [{
    "name": "checkboxname",
    "type": "singlecheckbox",
    "label": "Checkbox",
    "checked": false,
    "iconColor": "#4AA564",
    "validate": [],
    "grid": {
      "row": true,
      "col": 12
    }
  }]
        
  <BridgesDynamicFields fields={fields} />  
`;

export default template;
