/**
 * FormactionField
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgespaginationDemo from 'client/components/common/BridgesPagination';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import properties from './properties.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesPagination/BridgesPagination'), 'utf8')`;

class BridgesPagination extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Bridges Pagination
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab importFile="BridgesPagination">
            <div>
              <Typography
                component="div"
                style={{ padding: '30px 15px 5px 15px', float: 'left', width: 'auto' }}
                className={classes.formActionBarcover}
              >
                <BridgespaginationDemo
                  page={2}
                  rowsPerPage={20}
                  count={120}
                  onChangePage={this.handleChangePage}
                />
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px', clear: 'left' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  Component creating json properties
                </Typography>

                <DocTable data={properties} />
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

BridgesPagination.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BridgesPagination);
