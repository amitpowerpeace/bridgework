const template = `
let field = {
  "name": "selectboxfield",
  "type": "selectbox",
  "label": "Bridges Selectbox",
  "required": false,
  "options": [
      {
          "label": "option 1",
          "value": "1"
      },
      {
          "label": "option 2",
          "value": "2"
      },
      {
          "label": "option 3",
          "value": "3"
      },
      {
          "label": "option 4",
          "value": "4"
      }
  ],
  "grid": {
    "row": true,
    "col": 12
  }
}

<FormControl fullWidth>
  <label className={classes.customLabel}>
    {'Label'}{' '}
    <sup className={classes.supTextStyle}>*</sup>
  </label>

  <Field
    name={selectboxname}
    component={<BridgesSelect />}
    validate={validate}
    field={field}
  />
</FormControl>
`;

export default template;
