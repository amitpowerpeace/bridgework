const template = `
let fields = [{
  "name": "selectboxfield",
  "type": "selectbox",
  "label": "Bridges Selectbox",
  "required": false,
  "options": [
      {
          "label": "option 1",
          "value": "1"
      },
      {
          "label": "option 2",
          "value": "2"
      },
      {
          "label": "option 3",
          "value": "3"
      },
      {
          "label": "option 4",
          "value": "4"
      }
  ],
  "grid": {
    "row": true,
    "col": 12
  }
}]
      
<BridgesDynamicFields fields={fields} />  
`;

export default template;
