/**
 * Select Field
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { Field, reduxForm } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesSelect from 'client/components/common/BridgesSelect';
import MarkdownElement from 'docs/modules/components/MarkdownElement';
import DocTable from 'docs/modules/components/DocTable';
import DocTab from 'docs/modules/components/DocTab';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
// COMPONENT RELATED DATA
import field from './field.json';
import properties from './properties.json';
import dynamicDemo from './dynamic_usage';
import fieldDemo from './field_usage';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});
const componentImport = preval`module.exports = require('fs').readFileSync(require.resolve('../../../client/components/common/BridgesSelect/BridgesSelect'), 'utf8')`;

class SelectField extends React.Component {
  state = {
    value: 'component',
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <Grid container className={classes.content}>
        <Grid item xs={12}>
          <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
            Group Selectbox
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: 30 }}>
          <DocTab>
            <div>
              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Grid xs={4}>
                  <form>
                    <label className={classes.componentLabel}>
                      {field.label}
                      {field.required ? <sup className={classes.supTextStyle}>*</sup> : null}
                    </label>
                    <Field name={field.name} component={BridgesSelect} field={field} />
                  </form>
                </Grid>
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  Component creating json properties
                </Typography>
                <DocTable data={properties} />
              </Typography>

              <Typography component="div" style={{ padding: '30px 15px 5px 15px' }}>
                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  <b>With dynamic</b> form component
                </Typography>
                <Grid item xs={12}>
                  <MarkdownElement
                    dir="ltr"
                    className={classes.code}
                    text={`\`\`\`jsx\n${dynamicDemo}\n\`\`\``}
                  />
                </Grid>

                <Typography
                  noWrap={false}
                  variant="title"
                  className={classes.componentDescription}
                  gutterBottom={false}
                >
                  <b>Without dynamic</b> form component
                </Typography>
                <Grid item xs={12}>
                  <MarkdownElement
                    dir="ltr"
                    className={classes.code}
                    text={`\`\`\`jsx\n${fieldDemo}\n\`\`\``}
                  />
                </Grid>
              </Typography>
            </div>
            <div>
              <Typography component="div">
                <MarkdownElement
                  dir="ltr"
                  className={classes.code}
                  text={`\`\`\`jsx\n${componentImport}\n\`\`\``}
                />
              </Typography>
            </div>
          </DocTab>
        </Grid>
      </Grid>
    );
  }
}

SelectField.propTypes = {
  classes: PropTypes.object.isRequired,
};

const SelectFieldForm = reduxForm({
  form: 'select-field-form',
})(SelectField);

export default withStyles(styles)(SelectFieldForm);
