import { findPages } from 'docs/modules/utils/find';

const pages = findPages({
  front: true,
});

export default pages;
