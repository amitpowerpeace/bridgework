import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});

class DocTable extends React.Component {
  render() {
    const { classes, data } = this.props;
    // const { data } = this.props.initialProps;
    return (
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {data['head'].map(item => (
              <TableCell key={Math.random()} className={classes.tableHeaderFont}>
                {' '}
                {item}{' '}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data['body'].map((item, i) => (
            <TableRow key={Math.random()}>
              {Object.keys(item).map(o => (
                <TableCell key={Math.random()} className={classes.tableBodyFont}>
                  {item[o]}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  }
}

DocTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
};

export default withStyles(styles)(DocTable);
