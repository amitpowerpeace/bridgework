import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});

class DocTable extends React.Component {
  state = {
    value: 'component',
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { children } = this.props;
    const { value } = this.state;
    return (
      <Grid item xs={10} style={{ marginBottom: 30 }}>
        <AppBar position="static" color="default">
          <Tabs value={value} onChange={this.handleChange}>
            <Tab value="component" label="Component UI" />
            <Tab value="code" label="Component code" />
          </Tabs>
        </AppBar>

        {value === 'component' && children[0]}
        {value === 'code' && children[1]}
      </Grid>
    );
  }
}

DocTable.propTypes = {
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(DocTable);
