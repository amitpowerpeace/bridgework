/**
 * This file "defines" url and title of all parent pages
 * It doesn't participate directly in navigation
 * id is unique identifier for the route
 */
const urls = {
  login: {
    id: 'register-application',
    title: 'Bridges Login',
    url: '/login',
  },
  appInfo: {
    id: 'application-info',
    title: 'Application Info',
    url: '/',
  },
  appReg: {
    id: 'application-register',
    title: 'App Registration',
    url: '/register',
  },
  frontDesk: {
    id: 'front-desk',
    title: 'Front Desk',
    url: '/front-desk',
  },
  scheduling: {
    id: 'scheduling',
    title: 'Scheduling',
    url: '/scheduling',
  },
  dataCollect: {
    id: 'Data Collect',
    title: 'Data Collect',
    url: '/data-collect',
  },
  eligibility: {
    id: 'eligibility',
    title: 'Eligibility',
    url: '/eligibility',
  },
  fapTime: {
    id: 'fap-time',
    title: 'Fap Time',
    url: '/fap-time',
  },
  help: {
    id: 'help',
    title: 'Help',
    url: '/help',
  },
  notFound: {
    id: 'not-found',
    title: '404 not found',
    url: '**',
  },
};
export default urls;
