/**
 * APPLICATION REGISTRATION - FILE CLEARANCE
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import DataTable from 'client/components/common/BridgesTable';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
// UTILITY DEPENDENCIES
import { makeRequest } from 'client/utils/fetchUtils';
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class FileClearance extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      tableData: {
        meta: [],
        data: [],
      },
      btnflg: false,
    };
  }

  componentDidMount() {
    // Set page title
    document.title = paths.fileClearance.title;

    makeRequest('mock/users').then(res => {
      console.log(res);

      this.setState({ tableData: res });
    });
  }

  updateEnableDisableBtn = value => {
    this.setState({
      btnflg: value,
    });
  };

  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.indvInfo.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.contactInfo.url,
      align: 'right',
    },
    {
      title: 'Create New Individual ID',
      type: 'navigate',
      url: paths.contactInfo.url,
      align: 'right',
    },
    {
      title: 'View Associated Cases',
      type: 'navigate',
      url: paths.caseAssociation.url,
      align: 'right',
    },
  ];

  render() {
    const { classes, handleSubmit, field } = this.props;
    const { tableData } = this.state;

    console.log(field);

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              File Clearance
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <DataTable tableData={tableData} updateEnableDisableBtn={this.updateEnableDisableBtn} />
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

FileClearance.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object,
};

FileClearance.defaultProps = {
  field: {
    tableData: {
      meta: [],
      data: [],
    },
  },
};

const FileClearanceForm = reduxForm({
  form: 'file-clearance',
})(FileClearance);

export default withStyles(styles)(FileClearanceForm);
