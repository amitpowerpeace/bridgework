import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import childDevCareReducer from 'client/routes/AppReg/components/ChildDevCare/modules';
import ChildDevCare from '../components';
import { getChildDevCareFields } from '../modules';

// inject reducers
injectAsyncReducers({
  childDevCareReducer,
});

const mapStateToProps = state => ({
  fields: getChildDevCareFields(state),
});

export default connect(mapStateToProps)(ChildDevCare);
