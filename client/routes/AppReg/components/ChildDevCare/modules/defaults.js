export const fields = {
  isSupplementSectionProvided: [
    {
      name: 'supplementSection',
      type: 'singlecheckbox',
      label: 'Supplement section was not provided.',
      hightlight: true,
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  emergencyHousing: [
    {
      name: 'emergencyhousing',
      type: 'togglebox',
      label: 'Does anyone currently live in temporary or emergency housing?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
};
export default fields;
