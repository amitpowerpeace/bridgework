// import { createSelector } from 'reselect';
import { fields } from './defaults';

const childDevCareData = {
  fields,
};

export const getChildDevCareFields = state => state.childDevCareReducer.fields || [];

/**
 * Reducer
 */
export default function childDevCareReducer(state = childDevCareData) {
  return state;
}
