/**
 * APPLICATION REGISTRATION - FODD ASSISTANT FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = () => ({
  ...formStyles,
  ...pageStyles,
});

class ChildDevCare extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.foodAssist.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.finalSummary.url,
      align: 'right',
    },
  ];

  render() {
    const { classes, handleSubmit, fields } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Child Development + Care
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.isSupplementSectionProvided} />

            <BridgesDynamicFields fields={fields.emergencyHousing} />
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

ChildDevCare.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

let ChildDevCareForm = reduxForm({
  form: 'Child-Development-Care',
})(ChildDevCare);

// Decorate with connect to read form values
// https://redux-form.com/7.3.0/examples/selectingformvalues/
const selector = formValueSelector('Child-Development-Care');
ChildDevCareForm = connect(state => {
  const { supplementSection, emergencyHousing } = selector(
    state,
    'supplementSection',
    'emergencyHousing',
  );

  return {
    supplementSection,
    emergencyHousing,
  };
})(ChildDevCareForm);

export default withStyles(styles)(ChildDevCareForm);
