// import { createSelector } from 'reselect';
import { fields } from './defaults';

const additionalGroupDetailsData = {
  fields,
};

export const getAdditionalGroupDetailsFields = state =>
  state.additionalGroupDetailsReducer.fields || [];

/**
 * Reducer
 */
export default function additionalGroupDetailsReducer(state = additionalGroupDetailsData) {
  return state;
}
