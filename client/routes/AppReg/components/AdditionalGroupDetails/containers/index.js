import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import additionalGroupDetailsReducer from 'client/routes/AppReg/components/AdditionalGroupDetails/modules';
import AdditionalGroupDetails from '../components';
import { getAdditionalGroupDetailsFields } from '../modules';

// inject reducers
injectAsyncReducers({
  additionalGroupDetailsReducer,
});

const mapStateToProps = state => ({
  fields: getAdditionalGroupDetailsFields(state),
});

export default connect(mapStateToProps)(AdditionalGroupDetails);
