/**
 *  HEALTH COVERAGE FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import paths from 'client/routes/AppReg/urls';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class AdditionalGroupDetails extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.finalDetails.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.nativeOrigin.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.additionalGroupDetails.title;
  }

  render() {
    const {
      classes,
      handleSubmit,
      fields,
      primaryCaretaker,
      medicalFacility,
      fosterCareForAnyone,
      applyingHealthcareInsurance,
      supplementSection,
    } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Healthcare Coverage
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Additional Group Details
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <BridgesDynamicFields fields={fields.isSupplementSectionProvided} />

            <BridgesDynamicFields fields={fields.primaryCaretaker} />
            <BridgesFormEleCollapse stack={primaryCaretaker} needle={'y'} dividerFlag>
              <BridgesMemberDetails
                members={fields.allHouseholdMembersAbove19years[0]}
                details={fields.childBelow19years}
              />
            </BridgesFormEleCollapse>

            <div className={supplementSection ? classes.subFormContent : classes.subFormEnb}>
              <BridgesDynamicFields fields={fields.medicalFacility} />
              <BridgesFormEleCollapse stack={medicalFacility} needle={'y'} dividerFlag>
                <BridgesMemberDetails members={fields.medicalFacilityOptions[0]} details={''} />
              </BridgesFormEleCollapse>

              <BridgesDynamicFields fields={fields.fosterCare} />
              <BridgesFormEleCollapse stack={fosterCareForAnyone} needle={'y'} dividerFlag>
                <BridgesMemberDetails
                  members={fields.allHouseholdMembersAbove18years}
                  details={''}
                />
              </BridgesFormEleCollapse>

              <BridgesDynamicFields fields={fields.healthcareInsurance} />
              <BridgesFormEleCollapse
                stack={applyingHealthcareInsurance}
                needle={'y'}
                dividerFlag={false}
              >
                <BridgesMemberDetails
                  members={fields.allHouseholdMembersAbove18years}
                  details={''}
                />
              </BridgesFormEleCollapse>
            </div>
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

AdditionalGroupDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

let AdditionalGroupDetailsForm = reduxForm({
  form: 'additional-group-details-form',
})(AdditionalGroupDetails);

const selector = formValueSelector('additional-group-details-form');
AdditionalGroupDetailsForm = connect(state => {
  const {
    primaryCaretaker,
    medicalFacility,
    fosterCareForAnyone,
    applyingHealthcareInsurance,
    supplementSection,
  } = selector(
    state,
    'primaryCaretaker',
    'medicalFacility',
    'fosterCareForAnyone',
    'applyingHealthcareInsurance',
    'supplementSection',
  );
  return {
    primaryCaretaker,
    medicalFacility,
    fosterCareForAnyone,
    applyingHealthcareInsurance,
    supplementSection,
  };
})(AdditionalGroupDetailsForm);

export default withStyles(styles)(AdditionalGroupDetailsForm);
