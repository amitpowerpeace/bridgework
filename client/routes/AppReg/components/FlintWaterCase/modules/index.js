// import { createSelector } from 'reselect';
import { fields } from './defaults';

const flintWaterCaseData = {
  fields,
};

export const getWaterFlintCaseFields = state => state.waterFlintCaseReducer.fields || [];

/**
 * Reducer
 */
export default function waterFlintCaseReducer(state = flintWaterCaseData) {
  return state;
}
