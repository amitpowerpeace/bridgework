import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import waterFlintCaseReducer from 'client/routes/AppReg/components/FlintWaterCase/modules';
import FlintWaterCase from '../components';
import { getWaterFlintCaseFields } from '../modules';

// inject reducers
injectAsyncReducers({
  waterFlintCaseReducer,
});

const mapStateToProps = state => ({
  fields: getWaterFlintCaseFields(state),
});

export default connect(mapStateToProps)(FlintWaterCase);
