/**
 *  HEALTH COVERAGE FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import pageStyles from 'client/styles/common/pages';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class FlintWaterCase extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.nativeOrigin.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.dependentIncome.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.waterFlintCase.title;
  }

  render() {
    const { classes, handleSubmit, fields, waterflintCase } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Healthcare Coverage
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Flint Water System
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <BridgesDynamicFields fields={fields.waterflintCase} />

            <BridgesFormEleCollapse stack={waterflintCase} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails
                  members={fields.people[0]}
                  details={fields.waterflintCaseOptions}
                />
              </div>
            </BridgesFormEleCollapse>
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

FlintWaterCase.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  waterflintCase: PropTypes.any,
};

let FlintWaterCaseForm = reduxForm({
  form: 'water-flint-case',
})(FlintWaterCase);

const selector = formValueSelector('water-flint-case');
FlintWaterCaseForm = connect(state => {
  const { primaryCaretaker, waterflintCase } = selector(
    state,
    'primaryCaretaker',
    'waterflintCase',
  );
  return {
    primaryCaretaker,
    waterflintCase,
  };
})(FlintWaterCaseForm);

export default withStyles(styles)(FlintWaterCaseForm);
