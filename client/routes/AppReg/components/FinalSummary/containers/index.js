import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import FinalSummary from '../components';
import { getFinalSummaryFields } from '../modules';
import finalSummaryReducer from 'client/routes/AppReg/components/FinalSummary/modules';

// inject reducers
injectAsyncReducers({
  finalSummaryReducer,
});

const mapStateToProps = state => ({
  fields: getFinalSummaryFields(state),
});

export default connect(mapStateToProps)(FinalSummary);
