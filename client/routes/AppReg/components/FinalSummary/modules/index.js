// import { createSelector } from 'reselect';
import { fields } from './defaults';

const finalSummaryData = {
  fields,
};

export const getFinalSummaryFields = state => state.finalSummaryReducer.fields || [];

/**
 * Reducer
 */
export default function finalSummaryReducer(state = finalSummaryData) {
  return state;
}
