export const fields = {
  collapseArray: [
    {
      header: 'Member and Program Details',
      fStatus: 'success',
      collType: 'TABLE',
      content: [
        {
          meta: [
            {
              name: 'Name',
              header: 'Name',
              hidden: false,
              type: 'text',
            },
            {
              name: 'Gender',
              header: 'Gender',
              hidden: false,
              type: 'text',
            },
            {
              name: 'Age',
              header: 'Age',
              hidden: false,
              type: 'text',
            },
            {
              name: 'Programs',
              header: 'Programs',
              hidden: false,
              type: 'text',
            },
          ],
          data: [
            {
              Name: 'Leona Ramsey',
              Gender: 'Male',
              Age: '86',
              Programs: 'FAP-CDC',
            },
            {
              Name: 'Leona Ramsey',
              Gender: 'Male',
              Age: '70',
              Programs: 'HCC_SSI',
            },
            {
              Name: 'Leona Ramsey',
              Gender: 'Female',
              Age: '67',
              Programs: 'FAP',
            },
            {
              Name: 'Leona Ramsey',
              Gender: 'Male',
              Age: '65',
              Programs: 'SER',
            },
          ],
        },
      ],
    },

    {
      header: 'Basic Individual Details',
      fStatus: 'success',
      content: [
        {
          name: 'individualid',
          type: 'readonlyField',
          label: 'Indiviual ID',
          value: '12347699',
          validate: [],
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'firstname',
          type: 'readonlyField',
          startIcon: '',
          label: 'First Name',
          value: 'Leona',
          validate: ['required'],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'middlename',
          type: 'readonlyField',
          label: 'Middle Name',
          value: '',
          stringOnly: false,
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'lastname',
          type: 'readonlyField',
          label: 'Last Name',
          value: 'Ramsay',
          stringOnly: false,
          validate: ['required'],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'Suffix',
          type: 'readonlyField',
          label: 'Suffix',
          value: 'Sr.',
          stringOnly: false,
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'dob',
          type: 'readonlyField',
          startIcon: '&#36;',
          label: 'Date of Birth',
          value: '04-17-1979',
          validate: ['required'],
          stringOnly: false,
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'snn',
          type: 'readonlyField',
          label: 'SSN',
          value: '234-23-2345',
          validate: ['required'],
          stringOnly: false,
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'sex',
          type: 'readonlyField',
          label: 'Sex',
          value: 'M',
          validate: [],
          stringOnly: false,
          grid: {
            row: true,
            col: 12,
          },
        },

        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'subHeading',
          label: 'Application Information',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'typeofapplication',
          type: 'readonlyField',
          label: 'Type of Application',
          value: '1171',
          validate: [],
          stringOnly: false,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'daterecived',
          type: 'readonlyField',
          label: 'Date Received',
          value: '01/01/2018',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'timerecived',
          type: 'readonlyField',
          label: 'Time Received',
          value: '10:00 AM',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'writelanguage',
          type: 'readonlyField',
          label: 'Write Language',
          value: 'English',
          validate: [],
          stringOnly: false,
          grid: {
            row: true,
            col: 2,
          },
        },
        {
          name: 'spokenlanguage',
          type: 'readonlyField',
          label: 'Spoken Language',
          value: 'English',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'interpreter',
          type: 'readonlyField',
          label: 'Does anyone need communication assistance?',
          value: 'No',
          validate: [],
          stringOnly: false,
          grid: {
            row: true,
            col: 12,
          },
        },

        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'subHeading',
          label: 'Contact Information',
          validate: [],
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: '',
          type: 'readonlyField',
          label: 'Homeless?',
          value: 'No',
          validate: [],
          stringOnly: false,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'addr',
          type: 'readonlyField',
          label: 'Household Street Address',
          value: '70 Marvin Terra',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'aptlot',
          type: 'readonlyField',
          label: 'Apartmentt/Lot#',
          value: 'T',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 6,
          },
        },

        {
          name: 'state',
          type: 'readonlyField',
          label: 'State',
          value: 'MI',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'zipcode',
          type: 'readonlyField',
          label: 'Zipcode',
          value: '23456',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'city',
          type: 'readonlyField',
          label: 'City',
          value: 'Lansing',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'county',
          type: 'readonlyField',
          label: 'County',
          value: 'Ingham',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 6,
          },
        },

        {
          name: 'cellphonenumber',
          type: 'readonlyField',
          label: 'Cell Phone Number',
          value: '(923)232-2323',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'homephonenumber',
          type: 'readonlyField',
          label: 'Home Phone Number',
          value: '(923)232-2323',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'TTY',
          type: 'readonlyField',
          label: 'TTY',
          value: '(923)232-2323',
          validate: [],
          stringOnly: false,
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'emailid',
          type: 'readonlyField',
          label: 'Email ID',
          value: 'leonaramsey@gmail.com',
          validate: [],
          stringOnly: false,
          grid: {
            row: true,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'Household Members',
      fStatus: 'error',
      content: [
        {
          name: 'people',
          type: 'radiogroup',
          label: '',
          compact: true,
          options: [
            {
              label: '<Medium>Leona Ramsey</Medium> <br /> Male&nbsp;|&nbsp;42&nbsp;years',
              value: 'Leone Ramsey',
            },
            {
              label: '<Medium>Owen Ramsey</Medium> <br /> Female&nbsp;|&nbsp;36&nbsp;years',
              value: 'Owen Ramsey',
            },
            {
              label: '<Medium>Nathen Ramsey</Medium> <br /> Female&nbsp;|&nbsp;10&nbsp;years',
              value: 'Nathen Ramsey',
            },
            {
              label: '<Medium>Nathen Ramsey</Medium> <br /> Female&nbsp;|&nbsp;13&nbsp;years',
              value: 'Mollie Ramsey',
            },
            {
              label: '<Medium>New Memeber</Medium> <br /> Gender&nbsp;|&nbsp;&nbsp;years',
              value: 'New Memeber',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 9,
          },
        },

        {
          name: 'Individual Id',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Individual Id',
          value: '12347699',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'firstname',
          type: 'readonlyField',
          startIcon: '*',
          label: 'First',
          value: 'Leona Ramsay',
          validate: ['required'],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'middlename',
          type: 'readonlyField',
          startIcon: '',
          label: 'Middle Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'lastname',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Last Name',
          value: 'Ramsey',
          validate: ['required'],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'Suffix',
          type: 'readonlyField',
          label: 'Suffix',
          value: 'Sr.',
          stringOnly: false,
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'sex',
          type: 'readonlyField',
          startIcon: '',
          label: 'Sex',
          value: 'M',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },

        {
          name: 'dob',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Date of Birth',
          value: '04-17-1979',
          validate: ['required'],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'Age',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Age',
          value: '42',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'ssn',
          type: 'readonlyField',
          startIcon: '*',
          label: 'SSN',
          value: '234-23-2345',
          validate: ['required'],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'US Citizen/National',
          type: 'readonlyField',
          startIcon: '*',
          label: 'US Citizen/National',
          value: 'Yes',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'Married',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Married',
          value: 'Yes',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'Ethnicity',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Ethnicity',
          value: 'Non Hispanic/Latino',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'Race',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Race',
          value: 'White',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'Programs applied for',
          type: 'readonlyField',
          startIcon: '*',
          label: 'Programs applied for',
          value: 'Healthcare,Food,Childcare',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'Household Details',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          name: 'firstname',
          type: 'readonlyField',
          startIcon: '',
          label: 'Is anyone prgnent now or were they in the last 3 months ',
          value: 'Yes Owen Ramsay',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'expected',
          type: 'readonlyField',
          startIcon: '',
          label: 'Expected',
          value: '1',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'End/due date',
          type: 'readonlyField',
          startIcon: '',
          label: 'End/Due Date',
          value: '05-07-2021',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'householdvalue',
          type: 'readonlyField',
          startIcon: '',
          label:
            'Does anyone in the household have a disability or physcial/emotional/mental health condition',
          value: 'Yes Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'migrant',
          type: 'readonlyField',
          startIcon: '',
          label:
            'Is anyone currently a migrant/seasonal farmworker, refugee/asylee, victiom of domestic violence, or victim of trafficking?',
          value: 'Yes Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'select type',
          type: 'readonlyField',
          label: 'Select Type',
          value: 'Migrant Frameworker',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'immigration status',
          type: 'readonlyField',
          label: 'Does anyone(who is not a US citizen/national) have qualified immigration status',
          value: 'Yes LeonaRamsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'documenttype',
          type: 'readonlyField',
          label: 'Document Type',
          value: 'Document',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'documentNumber',
          type: 'readonlyField',
          label: 'Document Number',
          value: '124392342',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },

        {
          name: 'dateofusentry',
          type: 'readonlyField',
          label: 'Date Of US Entry',
          value: '08-13-2012',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
      ],
    },

    {
      header: 'Assets',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          name: 'financial accounts',
          type: 'readonlyField',
          startIcon: '',
          label: 'Is there anyone on the application with money or financial accounts?',
          value: 'yes, Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'typesofaccount ',
          type: 'readonlyField',
          startIcon: '',
          label: 'Types of Account',
          value: 'Savings',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'name',
          type: 'readonlyField',
          startIcon: '',
          label: 'Name of Bank/Institution',
          value: 'Bank Of America',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'Amount',
          type: 'readonlyField',
          startIcon: '',
          label: 'Amount',
          value: '$1000',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
      ],
    },
    {
      header: 'Income Details',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          name: 'name',
          type: 'readonlyField',
          startIcon: '',
          label: 'Is anyone employed?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'employeename',
          type: 'readonlyField',
          startIcon: '',
          label: 'Employee name',
          value: 'Company xxxx',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'hoursperweek',
          type: 'readonlyField',
          startIcon: '',
          label: 'Avg Hours per Week',
          value: '45',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'wage/tips',
          type: 'readonlyField',
          startIcon: '',
          label: 'Wage/Tips(Before Tax)',
          value: '$20 per Week',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'name',
          type: 'readonlyField',
          startIcon: '',
          label: 'Is anyone self-employed?',
          value: 'Yes, Owen Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'typeofwork',
          type: 'readonlyField',
          startIcon: '',
          label: 'Type of Work?',
          value: 'Regular',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'monthlyincome',
          type: 'readonlyField',
          startIcon: '',
          label: 'Monthly Income(Before Expenses)',
          value: '$200',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'expenses',
          type: 'readonlyField',
          startIcon: '',
          label: 'Expenses',
          value: '$120',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'addiitional income',
          type: 'readonlyField',
          startIcon: '',
          label: 'Does anyone have additional income?',
          value: 'Yes, Leona Ramsey, Social Security(RSDI)',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'Others',
          type: 'readonlyField',
          startIcon: '',
          label: 'Others',
          value: 'Rental Income',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'amount recieved',
          type: 'readonlyField',
          startIcon: '',
          label: 'Amount Recieved',
          value: '$20 Per Hour',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'Expenses',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          name: 'loan',
          type: 'readonlyField',
          startIcon: '',
          label: 'Does anyone pay student loan interest or tax-deductible expenses?',
          value: 'Yes, Leona Ramsey, Education',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'amount',
          type: 'readonlyField',
          startIcon: '',
          label: 'Amount',
          value: '$2000',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'lastname',
          type: 'readonlyField',
          startIcon: '',
          label: 'How often paid',
          value: 'Monthly',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
      ],
    },
    {
      header: 'Final Details',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          name: 'vote',
          type: 'readonlyField',
          startIcon: '',
          label: 'Would you like help registering to vote at your current address?',
          value: 'Yes, send me a voter registration application',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'Healthcare Coverage',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          type: 'subHeading',
          label: 'Additional Group Details',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'primarycaretaker',
          type: 'readonlyField',
          startIcon: '',
          label: 'Is anyone the primary caretaker for a child (under age of 19) in the home?',
          value: 'Yes',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'caretaker',
          type: 'readonlyField',
          startIcon: '',
          label: 'who is the caretaker?(showing household memebers age 19 or older)',
          value: 'Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'child',
          type: 'readonlyField',
          startIcon: '',
          label: 'Who is the child(showing household members below age)',
          value: 'Mollie Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'Does anyone live in a medical Facility or nursing home',
          type: 'readonlyField',
          startIcon: '',
          label: 'Does anyone live in a medical Facility or nursing home?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'fostercare',
          type: 'readonlyField',
          label: 'Was anyone in foster care when they turned 18?',
          value: 'Yes',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'fostercare',
          type: 'readonlyField',
          label: 'Who?(showing household memebers age 18 or older)',
          value: 'Leona Ramsey, Owen Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'fostercare',
          type: 'readonlyField',
          label:
            'Is anyone applying for health insurance currently incarcerated (detained or jailed)?',
          value: 'Leona Ramsey, Owen Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'fostercare',
          type: 'readonlyField',
          label: 'Who?(showing household memebers age 18 or older)',
          value: 'Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'subHeading',
          label: 'American Indian or Alaska Native',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'native',
          type: 'readonlyField',
          label: 'Is anyone an American Indian or Alaska Native?',
          value: 'Yes,Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'native',
          type: 'readonlyField',
          label: 'If yes, are they a member of federally recognised tribe?',
          value: 'Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'tribe',
          type: 'readonlyField',
          label: 'Tribe',
          value: 'Tribe1',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'service',
          type: 'readonlyField',
          label:
            'Has anyone ever received a service or referral from the Indian Health Service, a tribal health program, or an urban Indian health program?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'subHeading',
          label: 'Flint Water Case',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'service',
          type: 'readonlyField',
          label:
            'Did anyone  consume water from the Flint Water System and live, work, or receive childcare or education at an address that was served by the Flint Water System from April 2014 through present day?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'address',
          type: 'readonlyField',
          label: 'Address Served by Flint Water',
          value: 'Home',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'address',
          type: 'readonlyField',
          label: 'Address',
          value: '70 Marvin Terra',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'address',
          type: 'readonlyField',
          label: 'Apartment',
          value: 'T',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'Lot',
          type: 'readonlyField',
          label: 'Lot',
          value: '100',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'state',
          type: 'readonlyField',
          label: 'State',
          value: 'MI',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'zipcode',
          type: 'readonlyField',
          label: 'Zipcode',
          value: '65332',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'city',
          type: 'readonlyField',
          label: 'City',
          value: 'Flint',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'zipcode',
          type: 'readonlyField',
          label: 'County',
          value: 'Genesse',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'startdate',
          type: 'readonlyField',
          label: 'Start Date',
          value: '04-17-2017',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'startdate',
          type: 'readonlyField',
          label: 'End Date',
          value: '04-17-2021',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'subHeading',
          label: 'Tax Filers, Dependents, Yearly Income',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'federal tax',
          type: 'readonlyField',
          label: 'Does anyone applying plan to file a  federal tax return next year?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'Are they filing jointly ',
          type: 'readonlyField',
          label: 'Are they filing jointly with a spouse',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'Are they filing jointly ',
          type: 'readonlyField',
          label: 'Are they claiming dependents?',
          value: 'Yes, Nathen Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'Are they filing jointly ',
          type: 'readonlyField',
          label: "Will anyone be claimed as a dependent on someone else's return tax?",
          value: 'Yes',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },

        {
          name: 'Are they filing jointly ',
          type: 'readonlyField',
          label: 'Who is the dependent?',
          value: 'Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'Are they filing jointly ',
          type: 'readonlyField',
          label: 'Who is the tax filer?',
          value: 'Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'Are they filing jointly ',
          type: 'readonlyField',
          label: 'How is the dependent related to tax filer?',
          value: 'Father',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },

        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'Does anyones income change from month to month? ',
          type: 'readonlyField',
          label: 'Does anyones income change from month to month?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'estimated income ',
          type: 'readonlyField',
          label: 'Total estimated income this year?',
          value: '$80,000',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'next year ',
          type: 'readonlyField',
          label: 'Total estimated income next year?',
          value: '$90,000',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          type: 'subHeading',
          label: 'Medical Bills',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'medicalbills ',
          type: 'readonlyField',
          label: 'Does anyone need help paying for medical bills from the past 3 months?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'months ',
          type: 'readonlyField',
          label: 'Which three months?',
          value: 'Jan, Feb, Mar',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },

        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },

        {
          type: 'subHeading',
          label: 'Coverage',
          underline: true,
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'coverage ',
          type: 'readonlyField',
          label: 'Is anyone currently enrolled in health coverage(even if not applying)?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'coveragetype ',
          type: 'readonlyField',
          label: 'Type and Name of Coverage',
          value: 'Medicare',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          name: 'policynumber ',
          type: 'readonlyField',
          label: 'Policy Number',
          value: '345354353',
          validate: [],
          grid: {
            row: false,
            col: 6,
          },
        },
        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'howmanyyears ',
          type: 'readonlyField',
          label:
            'To make it easier to determine healthcare eligibility in future years, can IRS data be used for automatic renewals?',
          value: 'Yes',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },

        {
          name: 'howmanyyears ',
          type: 'readonlyField',
          label: 'For how many years',
          value: '1 years',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'Food Assistance Program',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: 'success',
      content: [
        {
          name: 'housing',
          type: 'readonlyField',
          startIcon: '',
          label: 'Does anyone pay for housing  expenses?',
          value: 'Yes, Leona Ramsey',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'amount',
          type: 'readonlyField',
          startIcon: '',
          label: 'Expense type',
          value: 'Mortgage',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'amount',
          type: 'readonlyField',
          startIcon: '',
          label: 'Amount',
          value: '$200',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },
        {
          name: 'lastname',
          type: 'readonlyField',
          startIcon: '',
          label: 'How often paid',
          value: 'Weekly',
          validate: [],
          grid: {
            row: false,
            col: 4,
          },
        },

        {
          type: 'hr',
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'utilities',
          type: 'readonlyField',
          startIcon: '',
          label: 'Does anyone pay for Utilities?',
          value: 'Yes',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
        {
          name: 'Which Utilities',
          type: 'readonlyField',
          startIcon: '',
          label: 'Which Utilities?',
          value: 'Air Conditioning, Heat',
          validate: [],
          grid: {
            row: false,
            col: 12,
          },
        },
      ],
    },
  ],
};
