/**
 * APPLICATION REGISTRATION - FINAL SUMMARY FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesCollapse from 'client/components/common/BridgesCollapse';
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import paths from 'client/routes/urls';
import arPaths from 'client/routes/AppReg/urls';
// THEME STYLES
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import FinalSummaryMemberTable from './FinalSummaryMemberDetails';
import BridgesButton from 'client/components/common/BridgesButton';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  btnMarginBtm: {
    marginBottom: '25px',
  },
  alignBtnSty: {
    float: 'right',
    '& button': {
      marginLeft: '20px',
    },
  },
});

class FinalSummary extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: arPaths.childDevCare.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.appInfo.url,
      align: 'right',
    },
  ];

  constructor(props, context) {
    super(props, context);
    this.state = {
      flgOpCls: true,
    };
  }

  // Set page title
  componentDidMount() {
    document.title = arPaths.finalSummary.title;
  }

  chKOpClos = flgTrueFalse => {
    this.setState({ flgOpCls: !this.state.flgOpCls });
  };

  render() {
    const { handleSubmit, classes, fields } = this.props;
    const flgOpCls = false;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={9}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Final Summary and Review
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography className={classes.applicationReveTxt} gutterBottom={false}>
              <p style={{ fontSize: '16px' }}>
                Application Received on <span style={{ color: 'dodgerblue' }}>: MM-DD-YYYY</span>
              </p>
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.btnMarginBtm}>
            <BridgesButton
              title={this.state.flgOpCls ? 'ExpandAll' : 'CollapseAll'}
              buttonType="squarebtn"
              buttonPos="left"
              iconAlign="right"
              buttonIcon="expand_less"
              buttonWidth="min"
            />
            <div className={classes.alignBtnSty}>
              <BridgesButton
                title="Save"
                iconAlign="left"
                buttonType="squarebtn"
                buttonIcon="get_app"
                buttonWidth="min"
              />
              <BridgesButton
                title="Print"
                iconAlign="left"
                buttonType="squarebtn"
                buttonIcon="print"
                buttonWidth="min"
              />
            </div>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            {fields.collapseArray.map(collapse => (
              <BridgesCollapse cData={collapse} key={Math.random()}>
                {collapse && collapse.collType && collapse.collType === 'TABLE' ? (
                  <FinalSummaryMemberTable tableData={collapse.content[0]} />
                ) : (
                  <BridgesDynamicFields fields={collapse.content} />
                )}
              </BridgesCollapse>
            ))}
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

FinalSummary.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.any,
  handleSubmit: PropTypes.any,
};

const FinalSummaryForm = reduxForm({
  form: 'finalsummary-select-form',
})(FinalSummary);

export default withStyles(styles)(FinalSummaryForm);
