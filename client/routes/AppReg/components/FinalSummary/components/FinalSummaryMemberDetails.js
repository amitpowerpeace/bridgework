/**
 * Final Summary Component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import keyBy from 'lodash/keyBy';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import BridgesButton from 'client/components/common/BridgesButton';

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 3,
    height: '0 !important',
  },
  table: {
    width: '99%',
    minWidth: 500,
  },
  tableHeader: {
    backgroundColor: palette.primary.dark,
  },
  tableHeaderFont: {
    textAlign: 'center',
    color: palette.white.base,
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    fontSize: size.display3,
    lineHeight: lineH.textfield,
    paddingLeft: '45px',
  },
  tableHeaderColor: {
    color: `${palette.white.base}!important`,
  },
  tableBodyFont: {
    textAlign: 'center',
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    fontSize: size.display3,
    lineHeight: lineH.textfield,
    padding: '8px',
    '& span': {
      height: '23px',
    },
  },
  iconColorChecked: {
    color: palette.primary.brand,
  },
  radioLabelUnChecked: {
    color: palette.grey.dark,
  },
  radioLabelChecked: {
    color: palette.primary.brand,
  },
  dataTableRow: {
    borderRight: 'none',
    borderLeft: 'none',
    '&[aria-checked="true"]': {
      outline: 'none',
      borderStyle: 'hidden',
      borderRight: 'none',
      borderLeft: 'none',
      // borderRight: '1px solid ' + palette.primary.brand, borderLeft: '1px solid ' +
      // palette.primary.brand, borderTop: '1px solid ' + palette.primary.brand,
      // borderBottom: '1px solid ' + palette.primary.brand,
      backgroundColor: palette.primary.lighter,
      color: palette.grey.darker,
      padding: 6,
    },
    '&[aria-checked="true"] > td': {
      color: palette.primary.brand,
    },
  },
  tablePaginateStyle: {
    float: 'right',
  },
  button: {
    margin: theme.spacing.unit,
    marginLeft: 400,
    border: `1px solid${palette.primary.brand}`,
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
  expeditedPara: {
    fontSize: '16px',
    backgroundColor: palette.notification.success,
    textAlign: 'center',
    padding: '10px 0px',
    color: palette.white.base,
  },
  expediatedFAPText: {
    borderStyle: 'solid',
    borderColor: palette.notification.success,
    color: palette.grey.darkest,
    fontWeight: '500',
  },
  expediatedFAP: {
    width: '97px',
    height: '4px',
    position: 'absolute',
    background: palette.primary.lightskyblue,
    borderRadius: '5px',
  },
  spanheaderUnderline: {
    left: '124px',
    bottom: -8,
    width: 85,
    position: 'absolute',
    height: 3,
    background: palette.primary.lightskyblue,
    borderRadius: 5,
    top: '530px',
  },
});

class FinalSummaryMemberTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      meta: props.tableData.meta,
      data: props.tableData.data,
    };
  }

  /**
   * Render table cell
   */
  renderTableCell = (n, item, i, classes, sortedMeta) => {
    const meta = sortedMeta[item];
    if (meta && !meta.hidden) {
      return (
        <TableCell key={i} className={classes.tableBodyFont}>
          {n[item]}
        </TableCell>
      );
    }
  };

  render() {
    const { classes } = this.props;
    const { order } = this.state;

    const data = this.props.tableData.data;
    const meta = this.props.tableData.meta;
    const sortedMeta = keyBy(meta, 'name');

    return (
      <div>
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <Table className={classes.table}>
              <TableHead className={classes.tableHeader}>
                <TableRow>
                  {meta.map((o, i) => {
                    if (!o.hidden) {
                      return (
                        <TableCell
                          key={i}
                          className={classes.tableHeaderFont}
                          active={!!o.header}
                          direction={order}
                        >
                          {o.header}
                        </TableCell>
                      );
                    }
                  })}
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((n, i) => {
                  // const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow hover className={classes.dataTableRow}>
                      {Object.keys(n).map((item, i) =>
                        this.renderTableCell(n, item, i, classes, sortedMeta),
                      )}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Grid>
          <Grid container className={classes.expediatedFAPText}>
            <Grid
              item
              xs={7}
              style={{
                padding: '15px',
              }}
            >
              <Typography variant="display2" className={classes.subHeading}>
                Expediated FAP
                <span className={classes.spanheaderUnderline} />
              </Typography>
            </Grid>
            <Grid
              item
              xs={5}
              style={{
                padding: '15px',
              }}
            >
              <BridgesButton
                title="Edit"
                iconAlign="left"
                buttonType="squarebtn"
                buttonPos="right"
                buttonIcon="edit"
                buttonWidth="min"
              />
            </Grid>

            <Grid item xs={12}>
              <Typography component="p" className={classes.expeditedPara}>
                This application meets expedited FAP criteria
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

FinalSummaryMemberTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableData: PropTypes.object.isRequired,
};

FinalSummaryMemberTable.defaultProps = {
  tableData: {
    data: [],
    meta: [],
  },
};

export default withStyles(styles)(FinalSummaryMemberTable);
