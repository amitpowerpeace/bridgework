/**
 * APPLICATION REGISTRATION - FODD ASSISTANT FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class FoodAssistance extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.coverageFromJobs.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.childDevCare.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.foodAssist.title;
  }

  render() {
    const {
      classes,
      handleSubmit,
      fields,
      paysExpense,
      paysUtility,
      paysUtilityOptions,
      supplementSection,
      paysutilities,
    } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Food Assistance Program
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Housing Expenses and Utilities
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <BridgesDynamicFields fields={fields && fields.supplementSection} />

            <div className={supplementSection ? classes.subFormContent : classes.subFormEnb}>
              <BridgesDynamicFields fields={fields.paysExpense} />
              <BridgesFormEleCollapse stack={paysExpense} needle={'y'} dividerFlag>
                <BridgesDynamicFields fields={fields.paysExpenseOptions} />
              </BridgesFormEleCollapse>

              <BridgesDynamicFields fields={fields.paysUtility} />
              <BridgesFormEleCollapse stack={paysUtility} needle={'y'} dividerFlag>
                <BridgesDynamicFields fields={fields.paysUtilityOptions} />

                <BridgesFormEleCollapse stack={paysutilities} needle={'other'} dividerFlag>
                  <BridgesDynamicFields fields={fields.other} />
                </BridgesFormEleCollapse>

                <BridgesDynamicFields fields={fields.paysExpenseExtraFee} />
              </BridgesFormEleCollapse>
            </div>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

FoodAssistance.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  paysExpense: PropTypes.any,
  paysUtility: PropTypes.any,
};

let FoodAssistanceForm = reduxForm({
  form: 'food-assistance',
})(FoodAssistance);

// Decorate with connect to read form values
// https://redux-form.com/7.3.0/examples/selectingformvalues/
const selector = formValueSelector('food-assistance');
FoodAssistanceForm = connect(state => {
  const {
    paysExpense,
    paysUtility,
    supplementSection,
    paysUtilityOptions,
    paysutilities,
  } = selector(
    state,
    'paysExpense',
    'paysUtility',
    'paysUtilityOptions',
    'supplementSection',
    'paysutilities',
  );

  return {
    paysExpense,
    paysUtility,
    paysUtilityOptions,
    supplementSection,
    paysutilities,
  };
})(FoodAssistanceForm);

export default withStyles(styles)(FoodAssistanceForm);
