import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import foodAssistanceReducer from 'client/routes/AppReg/components/FoodAssistance/modules';
import FoodAssistanceForm from '../components';
import { getFoodAssistanceFields } from '../modules';

// inject reducers
injectAsyncReducers({
  foodAssistanceReducer,
});

const mapStateToProps = state => ({
  fields: getFoodAssistanceFields(state),
});

export default connect(mapStateToProps)(FoodAssistanceForm);
