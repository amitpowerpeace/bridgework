export const fields = {
  subHeadingFoodAssistance: [
    {
      type: 'subHeading',
      label: 'Household Details',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  supplementSection: [
    {
      name: 'supplementSection',
      type: 'singlecheckbox',
      label: 'Supplement section was not provided.',
      hightlight: true,
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  paysExpense: [
    {
      name: 'paysExpense',
      type: 'togglebox',
      label: 'Does anyone pay for housing expenses?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],

  paysUtility: [
    {
      name: 'paysUtility',
      type: 'togglebox',
      label: 'Does anyone pay for utilities?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],

  paysExpenseOptions: [
    {
      name: 'expenseType',
      type: 'selectbox',
      label: 'Expense Type',
      options: [
        {
          label: 'Rent',
          value: 'Rent',
        },
        {
          label: 'Mortgage',
          value: 'Mortgage',
        },
        {
          label: 'Land Contract',
          value: 'Land Contract',
        },
        {
          label: 'Mobile Home Lot Rent',
          value: 'Mobile Home Lot Rent',
        },
        {
          label: 'HomeOwners Insurance',
          value: 'HomeOwners Insurance',
        },
        {
          label: 'Property Tax',
          value: 'Property Tax',
        },
        {
          label: 'Other',
          value: 'Other',
        },
      ],
      errorMessage: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: false,
    },

    {
      name: 'amount',
      type: 'text',
      label: 'Amount',
      placeholder: 'ex:$000000.00',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'paid',
      type: 'selectbox',
      label: 'How Often Paid',
      placeholder: 'Select a frequency',
      options: [
        {
          label: 'Weekly',
          value: 'Weekly',
        },
        {
          label: 'Monthly',
          value: 'Monthly',
        },
        {
          label: 'Yearly',
          value: 'Yearly',
        },
      ],
      errorMessage: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: true,
    },

    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Expense',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  paysUtilityOptions: [
    {
      name: 'paysutilities',
      type: 'checkboxgroup',
      label: '',
      options: [
        {
          label: 'Heat',
          value: 'Heat',
        },

        {
          label: 'Air Conditioning',
          value: 'AirConditioning',
        },
        {
          label: 'Electricity',
          value: 'Electricity',
        },
        {
          label: 'Water/Sewer',
          value: 'Water/Sewer',
        },
        {
          label: 'Trash Pickup',
          value: 'TrashPickup',
        },
        {
          label: 'Phone',
          value: 'phone',
        },
        {
          label: 'Cooking Fuel',
          value: 'CookingFuel',
        },
        {
          label: 'Other',
          value: 'other',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  other: [
    {
      name: 'otherPay',
      type: 'text',
      label: 'Other',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: false,
    },
  ],
  paysExpenseExtraFee: [
    {
      name: 'paysExpenseExtraFee',
      type: 'togglebox',
      label:
        'If utilities are included in rent, does anyone pay an extra fee for air conditioning?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },

    {
      name: 'paysExpenseSER',
      type: 'togglebox',
      label:
        'Has anyone applying for FAP received more than $20 in State Emergency Relief(SER) energy payments or Michigan Energy Assistance Program(MEAP) payments in the last 12 months?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },

    {
      name: 'paysExpenseHHC',
      type: 'togglebox',
      label:
        'Has anyone applying for FAP received more than $20 in Home Heating Credit(HHC) in the last 12 months? ',
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
};
export default fields;
