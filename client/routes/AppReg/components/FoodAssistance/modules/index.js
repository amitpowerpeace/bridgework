// import { createSelector } from 'reselect';
import { fields } from './defaults';

const foodAssistanceData = {
  fields,
};

export const getFoodAssistanceFields = state => state.foodAssistanceReducer.fields || [];

/**
 * Reducer
 */
export default function foodAssistanceReducer(state = foodAssistanceData) {
  return state;
}
