export const fields = {
  whoIsPregnantInlast3months: [
    {
      name: 'whoIsPregnantInlast3months',
      type: 'togglebox',
      label: 'Is anyone pregnant now or were they in the last 3 months?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  healthConditionStatusInfamily: [
    {
      name: 'healthConditionStatusInfamily',
      type: 'togglebox',
      label:
        'Does anyone in the household hava a disability or physical/emotional/mental health condition?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  isCitizen: [
    {
      name: 'isCitizen',
      type: 'togglebox',
      label: 'Does anyone (who is not a US Citizen/National) have qualified immigration status?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: ['required'],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyChildrenWhosParentLivingSeparate: [
    {
      name: 'anyChildrenWhosParentLivingSeparate',
      type: 'togglebox',
      label: 'Do any children (under age 20) have a parent who is living outside he home?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyEnrolledForCollegeOrVacationalSchool: [
    {
      name: 'anyEnrolledForCollegeOrVacationalSchool',
      type: 'togglebox',
      label: 'Is anyone in the household currently enrolled in college/vocational school?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyFosterInFamily: [
    {
      name: 'anyFosterInFamily',
      type: 'togglebox',
      label:
        'Is anyone in the household a foster child, foster parent, adopted child, or non-parent caregiver?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  anyFosterInFamilyOptions: [
    {
      name: 'anybodyFosterParentsInFamily',
      type: 'selectbox',
      label: 'Type',
      options: [
        {
          label: 'Options one',
          value: '1',
        },
        {
          label: 'Options one',
          value: '2',
        },
        {
          label: 'Options three',
          value: '3',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
  ],
  people: [
    {
      name: '',
      type: 'radiogroup',
      label: 'Who?',
      display: 'vertical',
      options: [
        {
          label: 'Owen Ramsay 36F',
          value: 'Owen Ramsay 36F',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: '',
      type: 'radiogroup',
      label: 'Who?',
      display: 'vertical',
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: '',
      type: 'radiogroup',
      label: 'Who?',
      display: 'vertical',
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  pregnantOptions: [
    {
      name: 'enddate',
      type: 'text',
      startIcon: '',
      label: '#Expected',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'end_date',
      type: 'datepicker',
      label: 'End/Due Date',
      value: '01/01/2018',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
  ],
  migrant: [
    {
      name: 'migrant',
      type: 'togglebox',
      label:
        'Is anyone currently a migrant/seasonal farmworker, refugee/asylee, victim of domestic violence, or victim of trafficking?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  specialAccommodationFlag: [
    {
      name: 'specialAccommodationFlag',
      type: 'singlecheckbox',
      label: 'Would you like to flag a special accommodation?',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  migrantOptions: [
    {
      name: 'isMigrant',
      type: 'selectbox',
      label: 'Type',
      options: [
        {
          label: 'Options one',
          value: '1',
        },
        {
          label: 'Options one',
          value: '2',
        },
        {
          label: 'Options three',
          value: '3',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
  ],
  iscitizenOptions: [
    {
      name: 'doctype',
      type: 'selectbox',
      label: 'Docment Type',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'document-number',
      type: 'text',
      startIcon: '',
      label: 'Document Number',
      value: '',
      validate: ['required'],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'date-of-us-entry',
      type: 'datepicker',
      label: 'Date of US Entry',
      value: '22-02-2018',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
  ],
};
