// import { createSelector } from 'reselect';
import { fields } from './defaults';

const householdDetailsData = {
  fields,
};

export const getHouseholdDetailsFields = state => state.householdDetailsReducer.fields || [];

/**
 * Reducer
 */
export default function householdDetailsReducer(state = householdDetailsData) {
  return state;
}
