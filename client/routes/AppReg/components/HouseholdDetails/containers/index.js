import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import householdDetailsReducer from 'client/routes/AppReg/components/HouseholdDetails/modules';
import HouseholdDetails from '../components';
import { getHouseholdDetailsFields } from '../modules';

// inject reducers
injectAsyncReducers({
  householdDetailsReducer,
});

const mapStateToProps = state => ({
  fields: getHouseholdDetailsFields(state),
});

export default connect(mapStateToProps)(HouseholdDetails);
