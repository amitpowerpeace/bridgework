/**
 * APPLICATION REGISTRATION - HOUSEHOLD DETAILS FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class HouseholdDetails extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.houseMember.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.assets.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.houseDetail.title;
  }

  render() {
    const {
      classes,
      handleSubmit,
      fields,
      whoIsPregnantInlast3months,
      healthConditionStatusInfamily,
      migrant,
      isCitizen,
      anyChildrenWhosParentLivingSeparate,
      anyEnrolledForCollegeOrVacationalSchool,
      anyFosterInFamily,
      specialAccommodationFlag,
    } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Household Details
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.whoIsPregnantInlast3months} />
            <BridgesFormEleCollapse stack={whoIsPregnantInlast3months} needle={'y'} dividerFlag>
              <BridgesMemberDetails members={fields.people[0]} details={fields.pregnantOptions} />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.healthConditionStatusInfamily} />
            <BridgesFormEleCollapse stack={healthConditionStatusInfamily} needle={'y'} dividerFlag>
              <BridgesMemberDetails members={fields.people[2]} details={''} />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.anyChildrenWhosParentLivingSeparate} />
            <BridgesFormEleCollapse
              stack={anyChildrenWhosParentLivingSeparate}
              needle={'y'}
              dividerFlag
            >
              <BridgesMemberDetails members={fields.people[2]} details={''} />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.anyEnrolledForCollegeOrVacationalSchool} />
            <BridgesFormEleCollapse
              stack={anyEnrolledForCollegeOrVacationalSchool}
              needle={'y'}
              dividerFlag
            >
              <BridgesMemberDetails members={fields.people[2]} details={''} />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.anyFosterInFamily} />
            <BridgesFormEleCollapse stack={anyFosterInFamily} needle={'y'} dividerFlag>
              <BridgesMemberDetails
                members={fields.people[1]}
                details={fields.anyFosterInFamilyOptions}
              />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.migrant} />
            <BridgesFormEleCollapse stack={migrant} needle={'y'} dividerFlag>
              <BridgesDynamicFields fields={fields.specialAccommodationFlag} />

              <BridgesFormEleCollapse stack={specialAccommodationFlag} dividerFlag={false}>
                <BridgesMemberDetails members={fields.people[1]} details={fields.migrantOptions} />
              </BridgesFormEleCollapse>
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.isCitizen} />
            <BridgesFormEleCollapse stack={isCitizen} needle={'y'}>
              <BridgesMemberDetails members={fields.people[0]} details={fields.iscitizenOptions} />
            </BridgesFormEleCollapse>
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

HouseholdDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  health: PropTypes.any,
  isCitizen: PropTypes.any,
  last3months: PropTypes.any,
  migrant: PropTypes.any,
};

let HouseholdDetailsForm = reduxForm({
  form: 'householddetails-info-form',
})(HouseholdDetails);

const selector = formValueSelector('householddetails-info-form');
HouseholdDetailsForm = connect(state => {
  const {
    whoIsPregnantInlast3months,
    healthConditionStatusInfamily,
    anyChildrenWhosParentLivingSeparate,
    anyEnrolledForCollegeOrVacationalSchool,
    anyFosterInFamily,
    migrant,
    isCitizen,
    specialAccommodationFlag,
  } = selector(
    state,
    'whoIsPregnantInlast3months',
    'healthConditionStatusInfamily',
    'migrant',
    'isCitizen',
    'anyChildrenWhosParentLivingSeparate',
    'anyEnrolledForCollegeOrVacationalSchool',
    'anyFosterInFamily',
    'specialAccommodationFlag',
  );
  return {
    whoIsPregnantInlast3months,
    healthConditionStatusInfamily,
    migrant,
    isCitizen,
    anyChildrenWhosParentLivingSeparate,
    anyEnrolledForCollegeOrVacationalSchool,
    anyFosterInFamily,
    specialAccommodationFlag,
  };
})(HouseholdDetailsForm);

export default withStyles(styles)(HouseholdDetailsForm);
