/**
 * CASE ASSOCIATION FORM
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesRadioGroup from 'client/components/common/BridgesRadioGroup';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesExpand from 'client/components/common/BridgesExpand';
import paths from 'client/routes/AppReg/urls';
import Icon from '@material-ui/core/Icon';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
// UTILITY DEPENDENCIES
import { makeRequest } from 'client/utils/fetchUtils';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { borderRadius, boxShadow } from 'client/styles/base/custom';
import { family, lineH, size, weight } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import Chance from 'chance';

const chance = new Chance();

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  headerUnderlinewidth: {
    left: 0,
    bottom: -8,
    width: 170,
    position: 'absolute',
    height: 5,
    background: palette.primary.light,
    borderRadius: 5,
  },
});

const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => {
  const statusChance = Math.random();
  return {
    _id: chance.guid(),
    caseOrAppNo: ['102120863', '102120866'][
      Math.floor(Math.random() * ['102120863', '102120866'].length)
    ],
    caseStatusOrMode: ['Closed', 'Case Change/Approved'][
      Math.floor(Math.random() * ['Closed', 'Case Change/Approved'].length)
    ],
    householdStatus: ['In', 'Out'][Math.floor(Math.random() * ['In', 'Out'].length)],
    hoh: ['Y', 'N'][Math.floor(Math.random() * ['Y', 'N'].length)],
    programs: ['FAP', 'CDC,MA', 'SER'][Math.floor(Math.random() * ['FAP', 'CDC,MA', 'SER'].length)],
    office: 'Alcona County',
    subComponent: [
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Primary applicant',
        value: 'John Doe',
        validate: [],
        grid: {
          row: true,
          col: 3,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Son',
        value: 'Nathan Doe',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Wife',
        value: 'Sarah Doe',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Daughter',
        value: 'Mollie Doe',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
      {
        type: 'hr',
        grid: {
          row: true,
          col: 12,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Physical Address',
        value: '70 Marvin Terrace',
        validate: [],
        grid: {
          row: true,
          col: 3,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Apt/Lot #',
        value: 'B',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'State',
        value: 'MI',
        validate: [],
        grid: {
          row: false,
          col: 1,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'City',
        value: 'Lansing',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'County',
        value: 'Ingham',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
      {
        name: '',
        type: 'readonlyField',
        startIcon: '',
        label: 'Zip Code',
        value: '43502',
        validate: [],
        grid: {
          row: false,
          col: 2,
        },
      },
    ],
  };
};

function makeData(len = 5553) {
  return range(len).map(d => {
    return {
      ...newPerson(),
      children: range(10).map(newPerson),
    };
  });
}

class CaseAssociation extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.fileClearance.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.contactInfo.url,
      align: 'right',
    },
    {
      title: 'Do Not Associate',
      type: 'navigate',
      url: paths.contactInfo.url,
      align: 'right',
    },
    {
      title: 'Associate to Case',
      type: 'navigate',
      url: paths.caseAssociation.url,
      align: 'right',
    },
  ];

  constructor(props, context) {
    super(props, context);
    this.state = {
      tableData: {
        meta: [],
        data: [],
      },
      expandColumnData: {
        Header: '',
        columns: [
          {
            expander: true,
            width: 65,
            Expander: ({ isExpanded, ...rest }) => (
              <div>
                {isExpanded ? (
                  <Icon className={props.classes.expandIcon}>expand_less</Icon>
                ) : (
                  <Icon className={props.classes.expandIcon}>expand_more</Icon>
                )}
              </div>
            ),
            style: {
              cursor: 'pointer',
              fontSize: 25,
              padding: 0,
              textAlign: 'center',
              userSelect: 'none',
            },
          },
        ],
      },
    };
  }

  componentDidMount() {
    // Set page title
    document.title = paths.caseAssociation.title;
    makeRequest('mock/caseassociation').then(res => {
      const tableData = Object.assign({}, this.state.tableData);
      tableData.meta = res.meta;
      tableData.meta.push(this.state.expandColumnData);
      tableData.data = makeData();
      this.setState({ tableData });
    });
  }

  render() {
    const { classes, handleSubmit } = this.props;
    const { tableData } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Case Association
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Case Associated with Leona Ramsey 42M
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <Grid container spacing={24}>
              <Grid item xs={12}>
                {tableData.data.length > 0 && <BridgesExpand TableData={tableData} />}
              </Grid>
            </Grid>
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

CaseAssociation.propTypes = {
  classes: PropTypes.object.isRequired,
};

const CaseAssociationForm = reduxForm({
  form: 'case-association',
})(CaseAssociation);

export default withStyles(styles)(CaseAssociationForm);
