import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { injectAsyncReducers } from 'client/store';
import assetDetailsReducer from 'client/routes/AppReg/components/AssetDetails/modules';
import AssetDetails from '../components';
import { addAmount, removeAmount, clearAllAmount } from '../modules';

// inject reducers
injectAsyncReducers({
  assetDetails: assetDetailsReducer,
});

export const getAssetDetailData = state => {
  return state.assetDetails || {};
};

const getFields = createSelector([getAssetDetailData], data => data.fields);
const amountStack = createSelector([getAssetDetailData], data => data.amount);

const mapStateToProps = state => ({
  fields: getFields(state),
  amountStack: amountStack(state),
});

const mapDispatchToProps = {
  addAmount,
  removeAmount,
  clearAllAmount,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AssetDetails);
