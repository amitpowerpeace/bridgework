/**
 * APPLICATION REGISTRATION - ASSET FORM
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesButton from 'client/components/common/BridgesButton';
import AddAmount from 'client/components/AppReg/AddAmount';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, font, size, weight, lineH } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  totalAmount: {
    color: palette.grey.darkest,
    fontFamily: family.primary,
    fontSize: size.display3,
    fontWeight: weight.semi,
    lineHeight: lineH.formLabel,
    paddingTop: 13,
    paddingRight: 16,
  },
  addMoreAmountBtn: {
    paddingTop: 20,
  },
});

class AssetDetails extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.houseDetail.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.incomeDetails.url,
      align: 'right',
    },
  ];

  constructor(props, context) {
    super(props, context);
    this.state = {
      assetsRecordList: 1,
      amountStack: [],
    };
  }

  // Set page title
  componentDidMount() {
    document.title = paths.assets.title;
  }

  fetchSum = text => {
    this.props.addAmount(text);
    this.setState({ amountStack: this.props.amountStack });
  };

  clearAmount = rmIndex => {
    this.props.removeAmount(rmIndex);
    this.setState({ amountStack: this.props.amountStack });
  };

  resetSum = () => {
    this.props.clearAllAmount();
    this.setState({ amountStack: this.props.amountStack });
  };

  render() {
    const { classes, handleSubmit, fields, anyasset } = this.props;
    const { amountStack } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Assets
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.moneyAccounts} />

            <BridgesFormEleCollapse stack={anyasset} needle={'y'} dividerFlag={false}>
              <AddAmount
                field={fields}
                amountStack={amountStack}
                onTrack={this.fetchSum}
                onReset={this.resetSum}
                onClearAmount={this.clearAmount}
              />
            </BridgesFormEleCollapse>
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

AssetDetails.propTypes = {
  anyasset: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

AssetDetails.defaultProps = {
  anyasset: 'n',
};

let AssetDetailsForm = reduxForm({
  form: 'assetdetails-info-form',
})(AssetDetails);

const selector = formValueSelector('assetdetails-info-form');
AssetDetailsForm = connect(state => {
  const anyasset = selector(state, 'anyasset');
  return {
    anyasset,
  };
})(AssetDetailsForm);

export default withStyles(styles)(AssetDetailsForm);
