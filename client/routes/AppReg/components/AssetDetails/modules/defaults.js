export const fields = {
  moneyAccounts: [
    {
      name: 'anyasset',
      type: 'togglebox',
      label:
        'Is there anyone on the application with liquid assets such as checking or savings accounts, savings bonds, etc. ?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 8,
      },
    },
  ],
  people: [
    {
      name: 'assetsbyname',
      type: 'checkboxgroup',
      label: 'Who, type of account, name of bank and amount?',
      compact: false,
      display: 'vertical',
      options: [
        {
          label: 'Leone Ramsay',
          value: 'Leone Ramsay',
        },
        {
          label: 'Owen Ramsay',
          value: 'Owen Ramsay',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  assetsRecord: [
    {
      name: 'enterAssetAmount',
      type: 'text',
      startIcon: '$',
      label: 'Amount',
      reset: false,
      value: '',
      validate: [],
      placeholder: 'ex. $000000.00',
      grid: {
        row: true,
        col: 3,
      },
      grouped: false,
      maskType: '',
      numberOnly: true,
    },
  ],
};
