// import { createSelector } from 'reselect';
import { fields } from './defaults';

// ------------------------------------
// Constants
// ------------------------------------
export const ADD_AMOUNT = 'ADD_AMOUNT';
export const REMOVE_AMOUNT = 'REMOVE_AMOUNT';
export const CLEAR_ALL_AMOUNT = 'CLEAR_ALL_AMOUNT';

// ------------------------------------
// Actions
// ------------------------------------
export function addAmount(amount) {
  return {
    type: ADD_AMOUNT,
    payload: amount,
  };
}

export function removeAmount(amountIndex) {
  return {
    type: REMOVE_AMOUNT,
    payload: amountIndex,
  };
}

export function clearAllAmount() {
  return {
    type: CLEAR_ALL_AMOUNT,
  };
}

export const actions = {
  addAmount,
  removeAmount,
  clearAllAmount,
};

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD_AMOUNT]: (state, action) => {
    state.amount.push({
      name: 'assetsRecordEach[]',
      type: 'text',
      startIcon: '$',
      label: 'Amount',
      reset: true,
      value: action.payload,
      validate: [],
      placeholder: 'ex. $000000.00',
      isDisabled: true,
      grid: {
        row: true,
        col: 3,
      },
      grouped: false,
      maskType: '',
      numberOnly: true,
    });
    return state;
  },
  [REMOVE_AMOUNT]: (state, action) => {
    state.amount.splice(action.payload, 1);
    return state;
  },
  [CLEAR_ALL_AMOUNT]: state => {
    state.amount.length = 0;
    return state;
  },
};

const initialState = {
  fields,
  amount: [],
};

/**
 * Reducer
 */
export default function assetDetailsReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
