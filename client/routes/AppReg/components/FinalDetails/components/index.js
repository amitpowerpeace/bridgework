/**
 * APPLICATION REGISTRATION - FINAL DETAILS FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class FinalDetails extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.expense.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.additionalGroupDetails.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.finalDetails.title;
  }

  render() {
    const { classes, handleSubmit, fields, anyoneRepresentingAnotherCase } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Final Details
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.representer} />
            <BridgesDynamicFields fields={fields.anyoneRepresentingAnotherCase} />
            <BridgesFormEleCollapse
              stack={anyoneRepresentingAnotherCase}
              needle={'y'}
              dividerFlag={false}
            >
              <BridgesMemberDetails
                members={fields.people[0]}
                details={fields.representerOptions}
              />
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

FinalDetails.propTypes = {
  anyoneRepresentingAnotherCase: PropTypes.string,
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

let FinalDetailsForm = reduxForm({
  form: 'FinalDetails-info-form',
})(FinalDetails);

const selector = formValueSelector('FinalDetails-info-form');
FinalDetailsForm = connect(state => {
  const anyoneRepresentingAnotherCase = selector(state, 'anyoneRepresentingAnotherCase');
  return {
    anyoneRepresentingAnotherCase,
  };
})(FinalDetailsForm);

export default withStyles(styles)(FinalDetailsForm);
