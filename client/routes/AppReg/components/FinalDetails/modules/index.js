// import { createSelector } from 'reselect';
import { fields } from './defaults';

const getAssetDetailsData = {
  fields,
};

export const getFinalDetailsFields = state => state.finalDetailsReducer.fields || [];

/**
 * Reducer
 */
export default function finalDetailsReducer(state = getAssetDetailsData) {
  return state;
}
