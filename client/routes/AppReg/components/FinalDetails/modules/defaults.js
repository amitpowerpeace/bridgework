export const fields = {
  representer: [
    {
      name: 'anyoneHelpRegisteringVoteCurrAddr',
      type: 'togglebox',
      label:
        'Would anyone in the household like help registering to vote at their current address?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: ['required'],
      grid: {
        row: true,
        col: 8,
      },
    },
  ],
  anyoneRepresentingAnotherCase: [
    {
      name: 'anyoneRepresentingAnotherCase',
      type: 'togglebox',
      label: 'Is someone acting for or representing anyone in this case?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  people: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: true,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 9,
      },
      grouped: true,
    },
  ],
  anythingElse: [
    {
      name: 'anythingElse',
      type: 'singlecheckbox',
      label: 'Anything Else?',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  extraNotes: [
    {
      name: 'extraNotes',
      type: 'singlecheckbox',
      label: 'Were there extra Notes?',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  representerOptions: [
    {
      type: 'subHeading',
      label: 'Authorized Representative Name',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'firstname',
      type: 'text',
      startIcon: '',
      label: 'First Name',
      value: '',
      validate: ['required'],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'middlename',
      type: 'text',
      startIcon: '',
      label: 'Middle Name',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'lastname',
      type: 'text',
      startIcon: '',
      label: 'Last Name',
      value: '',
      validate: ['required'],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'house_street_address',
      type: 'text',
      startIcon: '',
      label: 'Street',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
      grouped: true,
    },
    {
      name: 'city',
      type: 'selectbox',
      label: 'City',
      options: [
        {
          label: 'city one',
          value: '1',
        },
        {
          label: 'city two',
          value: '2',
        },
        {
          label: 'city three',
          value: '3',
        },
        {
          label: 'city four',
          value: '4',
        },
      ],
      validate: ['required'],
      errorMessage: '',
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'state',
      type: 'selectbox',
      label: 'State',
      options: [
        {
          label: 'state one',
          value: '1',
        },
        {
          label: 'state two',
          value: '2',
        },
        {
          label: 'state three',
          value: '3',
        },
        {
          label: 'state four',
          value: '4',
        },
      ],
      validate: ['required'],
      errorMessage: '',
      grid: {
        row: false,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'Zip Code',
      type: 'text',
      startIcon: '',
      label: 'Zipcode',
      value: '',
      maskType: 'zipcode',
      validate: ['required'],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'homephonenumber',
      type: 'text',
      startIcon: '',
      label: 'Phone Number',
      value: '',
      validate: [],
      maskType: 'phone',
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'emailid',
      type: 'text',
      startIcon: ' ',
      label: 'Email ID',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 6,
      },
      grouped: true,
    },
  ],
};
