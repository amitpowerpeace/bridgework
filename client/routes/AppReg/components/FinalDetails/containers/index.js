import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import FinalDetails from '../components';
import { getFinalDetailsFields } from '../modules';
import finalDetailsReducer from 'client/routes/AppReg/components/FinalDetails/modules';

// inject reducers
injectAsyncReducers({
  finalDetailsReducer,
});

const mapStateToProps = state => ({
  fields: getFinalDetailsFields(state),
});

export default connect(mapStateToProps)(FinalDetails);
