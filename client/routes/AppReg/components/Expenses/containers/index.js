import { connect } from 'react-redux';
import Expenses from '../components';
import { getExpensesFields } from '../modules';
import { injectAsyncReducers } from 'client/store';
import expensesReducer from 'client/routes/AppReg/components/Expenses/modules';

// inject reducers
injectAsyncReducers({
  expensesReducer,
});

const mapStateToProps = state => ({
  fields: getExpensesFields(state),
});

export default connect(mapStateToProps)(Expenses);
