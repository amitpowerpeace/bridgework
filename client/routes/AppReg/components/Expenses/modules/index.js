// import { createSelector } from 'reselect';
import { fields } from './defaults';

const expensesData = {
  fields,
};

export const getExpensesFields = state => state.expensesReducer.fields || [];

/**
 * Reducer
 */
export default function expensesReducer(state = expensesData) {
  return state;
}
