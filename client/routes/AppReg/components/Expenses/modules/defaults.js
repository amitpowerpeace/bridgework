export const fields = {
  loanExpenses: [
    {
      name: 'loanExpense',
      type: 'togglebox',
      label: 'Does anyone pay student loan interest or tax deductible expenses?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  people: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: true,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 9,
      },
      grouped: true,
    },
  ],
  expensesOptions: [
    {
      name: 'expenseType',
      type: 'selectbox',
      label: 'Expense Type',
      options: [
        {
          label: 'Student Loan In',
          value: 'Student Loan In',
        },
        {
          label: 'Tax Dedutible Expenses',
          value: 'Tax Dedutible Expenses',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Delete Expense',
      buttonIcon: 'delete',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      buttonColor: 'red',
      buttonDynamic: true,
      grid: {
        row: false,
        col: 7,
      },
    },
    {
      name: 'amount',
      type: 'text',
      startIcon: '',
      label: 'Amount',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'howOftenPaid',
      type: 'selectbox',
      label: 'How Often Paid',
      options: [
        {
          label: 'Day',
          value: '1',
        },
        {
          label: 'Week',
          value: '2',
        },
        {
          label: 'Monthly',
          value: '3',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Expense',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
};
