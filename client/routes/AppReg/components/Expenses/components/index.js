/**
 * APPLICATION REGISTRATION - EXPENSES FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { Values } from 'redux-form-website-template';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';

import MembersArrayForm from 'client/components/common/BridgesMemberDetails/BridgesMemberArray';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class Expenses extends React.Component {
  
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.incomeDetails.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.finalDetails.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.expense.title;
  }

  render() {
    const { classes, handleSubmit, fields, loanExpense } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Expenses
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.loanExpenses} />
            <BridgesFormEleCollapse stack={loanExpense} needle={'y'} dividerFlag={false}>
              
              {/* <BridgesMemberDetails members={fields.people[0]} details={fields.expensesOptions} /> */}

              <MembersArrayForm members={fields.people[0]} details={fields.expensesOptions} />

            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
        
        <Values form="expenses-info-form" />

      </div>
    );
  }
}

Expenses.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  loanExpense: PropTypes.any,
};

let ExpensesForm = reduxForm({
  form: 'expenses-info-form',
})(Expenses);

const selector = formValueSelector('expenses-info-form');
ExpensesForm = connect(state => {
  const loanExpense = selector(state, 'loanExpense');
  return {
    loanExpense,
  };
})(ExpensesForm);

export default withStyles(styles)(ExpensesForm);