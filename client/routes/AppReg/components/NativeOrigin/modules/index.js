// import { createSelector } from 'reselect';
import { fields } from './defaults';

const nativeOfPersonData = {
  fields,
};

export const getNativeOfPersonFields = state => state.nativeOfPersonReducer.fields || [];

/**
 * Reducer
 */
export default function nativeOfPersonReducer(state = nativeOfPersonData) {
  return state;
}
