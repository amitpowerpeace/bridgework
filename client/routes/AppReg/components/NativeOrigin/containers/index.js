import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import nativeOfPersonReducer from 'client/routes/AppReg/components/NativeOrigin/modules';
import NativeOrigin from '../components';
import { getNativeOfPersonFields } from '../modules';

// inject reducers
injectAsyncReducers({
  nativeOfPersonReducer,
});

const mapStateToProps = state => ({
  fields: getNativeOfPersonFields(state),
});

export default connect(mapStateToProps)(NativeOrigin);
