/**
 *  HEALTH COVERAGE FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});
class NativeOrigin extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.additionalGroupDetails.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.waterFlintCase.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.nativeOrigin.title;
  }

  render() {
    const {
      classes,
      handleSubmit,
      fields,
      anyoneEligible,
      anyoneTribe,
      anyoneNative,
      isFederallyRecognizedTribe,
      servicesFromAnyOtherSource,
    } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Healthcare Coverage
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  American Indian or Alaska Native
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <BridgesDynamicFields fields={fields.anyoneNative} />
            <BridgesFormEleCollapse stack={anyoneNative} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails
                  members={fields.people[1]}
                  details={fields.anyoneNativeOptionsLevel1}
                />

                {/* <BridgesFormEleCollapse stack={isFederallyRecognizedTribe} needle={'y'} dividerFlag={false}>

                  <div className={classes.divFullWidth}>
                    <BridgesDynamicFields fields={fields.anyoneNativeOptionsLevel2} />
                  </div>
                </BridgesFormEleCollapse> */}
              </div>
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.anyoneEligible} />

            <BridgesFormEleCollapse stack={anyoneEligible} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails members={fields.people[0]} details={''} />
              </div>
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.servicesFromAnyOtherSource} />

            <BridgesFormEleCollapse stack={servicesFromAnyOtherSource} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails members={fields.people[0]} details={''} />
              </div>
            </BridgesFormEleCollapse>
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

NativeOrigin.propTypes = {
  anyoneEligible: PropTypes.any,
  anyoneNative: PropTypes.any,
  anyoneTribe: PropTypes.any,
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isFederallyRecognizedTribe: PropTypes.any,
  servicesFromAnyOtherSource: PropTypes.any,
};

let NativeOriginForm = reduxForm({
  form: 'native-origin',
})(NativeOrigin);

const selector = formValueSelector('native-origin');
NativeOriginForm = connect(state => {
  const {
    primaryCaretaker,
    anyoneEligible,
    anyoneTribe,
    anyoneNative,
    isFederallyRecognizedTribe,
    servicesFromAnyOtherSource,
  } = selector(
    state,
    'primaryCaretaker',
    'anyoneEligible',
    'anyoneTribe',
    'anyoneNative',
    'isFederallyRecognizedTribe',
    'servicesFromAnyOtherSource',
  );
  return {
    primaryCaretaker,
    anyoneEligible,
    anyoneTribe,
    anyoneNative,
    isFederallyRecognizedTribe,
    servicesFromAnyOtherSource,
  };
})(NativeOriginForm);

export default withStyles(styles)(NativeOriginForm);
