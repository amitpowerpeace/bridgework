import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import medicalBillsReducer from 'client/routes/AppReg/components/MedicalBills/modules';
import MedicalBills from '../components';
import { getMedicalBillsFields } from '../modules';

// inject reducers
injectAsyncReducers({
  medicalBillsReducer,
});

const mapStateToProps = state => ({
  fields: getMedicalBillsFields(state),
});

export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(MedicalBills);
