/**
 *  HEALTH COVERAGE FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class MedicalBills extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.dependentIncome.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.coverage.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.medicalBills.title;
  }

  render() {
    const { classes, handleSubmit, fields, helpForMedicalBills } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Healthcare Coverage
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Medical Bills
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            {!helpForMedicalBills && (
              <BridgesDynamicFields fields={fields.medicalBillsApplicationRecived} />
            )}

            <BridgesDynamicFields fields={fields.helpForPayingMedicalBills} />
            <BridgesFormEleCollapse stack={helpForMedicalBills} needle={'y'} dividerFlag={false}>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails
                  members={fields.helpForMedicalBillsFrom[0]}
                  details={fields.helpForMedicalBillsPeriod}
                />
              </div>
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

MedicalBills.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  helpForMedicalBills: PropTypes.any,
};

let MedicalBillsForm = reduxForm({
  form: 'medical-bills',
})(MedicalBills);

const selector = formValueSelector('medical-bills');
MedicalBillsForm = connect(state => {
  const helpForMedicalBills = selector(state, 'helpForMedicalBills');
  return {
    helpForMedicalBills,
  };
})(MedicalBillsForm);

export default withStyles(styles)(MedicalBillsForm);
