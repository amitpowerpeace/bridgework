// import { createSelector } from 'reselect';
import { fields } from './defaults';

const medicalBillsData = {
  fields,
};

export const getMedicalBillsFields = state => state.medicalBillsReducer.fields || [];

/**
 * Reducer
 */
export default function medicalBillsReducer(state = medicalBillsData) {
  return state;
}
