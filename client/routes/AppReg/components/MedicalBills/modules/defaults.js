export const fields = {
  people: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: true,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 12,
      },
      grouped: false,
    },
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'peopleeHavingOtherHealtServiceProgram',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 12,
      },
      grouped: false,
    },
    {
      name: 'peoplesOptedForWaterFlintSystem',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'whoIsDependent',
      type: 'checkboxgroup',
      label: 'Who is the dependent?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 12,
      },
      grouped: true,
    },
  ],
  isSupplementSectionProvided: [
    {
      name: 'supplementSection',
      type: 'singlecheckbox',
      label: 'Supplement section was not provided.',
      hightlight: true,
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingAnyonenative: [
    {
      type: 'subHeading',
      label: 'Additional Group Details',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingAdditionalGroupDetails: [
    {
      type: 'subHeading',
      label: 'Additional Group Details',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingAnyonetribe: [
    {
      type: 'subHeading',
      label: 'American Indian or Alaska Native',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingWaterflintcase: [
    {
      type: 'subHeading',
      label: 'Flint Water System',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingtaxfilersdependentsyearlyIncome: [
    {
      type: 'subHeading',
      label: 'Tax Filers, Dependents, Yearly Income',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingmedicalbills: [
    {
      type: 'subHeading',
      label: 'Medical Bills',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  subHeadingCoverage: [
    {
      type: 'subHeading',
      label: 'Coverage',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  primaryCaretaker: [
    {
      name: 'primaryCaretaker',
      type: 'togglebox',
      label: 'Is anyone the primary caretaker for a child (under age of 19) in the home?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      showMoreOption: false,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  medicalFacility: [
    {
      name: 'medicalFacility',
      type: 'togglebox',
      label: 'Does anyone live in a medical facility or nursing home?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  fosterCare: [
    {
      name: 'fosterCareForAnyone',
      type: 'togglebox',
      label: 'Was anyone in foster care when they turned 18?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  healthcareInsurance: [
    {
      name: 'applyingHealthcareInsurance',
      type: 'togglebox',
      label: 'Is anyone applying for health insurance currently incarcerated (detained or jailed)?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  healthProgram: [
    {
      name: 'healthProgram',
      type: 'togglebox',
      label:
        'Has anyone ever gotten a service or referral from the Indian Health Service, a tribal health program, or an urban Indian health program?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneEligible: [
    {
      name: 'anyoneEligible',
      type: 'togglebox',
      label: 'If no, is anyone eligible to get these services?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  allHouseholdMembersAbove19years: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who is the caretaker? (Showing household members age 19 or older)',
      display: 'vertical',
      options: [
        {
          label: 'Leona Ramsay 40M',
          value: 'leona',
        },
        {
          label: 'Owen Ramsay 36F',
          value: 'owen',
        },
      ],
      validate: [],
      showMoreOption: true,
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],
  allHouseholdMembers: {
    name: 'people',
    type: 'radiogroup',
    label: 'Who?',
    display: 'vertical',
    options: [
      {
        label: 'Leona Ramsay',
        value: 'leona',
      },
      {
        label: 'Owen Ramsay',
        value: 'owen',
      },
    ],
    validate: [],
    showMoreOption: false,
    grid: {
      row: true,
      col: 12,
    },
    grouped: false,
  },
  allHouseholdMembersAbove18years: {
    name: 'people',
    type: 'checkboxgroup',
    label: 'Who? (Showing all household members age 18 or older)',
    display: 'vertical',
    options: [
      {
        label: 'Leona Ramsay',
        value: 'leona',
      },
      {
        label: 'Owen Ramsay',
        value: 'owen',
      },
    ],
    validate: [],
    showMoreOption: true,
    grid: {
      row: true,
      col: 12,
    },
    grouped: false,
  },
  householdMembersCareTaker: {
    name: 'people',
    type: 'radiogroup',
    label: 'Who is the caretaker?',
    display: '',
    options: [
      {
        label: 'Leona Ramsay',
        value: 'leona',
      },
      {
        label: 'Owen Ramsay',
        value: 'owen',
      },
    ],
    validate: [],
    grid: {
      row: true,
      col: 12,
    },
    grouped: false,
  },
  childBelow19years: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who is the child (showing household members below age 19)',
      display: '',
      options: [
        {
          label: 'Leona Ramsay',
          value: 'leona',
        },
        {
          label: 'Owen Ramsay',
          value: 'owen',
        },
      ],
      validate: [],
      showMoreOption: true,
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],
  primaryCaretakerOptions: [
    {
      name: 'primaryCaretakerOptions',
      type: 'radiogroup',
      label: 'Who is the child (showing household members below age 19)',
      display: '',
      options: [
        {
          label: 'Mollie Ramsay',
          value: '',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  medicalFacilityOptions: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 12,
      },
      grouped: false,
    },
  ],
  anyoneNative: [
    {
      name: 'anyoneNative',
      type: 'togglebox',
      label: 'Is anyone an American Indian or Alaska Native?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  servicesFromAnyOtherSource: [
    {
      name: 'servicesFromAnyOtherSource',
      type: 'togglebox',
      label:
        'Has anyone ever received a service or referral from the Indian Health Service, a tribal health program, or an urban Indian health program?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneEligibleToOptForServices: [
    {
      name: 'anyoneEligibleToOptForServices',
      type: 'togglebox',
      label: 'If no, is anyone eligible to get these services?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneNativeOptionsLevel1: [
    {
      name: 'isFederallyRecognizedTribe',
      type: 'togglebox',
      label: 'Is this individual a member of a federally recognized tribe?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 7,
      },
      grouped: true,
    },
  ],
  anyoneNativeOptionsLevel2: [
    {
      name: 'anyoneNativeOptionsLevel2',
      type: 'selectbox',
      label: 'Tribe',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
        {
          label: 'three',
          value: '3',
        },
        {
          label: 'four',
          value: '4',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: false,
    },
  ],
  anyoneTribeOptions: [
    {
      name: 'anyoneTribeOptions',
      type: 'radiogroup',
      label:
        'Has anyone ever received a service or referral from the Indian Health Service, a tribal health program, or an urban Indian health program?',
      display: '',
      options: [
        {
          label: 'Leona Ramsay',
          value: 'leona',
        },
        {
          label: 'Owen Ramsay',
          value: 'owen',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  medicalBillsApplicationRecived: [
    {
      name: '',
      type: 'readonlyField',
      label: 'Application received on',
      value: '05-22-2017',
      stringOnly: false,
      inline: true,
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
    },
  ],
  helpForPayingMedicalBills: [
    {
      name: 'helpForMedicalBills',
      type: 'togglebox',
      label: 'Does anyone need help paying for medical bills from the past 3 months?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  helpForMedicalBillsFrom: [
    {
      name: 'helpForMedicalBillsFrom',
      type: 'checkboxgroup',
      label: 'Who?',
      compact: false,
      display: 'vertical',
      options: [
        {
          label: 'Leone Ramsay 40M',
          value: 'Leone Ramsay',
        },
        {
          label: 'Owen Ramsay 36F',
          value: 'Owen Ramsay',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],
  helpForMedicalBillsPeriod: [
    {
      name: 'helpForMedicalBillsPeriod',
      type: 'selectbox',
      label: 'Type',
      options: [
        {
          label: '3 months',
          value: '1',
        },
        {
          label: '6 months',
          value: '6',
        },
        {
          label: '9 months',
          value: '9',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
  ],
  waterflintCase: [
    {
      name: 'waterflintCase',
      type: 'togglebox',
      label:
        'Did anyone consume water from the Flint Water System and live, work, or receive childcare or education at an address that was served by the Flint Water System from April 2014 through present day?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 9,
      },
    },
  ],
  waterflintCaseOptions: [
    {
      name: 'addrServedbyFlintWater',
      type: 'radio',
      label: 'Address Served by Flint Water',
      compact: false,
      options: [
        {
          label: 'Home',
          value: 'Home',
        },
        {
          label: 'Work',
          value: 'Work',
        },
        {
          label: 'School',
          value: 'School',
        },
        {
          label: 'Childcare Facility',
          value: 'Childcare Facility',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      type: 'checkbuttongroup',
      label: '',
      name: '1171',
      spreadType: 'vertical',
      viewType: 'table',
      showLimitedTableData: true,
      viewTypeTitle: 'Use Current Physical Address',
      options: [
        {
          label: 'No.123 Main Street, Lansing, Michigan',
          value: 'No.123 Main Street, Lansing, Michigan',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      type: 'checkbuttongroup',
      label: '',
      name: '1171',
      spreadType: 'vertical',
      viewType: 'table',
      showLimitedTableData: true,
      viewTypeTitle: 'Use Address from Data Collection',
      options: [
        {
          label: 'No.123 Main Street, Lansing, Michigan',
          value: 'No.123 Main Street, Lansing, Michigan',
        },
        {
          label: 'No.123 Main Street, Lansing, Michigan',
          value: 'No.123 Main Street, Lansing, Michigan',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'house_street_address',
      type: 'text',
      startIcon: '',
      label: 'House Street Address',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
      grouped: true,
    },
    {
      name: 'dwellingType',
      type: 'selectbox',
      label: 'Dwelling Type',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      errorMessage: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
    },
    {
      name: 'facilityType',
      type: 'selectbox',
      label: 'Facility Type',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      errorMessage: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
    },
    {
      name: 'Apt/Lot#',
      type: 'text',
      startIcon: '',
      label: 'Address Line 2',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
      grouped: true,
    },
    {
      name: 'state',
      type: 'text',
      startIcon: '',
      label: 'State',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'zipcode',
      type: 'text',
      startIcon: '',
      label: 'Zipcode',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 2,
      },
      maskType: 'zipcode',
      grouped: false,
    },
    {
      name: 'city',
      type: 'selectbox',
      label: 'City',
      options: [
        {
          label: 'city one',
          value: '1',
        },
        {
          label: 'city two',
          value: '2',
        },
        {
          label: 'city three',
          value: '3',
        },
        {
          label: 'city four',
          value: '4',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'country',
      type: 'selectbox',
      label: 'Country',
      options: [
        {
          label: 'Country one',
          value: '1',
        },
        {
          label: 'Country two',
          value: '2',
        },
        {
          label: 'Country three',
          value: '3',
        },
        {
          label: 'Country four',
          value: '4',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'waterFlintCaseStartDate',
      type: 'datepicker',
      label: 'Start Date',
      value: 'MM/YY',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'waterFlintCaseEndDate',
      type: 'datepicker',
      label: 'End Date',
      value: 'MM/YY',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Another Address',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  federalTax: [
    {
      name: 'federalTax',
      type: 'togglebox',
      label: 'Does anyone applying plan to file a federal tax return next year?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  dependentTax: [
    {
      name: 'dependentTax',
      type: 'togglebox',
      label: 'Will anyone applying be claimed as a dependent on someone else’s tax return?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  incomeChange: [
    {
      name: 'incomeChange',
      type: 'togglebox',
      label: 'Does anyone’s income change from month to month?',
      toggleStack: [
        {
          title: 'Y',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  federalTaxOptions: [
    {
      name: 'filingjointly',
      type: 'togglebox',
      label: 'Are they filing jointly with a spouse?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'claimingDependents',
      type: 'togglebox',
      label: 'Are they claiming dependents?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],
  dependentTaxOptions: [
    {
      type: 'checkboxgroup',
      label: 'Who is the Tax filer?',
      name: 'taxfiler',
      options: [
        {
          label: 'Leone Ramsey',
          value: 'Leone Ramsey',
        },
        {
          label: 'Owen Ramsey',
          value: 'Owen Ramsey',
        },
        {
          label: 'Other Person',
          value: 'Other person',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'dependenttaxfiler',
      type: 'selectbox',
      label: 'How is the dependent realted to the tax filer?',
      options: [
        {
          label: 'Brother',
          value: 'brother',
        },
        {
          label: 'Sister',
          value: 'sister',
        },
        {
          label: 'Spouse',
          value: 'spouse',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
      grouped: true,
    },
  ],
  incomeChangeOptions: [
    {
      name: 'totalestimatedincomethisyear',
      type: 'text',
      startIcon: '',
      label: 'Total estimated income this year',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'totalestimatedincomenextyear',
      type: 'text',
      startIcon: '',
      label: 'Total estimated income next year',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'notProvidedEstimatedIncomeThisYear',
      type: 'singlecheckbox',
      label: 'Not Provided',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'notProvidedEstimatedIncomeNextYear',
      type: 'singlecheckbox',
      label: 'Not Provided',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
  ],
  anyoneunderhealthCoverage: [
    {
      name: 'anyoneunderhealthCoverage',
      type: 'singlecheckbox',
      label: 'Is anyone currently enrolled in health coverage (even if not applying)?',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  autorenewalhealthCoverage: [
    {
      name: 'autorenewalhealthCoverage',
      type: 'singlecheckbox',
      label:
        'To make it easier to determine healthcare eligibility in future years can IRS data be used for automatic renewals?',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  helprequiredForMedicalBillsOptions: [
    {
      name: 'months',
      type: 'radiogroup',
      label: 'Which three months?',
      compact: false,
      options: [
        {
          label: 'Jan',
          value: '1',
        },
        {
          label: 'Feb',
          value: '2',
        },
        {
          label: 'Mar',
          value: '3',
        },
        {
          label: 'Apr',
          value: '4',
        },
        {
          label: 'May',
          value: '5',
        },
        {
          label: 'Jun',
          value: '6',
        },
        {
          label: 'Jul',
          value: '7',
        },
        {
          label: 'Aug',
          value: '8',
        },
        {
          label: 'Sep',
          value: '9',
        },
        {
          label: 'Oct',
          value: '10',
        },
        {
          label: 'Nov',
          value: '11',
        },
        {
          label: 'Dec',
          value: '12',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 9,
      },
      grouped: false,
    },
  ],
  anyoneunderhealthCoverageOptions: [
    {
      name: 'typeandnameofcoverage',
      type: 'text',
      startIcon: '',
      label: 'Type and Name of Coverage',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: false,
    },
    {
      type: 'checkboxgroup',
      label: 'What programs is the household applying for?',
      name: 'programselection',
      options: [
        {
          label: 'Medicare',
          value: 'Medicare',
        },
        {
          label: 'COBRA',
          value: 'COBRA',
        },
        {
          label: 'Retiree Health Plan',
          value: 'Retiree Health Plan',
        },
        {
          label: 'Limited Benefit Plan',
          value: 'Limited Benefit Plan',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
    {
      name: 'policynumber',
      type: 'text',
      startIcon: '',
      label: 'Policy Number',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
    },
  ],
  autorenewalhealthCoverageOptions: [
    {
      name: 'years',
      type: 'radiogroup',
      label: 'For how many years?',
      compact: false,
      options: [
        {
          label: '5',
          value: '5',
        },
        {
          label: '4',
          value: '4',
        },
        {
          label: '3',
          value: '3',
        },
        {
          label: '2',
          value: '2',
        },
        {
          label: '1',
          value: '1',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
};
