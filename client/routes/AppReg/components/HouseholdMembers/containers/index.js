import { connect } from 'react-redux';
import HouseholdMembers from '../components';
import { getHouseholdMembersFields } from '../modules';
import { injectAsyncReducers } from 'client/store';
import householdMembersReducer from 'client/routes/AppReg/components/HouseholdMembers/modules';

// inject reducers
injectAsyncReducers({
  householdMembersReducer,
});

const mapStateToProps = state => ({
  fields: getHouseholdMembersFields(state),
});

export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(HouseholdMembers);
