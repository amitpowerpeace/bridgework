// import { createSelector } from 'reselect';
import { fields } from './defaults';

const householdMembersData = {
  fields,
};

export const getHouseholdMembersFields = state => state.householdMembersReducer.fields || [];

/**
 * Reducer
 */
export default function householdMembersReducer(state = householdMembersData) {
  return state;
}
