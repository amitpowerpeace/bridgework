export const fields = {
  ignorantUser: [
    {
      name: 'ignorantuser',
      type: 'singlecheckbox',
      label:
        'None of the applicants have selected a program on the 1171, the program selection on the front page of the application is blank, and none of the supplement questions have been answered',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 9,
      },
    },
  ],
  memberStack: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: '',
      compact: true,
      options: [
        {
          label: '<big>Leona Ramsey</big> <br /> Male&nbsp;|&nbsp;29&nbsp;years',
          value: 'Leone Ramsey',
        },
        {
          label: '<big>Owen Ramsey</big> <br /> Female&nbsp;|&nbsp;26&nbsp;years',
          value: 'Owen Ramsey',
        },
        {
          label: '<big>Nathen Ramsey</big> <br /> Female&nbsp;|&nbsp;10&nbsp;years',
          value: 'Nathen Ramsey',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 9,
      },
    },
  ],
  addNewMembers: [
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add New Members to Case',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'left',
      buttonColor: 'blue',
      grid: {
        row: true,
        col: 4,
      },
    },
  ],
  collapseArray: [
    {
      header: 'Leone Ramsey',
      hoh: true,
      headerTagLong: 'Head of household',
      headerTagShort: 'HOH',
      headingTeaser: 'Male | 32 years',
      gender: 'Male',
      age: '32 years',
      fStatus: 'success',
      content: [
        {
          name: 'firstname',
          type: 'text',
          startIcon: '',
          label: 'First Name',
          value: 'Leona Ramsay',
          validate: [],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'middlename',
          type: 'text',
          startIcon: '',
          label: 'Middle Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'lastname',
          type: 'text',
          startIcon: '',
          label: 'Last Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'snn',
          type: 'text',
          startIcon: '',
          label: 'SSN',
          value: '000-000-0000',
          placeholder: '000-000-0000',
          maskType: 'ssn',
          validate: [],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'dob',
          type: 'datepicker',
          label: 'Date of Birth',
          value: '22-02-2018',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'Sex',
          toggleStack: [
            {
              title: 'M',
              value: 'm',
            },
            {
              title: 'F',
              value: 'f',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'US Citizen',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'Married',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'In the home',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'relationwithhoh',
          type: 'selectbox',
          label: 'Relation to HOH',
          options: [
            {
              label: 'one',
              value: '1',
            },
            {
              label: 'two',
              value: '2',
            },
            {
              label: 'three',
              value: '3',
            },
            {
              label: 'four',
              value: '4',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 4,
          },
        },
        {
          name: 'ethnicity',
          type: 'radiogroup',
          label: 'Ethnicity',
          compact: false,
          options: [
            {
              label: 'Hispanic/Latino',
              value: 'Hispanic/Latino',
            },
            {
              label: 'Non Hispanic/Latino',
              value: 'Non Hispanic/Latino',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'race',
          type: 'radiogroup',
          label: 'Race',
          compact: false,
          options: [
            {
              label: 'African American / Black',
              value: 'African American / Black',
            },
            {
              label: 'American Indian / Alsaka Native',
              value: 'American Indian / Alsaka Native',
            },
            {
              label: 'Asian',
              value: 'Asian',
            },
            {
              label: 'Native huwaiian / Other Pacific Islander',
              value: 'Native huwaiian / Other Pacific Islander',
            },
            {
              label: 'White',
              value: 'white',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'programs',
          type: 'radiogroup',
          label: 'Programs',
          compact: false,
          options: [
            {
              label: 'Healthcare',
              value: 'healthcare',
            },
            {
              label: 'Food',
              value: 'food',
            },
            {
              label: 'Cash',
              value: 'cash',
            },
            {
              label: 'Childcare',
              value: 'childcare',
            },
            {
              label: 'SER',
              value: 'ser',
            },
            {
              label: 'NONE',
              value: 'none',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'lessThan50',
          type: 'singlecheckbox',
          label: 'Is this person a DHHS employee?',
          checked: false,
          iconColor: '#4AA564',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'Owen Ramsey',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: 'Female | 26 Years',
      gender: 'Male',
      age: '32 years',
      fStatus: 'error',
      content: [
        {
          name: 'firstname',
          type: 'text',
          startIcon: '',
          label: 'First Name',
          value: 'Leona Ramsay',
          validate: [],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'middlename',
          type: 'text',
          startIcon: '',
          label: 'Middle Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'lastname',
          type: 'text',
          startIcon: '',
          label: 'Last Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'snn',
          type: 'text',
          startIcon: '',
          label: 'SSN',
          value: '000-000-0000',
          placeholder: '000-000-0000',
          maskType: 'ssn',
          validate: [],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'dob',
          type: 'datepicker',
          label: 'Date of Birth',
          value: '22-02-2018',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'Sex',
          toggleStack: [
            {
              title: 'M',
              value: 'm',
            },
            {
              title: 'F',
              value: 'f',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'US Citizen',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'Married',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'In the home',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'relationwithhoh',
          type: 'selectbox',
          label: 'Relation to HOH',
          options: [
            {
              label: 'one',
              value: '1',
            },
            {
              label: 'two',
              value: '2',
            },
            {
              label: 'three',
              value: '3',
            },
            {
              label: 'four',
              value: '4',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 4,
          },
        },
        {
          name: 'ethnicity',
          type: 'radiogroup',
          label: 'Ethnicity',
          compact: false,
          options: [
            {
              label: 'Hispanic/Latino',
              value: 'Hispanic/Latino',
            },
            {
              label: 'Non Hispanic/Latino',
              value: 'Non Hispanic/Latino',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'race',
          type: 'radiogroup',
          label: 'Race',
          compact: false,
          options: [
            {
              label: 'African American / Black',
              value: 'African American / Black',
            },
            {
              label: 'American Indian / Alsaka Native',
              value: 'American Indian / Alsaka Native',
            },
            {
              label: 'Asian',
              value: 'Asian',
            },
            {
              label: 'Native huwaiian / Other Pacific Islander',
              value: 'Native huwaiian / Other Pacific Islander',
            },
            {
              label: 'White',
              value: 'white',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'programs',
          type: 'radiogroup',
          label: 'Programs',
          compact: false,
          options: [
            {
              label: 'Healthcare',
              value: 'healthcare',
            },
            {
              label: 'Food',
              value: 'food',
            },
            {
              label: 'Cash',
              value: 'cash',
            },
            {
              label: 'Childcare',
              value: 'childcare',
            },
            {
              label: 'SER',
              value: 'ser',
            },
            {
              label: 'NONE',
              value: 'none',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'lessThan50',
          type: 'singlecheckbox',
          label: 'Is this person a DHHS employee?',
          checked: false,
          iconColor: '#4AA564',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
      ],
    },
    {
      header: 'New member',
      hoh: false,
      headerTagLong: '',
      headerTagShort: '',
      headingTeaser: '',
      fStatus: '',
      gender: 'Male',
      age: '32 years',
      content: [
        {
          name: 'firstname',
          type: 'text',
          startIcon: '',
          label: 'First Name',
          value: 'Leona Ramsay',
          validate: [],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'middlename',
          type: 'text',
          startIcon: '',
          label: 'Middle Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'lastname',
          type: 'text',
          startIcon: '',
          label: 'Last Name',
          value: '',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'snn',
          type: 'text',
          startIcon: '',
          label: 'SSN',
          value: '000-000-0000',
          placeholder: '000-000-0000',
          maskType: 'ssn',
          validate: [],
          grid: {
            row: true,
            col: 3,
          },
        },
        {
          name: 'dob',
          type: 'datepicker',
          label: 'Date of Birth',
          value: '22-02-2018',
          validate: [],
          grid: {
            row: false,
            col: 3,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'Sex',
          toggleStack: [
            {
              title: 'M',
              value: 'm',
            },
            {
              title: 'F',
              value: 'f',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'US Citizen',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'Married',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'test',
          type: 'togglebox',
          label: 'In the home',
          toggleStack: [
            {
              title: 'Y',
              value: 'y',
            },
            {
              title: 'N',
              value: 'n',
            },
          ],
          validate: [],
          grid: {
            row: false,
            col: 2,
          },
        },
        {
          name: 'relationwithhoh',
          type: 'selectbox',
          label: 'Relation to HOH',
          options: [
            {
              label: 'one',
              value: '1',
            },
            {
              label: 'two',
              value: '2',
            },
            {
              label: 'three',
              value: '3',
            },
            {
              label: 'four',
              value: '4',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 4,
          },
        },
        {
          name: 'ethnicity',
          type: 'radiogroup',
          label: 'Ethnicity',
          compact: false,
          options: [
            {
              label: 'Hispanic/Latino',
              value: 'Hispanic/Latino',
            },
            {
              label: 'Non Hispanic/Latino',
              value: 'Non Hispanic/Latino',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'race',
          type: 'radiogroup',
          label: 'Race',
          compact: false,
          options: [
            {
              label: 'African American / Black',
              value: 'African American / Black',
            },
            {
              label: 'American Indian / Alsaka Native',
              value: 'American Indian / Alsaka Native',
            },
            {
              label: 'Asian',
              value: 'Asian',
            },
            {
              label: 'Native huwaiian / Other Pacific Islander',
              value: 'Native huwaiian / Other Pacific Islander',
            },
            {
              label: 'White',
              value: 'white',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'programs',
          type: 'radiogroup',
          label: 'Programs',
          compact: false,
          options: [
            {
              label: 'Healthcare',
              value: 'healthcare',
            },
            {
              label: 'Food',
              value: 'food',
            },
            {
              label: 'Cash',
              value: 'cash',
            },
            {
              label: 'Childcare',
              value: 'childcare',
            },
            {
              label: 'SER',
              value: 'ser',
            },
            {
              label: 'NONE',
              value: 'none',
            },
          ],
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
        {
          name: 'lessThan50',
          type: 'singlecheckbox',
          label: 'Is this person a DHHS employee?',
          checked: false,
          iconColor: '#4AA564',
          validate: [],
          grid: {
            row: true,
            col: 12,
          },
        },
      ],
    },
  ],
};
