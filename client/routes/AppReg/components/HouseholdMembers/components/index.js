/**
 * APPLICATION REGISTRATION - HOUSEHOLD MEMBERS FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesButton from 'client/components/common/BridgesButton';
import BridgesCollapse from 'client/components/common/BridgesCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class HouseholdMembers extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.programSelection.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.houseDetail.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.houseMember.title;
  }

  render() {
    const { classes, handleSubmit, fields } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Household Members
            </Typography>
          </Grid>
        </Grid>

        <BridgesPaper>
          <BridgesDynamicFields fields={fields.ignorantUser} />

          <Grid container spacing={24}>
            <Grid item xs={12}>
              <Typography variant="display4" className={classes.subHeading}>
                Add Members from Associated Case
                <span className={classes.headerUnderline} />
              </Typography>
            </Grid>
          </Grid>

          <BridgesDynamicFields fields={fields.memberStack} />

          <Grid container spacing={24}>
            <Grid item xs={12}>
              <Typography variant="display4" className={classes.subHeading}>
                Add New Members to this Case
                <span className={classes.headerUnderline} />
              </Typography>
            </Grid>
          </Grid>

          <Grid container spacing={24}>
            <Grid item xs={12}>
              <BridgesDynamicFields fields={fields.addNewMembers} />
            </Grid>
          </Grid>
        </BridgesPaper>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            {fields.collapseArray.map((collapse, i) => (
              <BridgesCollapse cData={collapse} key={i}>
                <BridgesDynamicFields fields={collapse.content} />
                <Grid
                  container
                  spacing={16}
                  alignItems={'flex-start'}
                  direction={'row'}
                  justify={'flex-end'}
                >
                  <Grid item>
                    <BridgesButton title="Save" iconAlign="left" buttonType="squarebtn" />
                  </Grid>
                  <Grid item>
                    <BridgesButton title="Cancel" iconAlign="left" buttonType="squarebtn" />
                  </Grid>
                </Grid>
              </BridgesCollapse>
            ))}
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

HouseholdMembers.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const HouseholdMembersForm = reduxForm({
  form: 'household-members-form',
})(HouseholdMembers);

export default withStyles(styles)(HouseholdMembersForm);
