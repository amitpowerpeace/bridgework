import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import programSelectionReducer from 'client/routes/AppReg/components/ProgramSelection/modules';
import ProgramSelection from '../components';
import { getProgramSelectionFields } from '../modules';

// inject reducers
injectAsyncReducers({
  programSelectionReducer,
});

const mapStateToProps = state => ({
  fields: getProgramSelectionFields(state),
});

export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(ProgramSelection);
