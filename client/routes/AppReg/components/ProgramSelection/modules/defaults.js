export const fields = {
  primary: [
    {
      type: 'checkboxgroup',
      label: 'What programs is the household applying for?',
      name: 'programSelection',
      options: [
        {
          label: 'Healthcare',
          value: 'healthcare',
        },
        {
          label: 'Food',
          value: 'food',
        },
        {
          label: 'Cash',
          value: 'cash',
        },
        {
          label: 'Child Care DC',
          value: 'child_care_dc',
        },
        {
          label: 'SER',
          value: 'ser',
        },
        {
          label: 'None',
          value: 'none',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  healthcare: [],
  food: [
    {
      name: 'monthyIncomeLessThan50',
      type: 'togglebox',
      label:
        'Is the Monthly income less than $150 or is there less than $100 in cash/accounts right now?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 8,
      },
    },
    {
      name: 'lowIncome',
      type: 'togglebox',
      label:
        'The client is a migrant or seasonal farmworker whose income has stopped and who has $100 or less in cash/accounts right now.',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 8,
      },
    },
    {
      name: 'combinedHouseholdMonthlyIncome',
      type: 'togglebox',
      label:
        'The combined household monthly income and cash/accounts are less than the household’s combined monthly rent/mortgage and utilities.',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 8,
      },
    },
  ],
  cash: [],
  child_care_dc: [],
  ser: [],
  none: [],
};
