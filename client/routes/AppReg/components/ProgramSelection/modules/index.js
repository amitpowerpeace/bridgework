// import { createSelector } from 'reselect';
import { fields } from './defaults';

const programSelectionData = {
  fields,
};

export const getProgramSelectionFields = state => state.programSelectionReducer.fields || [];

/**
 * Reducer
 */
export default function programSelectionReducer(state = programSelectionData) {
  return state;
}
