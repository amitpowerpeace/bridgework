/**
 * APPLICATION REGISTRATION - PROGRAM SELECTION FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';
import pageStyles from 'client/styles/common/pages';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...pageStyles,
  ...formStyles,
});

class ProgramSelection extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.contactInfo.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.houseMember.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.programSelection.title;
  }

  render() {
    const { classes, handleSubmit, fields, programSelection } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Program Selection
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.primary} />
            <BridgesFormEleCollapse stack={programSelection} needle={'food'} dividerFlag={false}>
              <BridgesDynamicFields fields={fields.food} />
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

ProgramSelection.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  programSelection: PropTypes.string,
};

ProgramSelection.defaultProps = {
  programSelection: '',
};

let ProgramSelectionForm = reduxForm({
  form: 'program-select-form',
})(ProgramSelection);

// Decorate with connect to read form values
// https://redux-form.com/7.3.0/examples/selectingformvalues/
const selector = formValueSelector('program-select-form');

ProgramSelectionForm = connect(state => {
  const programSelection = selector(state, 'programSelection');
  return {
    programSelection,
  };
})(ProgramSelectionForm);

export default withStyles(styles)(ProgramSelectionForm);
