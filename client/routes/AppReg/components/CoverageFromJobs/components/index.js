/**
 *  HEALTH COVERAGE FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { formValueSelector, reduxForm } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import paths from 'client/routes/AppReg/urls';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
// THEME STYLES
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});
let CoverageFromJobs = props => {
  const {
    classes,
    handleSubmit,
    fields,
    subHeadingCoverage,
    anyoneunderhealthCoverage,
    anyoneunderHealthCovOptions,
    anyoneunderhealthCovegeOpt,
  } = props;

  const actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.coverage.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.foodAssist.url,
      align: 'right',
    },
  ];

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
            Healthcare Coverage
          </Typography>
        </Grid>
      </Grid>
      <form className={classes.dataForm} onSubmit={handleSubmit}>
        <BridgesPaper minHeight={422} elevate={1} roundcorner>
          <BridgesDynamicFields fields={fields.subHeadingCoverage} />
          <BridgesDynamicFields fields={fields && fields.anyoneunderhealthCoverage} />
          <BridgesFormEleCollapse stack={anyoneunderhealthCoverage} needle={'y'} dividerFlag>
            <BridgesMemberDetails
              members={fields && fields.anyoneunderHealthCovOptions[0]}
              details={fields && fields.anyoneunderhealthCovegeOpt}
            />
          </BridgesFormEleCollapse>
        </BridgesPaper>
        <BridgesFormActions actions={actions} />
      </form>
    </div>
  );
};

CoverageFromJobs.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  subHeadingCoverage: PropTypes.any,
};

CoverageFromJobs = reduxForm({
  form: 'coveragefromjobs',
})(CoverageFromJobs);

const selector = formValueSelector('coveragefromjobs');
CoverageFromJobs = connect(state => {
  const {
    anyoneunderhealthCoverage,
    anyoneunderHealthCovOptions,
    anyoneunderhealthCovegeOpt,
    subHeadingCoverage,
  } = selector(
    state,
    'anyoneunderhealthCoverage',
    'anyoneunderHealthCovOptions',
    'anyoneunderhealthCovegeOpt',
    'subHeadingCoverage',
  );
  return {
    anyoneunderhealthCoverage,
    anyoneunderHealthCovOptions,
    anyoneunderhealthCovegeOpt,
    subHeadingCoverage,
  };
})(CoverageFromJobs);

export default withStyles(styles)(CoverageFromJobs);
