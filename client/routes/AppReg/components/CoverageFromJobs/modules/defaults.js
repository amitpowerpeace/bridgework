export const fields = {
  subHeadingCoverage: [
    {
      type: 'subHeading',
      label: 'Coverage From Jobs',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneunderhealthCoverage: [
    {
      name: 'anyoneunderhealthCoverage',
      type: 'togglebox',
      label: 'Is anyone currently enrolled in health coverage (even if not applying)?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneunderHealthCovOptions: [
    {
      name: 'anyoneunderHealthCovOptions',
      type: 'checkboxgroup',
      label: 'Who?',
      display: 'vertical',
      options: [
        {
          label: 'Leona Ramsay 40,M',
          value: 'leona',
        },
        {
          label: 'Owen Ramsay 36,F',
          value: 'owen',
        },
        {
          label: 'Nathan Ramsay 10,M',
          value: 'Nathan',
        },
        {
          label: 'Mollie Ramsay 13,F',
          value: 'Mollie',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
  anyoneunderhealthCovegeOpt: [
    {
      name: 'typecoverage',
      type: 'text',
      startIcon: '',
      label: 'Employee Social Security#',
      placeholder: '000-000-0000',
      maskType: 'ssn',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'policycoverage',
      type: 'text',
      startIcon: '',
      label: 'Employer',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'policycoverage',
      type: 'text',
      startIcon: '',
      label: 'Employer Identification#',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'policycoverage',
      type: 'text',
      startIcon: '',
      label: 'Employer Contract',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'policycoverage',
      type: 'text',
      startIcon: '',
      label: 'Email of Employer Contract',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
      grouped: true,
    },
    {
      name: 'employercovaerage',
      type: 'togglebox',
      label: 'Can the employee get coverage now or sometime in the next 3 months?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'employercovaerage',
      type: 'togglebox',
      label:
        'Does the employer offer a health plan that pays atleast 60% of the total costs of benefits(the minimum value)?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: 'employercovaerage',
      type: 'togglebox',
      label: 'Will the employer make any changes for the new plan year(if you know)?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],

  autorenewalhealthCoverage: [
    {
      name: 'autorenewalhealthCoverage',
      type: 'togglebox',
      label:
        'To make it easier to determine healthcare eligibility in future years can IRS data be used for automatic renewals?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  autorenewalhealthCoverageOptions: [
    {
      name: 'years',
      type: 'radiogroup',
      label: 'For how many years?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: '5',
          value: '5',
        },
        {
          label: '4',
          value: '4',
        },
        {
          label: '3',
          value: '3',
        },
        {
          label: '2',
          value: '2',
        },
        {
          label: '1',
          value: '1',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
};
