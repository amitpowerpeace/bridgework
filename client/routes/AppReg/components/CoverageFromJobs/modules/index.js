// import { createSelector } from 'reselect';
import { fields } from './defaults';

const CoverageFromJobsData = {
  fields,
};

export const getCoverageFromJobsFields = state => state.coverageFromJobsReducer.fields || [];

/**
 * Reducer
 */
export default function coverageFromJobsReducer(state = CoverageFromJobsData) {
  return state;
}
