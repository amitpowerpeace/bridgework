import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import coverageFromJobsReducer from 'client/routes/AppReg/components/CoverageFromJobs/modules';
import CoverageFromJobs from '../components';
import { getCoverageFromJobsFields } from '../modules';

// inject reducers
injectAsyncReducers({
  coverageFromJobsReducer,
});

const mapStateToProps = state => ({
  fields: getCoverageFromJobsFields(state),
});

export default connect(mapStateToProps)(CoverageFromJobs);
