/**
 *  Dependents' Income Form
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import pageStyles from 'client/styles/common/pages';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class TaxFilersDependentsYearlyIncome extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.waterFlintCase.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.medicalBills.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.dependentIncome.title;
  }

  render() {
    const { classes, handleSubmit, fields, federalTax, dependentTax, incomeChange } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Healthcare Coverage
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Tax Filers, Dependents, Yearly Income
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <BridgesDynamicFields fields={fields.federalTax} />

            <BridgesFormEleCollapse stack={federalTax} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails
                  members={fields.people[1]}
                  details={fields.federalTaxOptions}
                />
              </div>
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.dependentTax} />
            <BridgesFormEleCollapse stack={dependentTax} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails
                  members={fields.people[1]}
                  details={fields.dependentTaxOptions}
                />
              </div>
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.incomeChange} />
            <BridgesFormEleCollapse stack={incomeChange} needle={'y'} dividerFlag>
              <div className={classes.divFullWidth}>
                <BridgesMemberDetails
                  members={fields.people[1]}
                  details={fields.incomeChangeOptions}
                />
              </div>
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

TaxFilersDependentsYearlyIncome.propTypes = {
  classes: PropTypes.object.isRequired,
  dependentTax: PropTypes.any,
  federalTax: PropTypes.any,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  incomeChange: PropTypes.any,
};

let TaxFilersDependentsYearlyIncomeForm = reduxForm({
  form: 'taxfilers-dependents-yearly-income',
})(TaxFilersDependentsYearlyIncome);

const selector = formValueSelector('taxfilers-dependents-yearly-income');
TaxFilersDependentsYearlyIncomeForm = connect(state => {
  const { primaryCaretaker, federalTax, dependentTax, incomeChange } = selector(
    state,
    'primaryCaretaker',
    'federalTax',
    'dependentTax',
    'incomeChange',
  );
  return {
    primaryCaretaker,
    federalTax,
    dependentTax,
    incomeChange,
  };
})(TaxFilersDependentsYearlyIncomeForm);

export default withStyles(styles)(TaxFilersDependentsYearlyIncomeForm);
