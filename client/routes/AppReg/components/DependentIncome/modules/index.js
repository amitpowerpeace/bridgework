// import { createSelector } from 'reselect';
import { fields } from './defaults';

const taxFilersDependentsYearlyIncomeData = {
  fields,
};

export const getTaxFilersDependentsYearlyIncomeFields = state =>
  state.taxFilersDependentsYearlyIncomeReducer.fields || [];

/**
 * Reducer
 */
export default function taxFilersDependentsYearlyIncomeReducer(
  state = taxFilersDependentsYearlyIncomeData,
) {
  return state;
}
