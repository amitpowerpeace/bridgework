import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import taxFilersDependentsYearlyIncomeReducer from 'client/routes/AppReg/components/DependentIncome/modules';
import DependentIncome from '../components';
import { getTaxFilersDependentsYearlyIncomeFields } from '../modules';

// inject reducers
injectAsyncReducers({
  taxFilersDependentsYearlyIncomeReducer,
});

const mapStateToProps = state => ({
  fields: getTaxFilersDependentsYearlyIncomeFields(state),
});

export default connect(mapStateToProps)(DependentIncome);
