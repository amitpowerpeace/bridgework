/**
 * APPLICATION REGISTRATION - CONTACT INFORMATION
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesModal from 'client/components/common/BridgesModal';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  subHeadingTop: {
    marginTop: 10
  }
});

const data = {
  homeless: 'n',
  diffMailAddress: 'n',
};

class ContactInfo extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.fileClearance.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.programSelection.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.contactInfo.title;
  }

  render() {
    const { classes, handleSubmit, fields, homeless, diffMailAddress } = this.props;
    const desc = 'Similar address found. Use this address?';
    const desctext = '106 W Allegan St Ingham, Lansing, MI 48933-1700';
    const title = 'Address Match';

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Contact Information
            </Typography>
          </Grid>
        </Grid>

        <form onSubmit={handleSubmit} className={classes.primary}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.primary} />

            <BridgesFormEleCollapse stack={homeless} needle={'n'} dividerFlag={false}>
              <Grid container spacing={24}>
                <Grid item xs={12}>
                  <Typography variant="display4" className={classNames(classes.subHeading,classes.subHeadingTop)}>
                    Physical Address
                    <span className={classes.headerUnderline} />
                  </Typography>
                </Grid>
              </Grid>
              <BridgesDynamicFields fields={fields.notHomeless} />
            </BridgesFormEleCollapse>

            <BridgesFormEleCollapse stack={homeless} needle={'y'} dividerFlag={false}>
              <BridgesDynamicFields fields={fields.mailingAddress} />
            </BridgesFormEleCollapse>

            <BridgesFormEleCollapse stack={diffMailAddress} needle={'y'} dividerFlag={false}>
              <BridgesDynamicFields fields={fields.mailingAddressSelected} />
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesPaper elevate={1} roundcorner>
            <BridgesModal title={title} desc={desc} desctext={desctext} />
          </BridgesPaper>
          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

ContactInfo.propTypes = {
  classes: PropTypes.object.isRequired,
  diffMailAddress: PropTypes.string,
  fields: PropTypes.object.isRequired,
  homeless: PropTypes.string,
};

let ContactInfoForm = reduxForm({
  form: 'contact-info-form',
})(ContactInfo);

// Decorate with connect to read form values
// https://redux-form.com/6.5.0/examples/selectingformvalues/
const selector = formValueSelector('contact-info-form');
ContactInfoForm = connect(state => {
  const { homeless, diffMailAddress } = selector(state, 'homeless', 'diffMailAddress');
  const initialValues = data;
  return {
    homeless,
    diffMailAddress,
    initialValues,
  };
})(ContactInfoForm);

export default withStyles(styles)(ContactInfoForm);
