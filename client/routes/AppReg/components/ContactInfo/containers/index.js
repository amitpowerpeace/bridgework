import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import contactInfoReducer from 'client/routes/AppReg/components/ContactInfo/modules';
import ContactInfo from '../components';
import { getContactInfoFields } from '../modules';

// inject reducers
injectAsyncReducers({
  contactInfoReducer,
});

const mapStateToProps = state => ({
  fields: getContactInfoFields(state),
});

export default connect(mapStateToProps)(ContactInfo);
