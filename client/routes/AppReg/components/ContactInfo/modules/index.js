// import { createSelector } from 'reselect';
import { fields } from './defaults';

const contactInfoData = {
  fields,
};

export const getContactInfoFields = state => state.contactInfoReducer.fields || [];

/**
 * Reducer
 */
export default function contactInfoReducer(state = contactInfoData) {
  return state;
}
