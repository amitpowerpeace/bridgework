export const fields = {
  primary: [
    {
      name: 'homeless',
      type: 'togglebox',
      label: 'HomeLess ?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  notHomeless: [
    {
      name: 'household_street_address',
      type: 'text',
      startIcon: '',
      label: 'Household Street Address',
      value: '',
      validate: ['required'],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      name: 'dwelling_type',
      type: 'selectbox',
      label: 'Dwelling Type',
      value: '',
      options: [
        {
          label: 'Office',
          value: '1',
        },
        {
          label: 'Apartment',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 2,
      },
    },
    {
      name: 'facility_type',
      type: 'selectbox',
      label: 'Facility Type',
      value: '',
      options: [
        {
          label: 'Office',
          value: '1',
        },
        {
          label: 'Apartment',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
    },
    {
      name: 'address',
      type: 'text',
      startIcon: '',
      label: 'Address Line 2',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      name: 'state',
      type: 'selectbox',
      startIcon: '',
      label: 'State',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: ['required'],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      name: 'zipcode',
      type: 'text',
      startIcon: '',
      label: 'Zipcode',
      value: '',
      validate: ['required'],
      grid: {
        row: false,
        col: 3,
      },
      maskType: 'zipcode',
    },
    {
      type: 'bridgesbutton',
      title: 'Check Address',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'left',
      buttonColor: '',
      buttonDynamic: true,
      grid: {
        row: false,
        col: 3,
      },
    },
    {
      name: '1171',
      label: '',
      type: 'checkbuttongroup',
      spreadType: 'vertical',
      viewType: 'table',
      viewTypeTitle: 'Address Matches',
      options: [
        {
          label: 'Self-Service Application',
          value: 'Self-Service Application',
        },
        {
          label: '1171',
          value: '1171',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
    {
      name: 'city',
      type: 'selectbox',
      startIcon: '',
      label: 'City',
      value: '',
      validate: ['required'],
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      name: 'country',
      type: 'selectbox',
      startIcon: ' ',
      label: 'Country',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: ['required'],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      type: 'subHeading',
      label: 'Phone and Email',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'celphonenumber',
      type: 'text',
      startIcon: '',
      label: 'Cell Phone Number',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      maskType: 'phone',
    },
    {
      name: 'homephonenumber',
      type: 'text',
      startIcon: '',
      label: 'Home Phone Number',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      maskType: 'phone',
    },
    {
      name: 'tty',
      type: 'text',
      startIcon: ' ',
      label: 'TTY',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
    },
    {
      name: 'emailaddress',
      type: 'text',
      startIcon: '',
      label: 'Email Address',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      type: 'hr',
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'diffMailAddress',
      type: 'togglebox',
      label: 'Is The Mailing Address Different From Above?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  mailingAddress: [
    {
      type: 'subHeading',
      label: 'Mailing Address',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'street_address',
      type: 'text',
      startIcon: ' ',
      label: 'Street Address',
      value: '',
      validate: ['required'],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      name: 'dwelling_type',
      type: 'selectbox',
      label: 'Dwelling Type',
      value: '',
      options: [
        {
          label: 'Office',
          value: '1',
        },
        {
          label: 'Apartment',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 2,
      },
    },
    {
      name: 'address',
      type: 'text',
      startIcon: '',
      label: 'Address Line 2',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      name: 'state',
      type: 'selectbox',
      startIcon: ' ',
      label: 'State',
      value: '',
      validate: [],
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      name: 'zipcode',
      type: 'text',
      startIcon: ' ',
      label: 'Zipcode',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      maskType: 'zipcode',
    },
    {
      name: 'city',
      type: 'selectbox',
      startIcon: ' ',
      label: 'City',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      name: 'country',
      type: 'selectbox',
      startIcon: ' ',
      label: 'Country',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
    },
    {
      type: 'hr',
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      type: 'subHeading',
      label: 'Phone and Email',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'celphonenumber',
      type: 'text',
      startIcon: ' ',
      label: 'Cell Phone Number',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      maskType: 'phone',
    },
    {
      name: 'homephonenumber',
      type: 'text',
      startIcon: ' ',
      label: 'Home Phone Number',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      maskType: 'phone',
    },
    {
      name: 'tty',
      type: 'text',
      startIcon: ' ',
      label: 'TTY',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
    },
    {
      name: 'emailaddress',
      type: 'text',
      startIcon: ' ',
      label: 'Email Address',
      value: '(000)000-0000',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
  ],
  mailingAddressSelected: [
    {
      type: 'subHeading',
      label: 'Mailing Address',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'street_address',
      type: 'text',
      startIcon: '',
      label: 'Street Address',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      name: 'dwelling_type',
      type: 'selectbox',
      startIcon: ' ',
      label: 'Dwelling Type',
      value: '',
      validate: [],
      options: [
        {
          label: 'Home',
          value: '1',
        },
        {
          label: 'Aprtment',
          value: '2',
        },
      ],
      grid: {
        row: false,
        col: 2,
      },
    },
    {
      name: 'mailing_address',
      type: 'text',
      startIcon: '',
      label: 'Address Line 2',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
    {
      name: 'state',
      type: 'selectbox',
      startIcon: '',
      label: 'State',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      name: 'zipcode',
      type: 'text',
      startIcon: '',
      label: 'Zipcode',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      maskType: 'zipcode',
    },
    {
      name: 'city',
      type: 'selectbox',
      startIcon: ' ',
      label: 'City',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
    },
    {
      name: 'country',
      type: 'selectbox',
      startIcon: '',
      label: 'County of Residence',
      value: '',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
    },
  ],
};
