import { fields, searchData } from './defaults';

// ------------------------------------
// Constants
// ------------------------------------
export const SEARCH_MEMBER = 'SEARCH_MEMBER';

const update = (state, mutations) => Object.assign({}, state, mutations);
const initSearchData = () => {
  let searchStack = searchData;
  searchStack = update(searchStack, { data: [] });
  return searchStack;
};

// ------------------------------------
// Actions
// ------------------------------------

export function searchMembers(text) {
  return {
    type: SEARCH_MEMBER,
    payload: text,
  };
}

export const actions = {
  searchMembers,
};

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SEARCH_MEMBER]: (state, action) => {
    let searchStack = searchData;
    action.payload = action.payload.trim();

    searchStack = update(searchStack, {
      data: searchStack.data.filter(
        o =>
          o.firstName.toLowerCase().indexOf(action.payload.toLowerCase()) !== -1 ||
          o.ssn === action.payload,
      ),
    });
    state = update(state, { members: searchStack });
    return state;
  },
};

/**
 * Reducer
 */

const initialState = {
  fields,
  members: initSearchData(),
};

export default function indvInfoReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
