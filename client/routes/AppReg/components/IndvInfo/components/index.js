/**
 * APPLICATION REGISTRATION - INDIVIUAL INFO FORM
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesSearch from 'client/components/common/BridgesSearch';
import DataTable from 'client/components/common/BridgesTable';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
// UTILITY DEPENDENCIES
import paths from 'client/routes/urls';
import arPaths from 'client/routes/AppReg/urls';

const styles = theme => ({
  ...pageStyles,
  ...formStyles,
  subHeadingBottomSpace: {
    marginTop: 16,
    marginBottom: 0,
  },
});
const data = {
  firstname: 'John',
  middlename: 'Middle',
  lastname: 'Doe',
  gender: 'f',
  snn: '0000000000',
  dob: Date.parse('Thu, 01 Jan 1970 07:10:00'),
};

class IndvInfo extends React.Component {
  state = {
    btnflg: false,
  };

  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.appInfo.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: arPaths.fileClearance.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = arPaths.contactInfo.title;
  }

  updateEnableDisableBtn = value => {
    this.setState({
      btnflg: value,
    });
  };

  handleSubmit = values => {
    console.log('Submitted====> ', values);
  };

  fetchResults = text => {
    this.props.searchMembers(text);
  };

  render() {
    const { classes, fields } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Basic Individual Details
            </Typography>
          </Grid>
          <form className={classes.dataForm} onSubmit={this.handleSubmit}>
            <BridgesPaper elevate={1} roundcorner>
              <Grid container>
                <Grid item md={12}>
                  <BridgesSearch onSearch={this.fetchResults} />
                </Grid>
              </Grid>

              <Divider className={classes.dividerStyle} />

              <BridgesFormEleCollapse stack={this.props.searchResults.data} dividerFlag>
                <Grid container spacing={24}>
                  <Grid item xs={12}>
                    <Typography
                      variant="display4"
                      className={classNames(classes.subHeading, classes.subHeadingBottomSpace)}
                    >
                      Select a Person
                      <span className={classes.headerUnderline} />
                    </Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <DataTable
                      tableData={this.props.searchResults}
                      updateEnableDisableBtn={this.updateEnableDisableBtn}
                    />
                  </Grid>
                </Grid>
              </BridgesFormEleCollapse>

              <BridgesDynamicFields fields={fields} />
            </BridgesPaper>

            <BridgesFormActions actions={this.actions} />
          </form>
        </Grid>
      </div>
    );
  }
}

IndvInfo.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.array.isRequired,
  // FUNCTION CATERS USER WITH SEARCH RESULTS
  searchMembers: PropTypes.func.isRequired,
  // OBJECT HOLDS DATA TO BE POPULATED INTO TABLE
  searchResults: PropTypes.object,
};

let IndvInfoForm = reduxForm({
  form: 'indv-info',
})(IndvInfo);

IndvInfoForm = connect(state => ({
  initialValues: data,
}))(IndvInfoForm);

export default withStyles(styles)(IndvInfoForm);
