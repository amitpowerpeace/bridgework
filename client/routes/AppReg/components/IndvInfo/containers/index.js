import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { injectAsyncReducers } from 'client/store';
import indvInfoReducer from 'client/routes/AppReg/components/IndvInfo/modules';
import IndvInfo from '../components';
import { searchMembers } from '../modules';

// inject reducers
injectAsyncReducers({
  indvInfo: indvInfoReducer,
});

export const getIndvInfo = state => {
  return state.indvInfo || {};
};

const getFields = createSelector([getIndvInfo], data => data.fields);
const searchResults = createSelector([getIndvInfo], data => data.members);

const mapStateToProps = state => ({
  fields: getFields(state),
  searchResults: searchResults(state),
});

const mapDispatchToProps = {
  searchMembers,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IndvInfo);
