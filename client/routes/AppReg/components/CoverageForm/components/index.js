/**
 *  HEALTH COVERAGE FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class Coverage extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.medicalBills.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.coverageFromJobs.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.coverage.title;
  }

  render() {
    const {
      classes,
      handleSubmit,
      fields,
      helprequiredForMedicalBills,
      anyoneunderhealthCoverage,
      anyoneunderHealthCovOptions,
      autorenewalhealthCoverage,
      anyoneunderhealthCovegeOpt,
      autorenewalhealthCoverageOptions,
    } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Healthcare Coverage
            </Typography>
          </Grid>
        </Grid>
        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Coverage
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
            </Grid>

            <BridgesDynamicFields fields={fields && fields.anyoneunderhealthCoverage} />
            <BridgesFormEleCollapse stack={anyoneunderhealthCoverage} needle={'y'} dividerFlag>
              <BridgesMemberDetails
                members={fields && fields.anyoneunderHealthCovOptions[0]}
                details={fields && fields.anyoneunderhealthCovegeOpt}
              />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.autorenewalhealthCoverage} />
            <BridgesFormEleCollapse stack={autorenewalhealthCoverage} needle={'y'} dividerFlag>
              <BridgesMemberDetails
                members={fields.autorenewalhealthCoverageOptions[0]}
                details={''}
              />
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

Coverage.propTypes = {
  anyoneunderhealthCoverage: PropTypes.any,
  autorenewalhealthCoverage: PropTypes.any,
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  helprequiredForMedicalBills: PropTypes.any,
};

let CoverageForm = reduxForm({
  form: 'coverage-info-form',
})(Coverage);

const selector = formValueSelector('coverage-info-form');
CoverageForm = connect(state => {
  const {
    primaryCaretaker,
    subHeadingCoverage,
    helprequiredForMedicalBills,
    anyoneunderhealthCoverage,
    autorenewalhealthCoverage,
    anyoneunderHealthCovOptions,
    anyoneunderhealthCovegeOpt,
    autorenewalhealthCoverageOptions,
  } = selector(
    state,
    'primaryCaretaker',
    'helprequiredForMedicalBills',
    'anyoneunderhealthCoverage',
    'autorenewalhealthCoverage',
    'subHeadingCoverage',
    'anyoneunderHealthCovOptions',
    'anyoneunderhealthCovegeOpt',
    'autorenewalhealthCoverageOptions',
  );
  return {
    primaryCaretaker,
    helprequiredForMedicalBills,
    anyoneunderhealthCoverage,
    autorenewalhealthCoverage,
    subHeadingCoverage,
    anyoneunderHealthCovOptions,
    anyoneunderhealthCovegeOpt,
    autorenewalhealthCoverageOptions,
  };
})(CoverageForm);

export default withStyles(styles)(CoverageForm);
