import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import coverageReducer from 'client/routes/AppReg/components/CoverageForm/modules';
import CoverageForm from '../components';
import { getCoverageFields } from '../modules';

// inject reducers
injectAsyncReducers({
  coverageReducer,
});

const mapStateToProps = state => ({
  fields: getCoverageFields(state),
});

export default connect(mapStateToProps)(CoverageForm);
