// import { createSelector } from 'reselect';
import { fields } from './defaults';

const CoverageData = {
  fields,
};

export const getCoverageFields = state => state.coverageReducer.fields || [];

/**
 * Reducer
 */
export default function coverageReducer(state = CoverageData) {
  return state;
}
