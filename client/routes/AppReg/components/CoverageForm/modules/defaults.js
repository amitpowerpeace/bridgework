export const fields = {
  subHeadingCoverage: [
    {
      type: 'subHeading',
      label: 'Coverage',
      underline: true,
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneunderhealthCoverage: [
    {
      name: 'anyoneunderhealthCoverage',
      type: 'togglebox',
      label: 'Is anyone currently enrolled in health coverage (even if not applying)?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  anyoneunderHealthCovOptions: [
    {
      name: 'anyoneunderHealthCovOptions',
      type: 'checkboxgroup',
      label: 'Who?',
      display: 'vertical',
      options: [
        {
          label: 'Leona Ramsay 40M',
          value: 'leona',
        },
        {
          label: 'Owen Ramsay 36F',
          value: 'owen',
        },
        {
          label: 'Nathan Ramsay 10M',
          value: 'Nathan',
        },
        {
          label: 'Mollie Ramsay 13F',
          value: 'Mollie',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],
  anyoneunderhealthCovegeOpt: [
    {
      name: 'typecoverage',
      type: 'text',
      startIcon: '',
      label: 'Type and Name of Coverage',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'policycoverage',
      type: 'text',
      startIcon: '',
      label: 'Policy Number',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: true,
    },

    {
      name: 'additionalIncomesource',
      type: 'checkboxgroup',
      label: ' ',
      compact: false,
      options: [
        {
          label: 'Medicare',
          value: 'Medicare',
        },
        {
          label: 'COBRA',
          value: 'COBRA',
        },
        {
          label: 'Retiree Health Plan',
          value: 'Retiree Health Plan',
        },
        {
          label: 'Limited Benefit Plan',
          value: 'Limited Benefit Plan',
        },
        {
          label: 'Need Help Paying Medicare Premiums',
          value: 'Need Help Paying Medicare Premiums',
        },
      ],
      errorMessage: '',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Coverage',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],

  autorenewalhealthCoverage: [
    {
      name: 'autorenewalhealthCoverage',
      type: 'togglebox',
      label:
        'To make it easier to determine healthcare eligibility in future years can IRS data be used for automatic renewals?',
      checked: false,
      toggleStack: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  autorenewalhealthCoverageOptions: [
    {
      name: 'years',
      type: 'radiogroup',
      label: 'For how many years?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: '5',
          value: '5',
        },
        {
          label: '4',
          value: '4',
        },
        {
          label: '3',
          value: '3',
        },
        {
          label: '2',
          value: '2',
        },
        {
          label: '1',
          value: '1',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
      grouped: false,
    },
  ],
};
