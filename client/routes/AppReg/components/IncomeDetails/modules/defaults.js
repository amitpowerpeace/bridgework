export const fields = {
  isanyoneEmployed: [
    {
      name: 'isanyoneEmployed',
      type: 'togglebox',
      label: 'Is anyone employed?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  selfEmployed: [
    {
      name: 'selfEmployed',
      type: 'togglebox',
      label: 'Is anyone self-employed?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  additionalIncome: [
    {
      name: 'additionalIncome',
      type: 'togglebox',
      label: 'Does anyone have additional income?',
      toggleStack: [
        {
          title: 'Yes',
          value: 'y',
        },
        {
          title: 'No',
          value: 'n',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  people: [
    {
      name: 'people',
      type: 'checkboxgroup',
      label: 'Who ?',
      display: 'vertical',
      compact: false,
      options: [
        {
          label: 'Leone Ramsey 40M',
          value: 'Leone Ramsey 40M',
        },
        {
          label: 'Owen Ramsey 36F',
          value: 'Owen Ramsey 36F',
        },
        {
          label: 'Nathen Ramsey 10M',
          value: 'Nathen Ramsey 10M',
        },
        {
          label: 'Molley Ramsey 13F',
          value: 'Molley Ramsey 13F',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
      grouped: true,
    },
  ],
  isanyoneEmployedOptions: [
    {
      name: 'employerName',
      type: 'text',
      startIcon: '',
      label: 'Employer Name',
      value: '',
      validate: ['required'],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Delete Employment',
      buttonIcon: 'delete',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      buttonColor: 'red',
      buttonDynamic: true,
      grid: {
        row: false,
        col: 7,
      },
    },
    {
      name: 'averageHoursPerWeek',
      type: 'timepicker',
      startIcon: '',
      label: 'Average Hours Per Week',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'wageortips',
      type: 'text',
      startIcon: '&#36;',
      label: 'Wages/Tips (Before Tax)',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'per',
      type: 'selectbox',
      label: 'Per',
      options: [
        {
          label: 'one',
          value: '1',
        },
        {
          label: 'two',
          value: '2',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Employment',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  selfEmployedOptions: [
    {
      name: 'typeOfWork',
      type: 'text',
      startIcon: '',
      label: 'Type of work',
      value: '',
      validate: ['required'],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Delete Self-Employment',
      buttonIcon: 'delete',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      buttonColor: 'red',
      grid: {
        row: false,
        col: 7,
      },
    },
    {
      name: 'monthlyIncome',
      type: 'text',
      startIcon: '&#36;',
      label: 'Monthly Income (Before Expenses)',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 4,
      },
      grouped: true,
    },
    {
      name: 'expenses',
      type: 'text',
      startIcon: '&#36;',
      label: 'Expenses',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Self-Employment',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  additionalIncomeOptions: [
    {
      name: 'typeOfIncome',
      type: 'timepicker',
      startIcon: '',
      label: 'Type of Income',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
      grouped: true,
    },
    {
      name: 'amountReceived',
      type: 'text',
      startIcon: '&#36;',
      label: 'Amount Received',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: 'per',
      type: 'selectbox',
      label: 'Per',
      options: [
        {
          label: 'Day',
          value: '1',
        },
        {
          label: 'Week',
          value: '2',
        },
        {
          label: 'Monthly',
          value: '3',
        },
      ],
      validate: [],
      grid: {
        row: false,
        col: 3,
      },
      grouped: false,
    },
    {
      name: '',
      type: 'bridgesbutton',
      title: 'Add Another Income',
      buttonIcon: '',
      iconAlign: '',
      buttonType: 'squarebtn',
      buttonPos: 'right',
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'remainderInformation',
      type: 'singlecheckbox',
      label: 'Remainder of information not provided',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  infoMissingFromApplication: [
    {
      name: 'infoMissingFromApplication',
      type: 'singlecheckbox',
      label: 'Some information from the application is missing',
      hightlight: true,
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
};
