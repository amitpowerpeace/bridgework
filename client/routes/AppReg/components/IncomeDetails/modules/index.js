// import { createSelector } from 'reselect';
import { fields } from './defaults';

const incomeDetailsData = {
  fields,
};

export const getIncomeDetailsFields = state => state.incomeDetailsReducer.fields || [];

/**
 * Reducer
 */
export default function incomeDetailsReducer(state = incomeDetailsData) {
  return state;
}
