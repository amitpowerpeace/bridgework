import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import IncomeDetails from '../components';
import { getIncomeDetailsFields } from '../modules';
import incomeDetailsReducer from 'client/routes/AppReg/components/IncomeDetails/modules';

// inject reducers
injectAsyncReducers({
  incomeDetailsReducer,
});

const mapStateToProps = state => ({
  fields: getIncomeDetailsFields(state),
});

export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(IncomeDetails);
