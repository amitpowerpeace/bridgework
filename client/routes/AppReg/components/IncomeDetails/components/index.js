/**
 * APPLICATION REGISTRATION - INCOME DETAILS FORM
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesMemberDetails from 'client/components/common/BridgesMemberDetails';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
// UTILITY DEPENDENCIES
import paths from 'client/routes/AppReg/urls';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
});

class IncomeDetails extends React.Component {
  actions = [
    {
      title: 'Back',
      type: 'back',
      url: paths.assets.url,
      align: 'left',
    },
    {
      title: 'Next',
      type: 'next',
      url: paths.expense.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.incomeDetails.title;
  }

  render() {
    const {
      classes,
      handleSubmit,
      fields,
      isanyoneEmployed,
      additionalIncome,
      selfEmployed,
    } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Income Details
            </Typography>
          </Grid>
        </Grid>

        <form className={classes.dataForm} onSubmit={handleSubmit}>
          <BridgesPaper elevate={1} roundcorner>
            <BridgesDynamicFields fields={fields.isanyoneEmployed} />
            <BridgesFormEleCollapse stack={isanyoneEmployed} needle={'y'} dividerFlag>
              <BridgesMemberDetails
                members={fields.people[0]}
                details={fields.isanyoneEmployedOptions}
              />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.selfEmployed} />
            <BridgesFormEleCollapse stack={selfEmployed} needle={'y'} dividerFlag>
              <BridgesMemberDetails
                members={fields.people[0]}
                details={fields.selfEmployedOptions}
              />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.additionalIncome} />
            <BridgesFormEleCollapse stack={additionalIncome} needle={'y'}>
              <BridgesMemberDetails
                members={fields.people[0]}
                details={fields.additionalIncomeOptions}
              />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.infoMissingFromApplication} />
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

IncomeDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

let IncomeDetailsForm = reduxForm({
  form: 'incomedetails-info-form',
})(IncomeDetails);

const selector = formValueSelector('incomedetails-info-form');
IncomeDetailsForm = connect(state => {
  const { isanyoneEmployed, additionalIncome, selfEmployed } = selector(
    state,
    'isanyoneEmployed',
    'additionalIncome',
    'selfEmployed',
  );
  return {
    isanyoneEmployed,
    additionalIncome,
    selfEmployed,
  };
})(IncomeDetailsForm);

export default withStyles(styles)(IncomeDetailsForm);
