/**
 * App Registration Component
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Route, Switch } from 'react-router-dom';
import urls from 'client/routes/AppReg/urls';
// Forms
import IndvInfoForm from 'client/routes/AppReg/components/IndvInfo';
import FileClearanceForm from 'client/routes/AppReg/components/FileClearance';
import CaseAssociationForm from 'client/routes/AppReg/components/CaseAssociationForm';
import ContactInformationForm from 'client/routes/AppReg/components/ContactInfo';
import ProgramSelectionForm from 'client/routes/AppReg/components/ProgramSelection';
import FoodAssistanceForm from 'client/routes/AppReg/components/FoodAssistance';
import ChildDevelopmentCareForm from 'client/routes/AppReg/components/ChildDevCare';
import FinalSummaryForm from 'client/routes/AppReg/components/FinalSummary';
import HouseholdMembersForm from 'client/routes/AppReg/components/HouseholdMembers';
import HouseholdDetailsForm from 'client/routes/AppReg/components/HouseholdDetails';
import AssetsForm from 'client/routes/AppReg/components/AssetDetails';
import IncomeDetailsForm from 'client/routes/AppReg/components/IncomeDetails';
import ExpensesForm from 'client/routes/AppReg/components/Expenses';
import FinalDetailsForm from 'client/routes/AppReg/components/FinalDetails';
import MedicalBillsForm from 'client/routes/AppReg/components/MedicalBills';
import CoverageForm from 'client/routes/AppReg/components/CoverageForm';
import CoverageFromJobs from 'client/routes/AppReg/components/CoverageFromJobs';
import AdditionalGroupDetailsForm from 'client/routes/AppReg/components/AdditionalGroupDetails';
import NativeOriginForm from 'client/routes/AppReg/components/NativeOrigin';
import WaterFlintCaseForm from 'client/routes/AppReg/components/FlintWaterCase';
import DependentsIncomeForm from 'client/routes/AppReg/components/DependentIncome';

const styles = theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
  },
});

class AppReg extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Switch>
          <Route path={urls.indvInfo.url} component={IndvInfoForm} exact />
          <Route path={urls.fileClearance.url} component={FileClearanceForm} />
          <Route path={urls.caseAssociation.url} component={CaseAssociationForm} />
          <Route path={urls.contactInfo.url} component={ContactInformationForm} />
          <Route path={urls.programSelection.url} component={ProgramSelectionForm} />
          <Route path={urls.houseMember.url} component={HouseholdMembersForm} />
          <Route path={urls.houseDetail.url} component={HouseholdDetailsForm} />
          <Route path={urls.assets.url} component={AssetsForm} />
          <Route path={urls.incomeDetails.url} component={IncomeDetailsForm} />
          <Route path={urls.expense.url} component={ExpensesForm} />
          <Route path={urls.finalDetails.url} component={FinalDetailsForm} />
          <Route path={urls.additionalGroupDetails.url} component={AdditionalGroupDetailsForm} />
          <Route path={urls.nativeOrigin.url} component={NativeOriginForm} />
          <Route path={urls.waterFlintCase.url} component={WaterFlintCaseForm} />
          <Route path={urls.dependentIncome.url} component={DependentsIncomeForm} />
          <Route path={urls.medicalBills.url} component={MedicalBillsForm} />
          <Route path={urls.coverage.url} component={CoverageForm} />
          <Route path={urls.coverageFromJobs.url} component={CoverageFromJobs} />
          <Route path={urls.foodAssist.url} component={FoodAssistanceForm} />
          <Route path={urls.childDevCare.url} component={ChildDevelopmentCareForm} />
          <Route path={urls.finalSummary.url} component={FinalSummaryForm} />
        </Switch>
      </div>
    );
  }
}

AppReg.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppReg);
