const urls = {
  indvInfo: {
    title: 'Individual Info',
    url: '/register',
  },
  fileClearance: {
    title: 'File Clearance',
    url: '/register/file-clearance',
  },
  caseAssociation: {
    title: 'Case Association',
    url: '/register/case-association',
  },
  contactInfo: {
    title: 'Contact Information',
    url: '/register/contact-information',
  },
  programSelection: {
    title: 'Program Selection',
    url: '/register/program-selection',
  },
  houseMember: {
    title: 'Household Member',
    url: '/register/house-member',
  },
  houseDetail: {
    title: 'Household Details',
    url: '/register/house-details',
  },
  assets: {
    title: 'Assets',
    url: '/register/assets',
  },
  incomeDetails: {
    title: 'Income Details',
    url: '/register/income-details',
  },
  expense: {
    title: 'Expense',
    url: '/register/expense',
  },
  finalDetails: {
    title: 'Final Details',
    url: '/register/final-details',
  },
  additionalGroupDetails: {
    title: 'Additional Details',
    url: '/register/health-coverage/additional-group-details',
  },
  nativeOrigin: {
    title: 'Native Origin',
    url: '/register/health-coverage/native-origin',
  },
  waterFlintCase: {
    title: 'Flint Water Case',
    url: '/register/health-coverage/water-flint-case',
  },
  dependentIncome: {
    title: 'Dependent Income',
    url: '/register/health-coverage/dependents-income',
  },
  medicalBills: {
    title: 'Medical Bills',
    url: '/register/health-coverage/medical-bills',
  },
  coverage: {
    title: 'Coverage',
    url: '/register/health-coverage/coverage',
  },
  coverageFromJobs: {
    title: 'Coverage from Jobs',
    url: '/register/health-coverage/coverageFromJobs',
  },
  foodAssist: {
    title: 'Food Assistence',
    url: '/register/food-assistance',
  },
  childDevCare: {
    title: 'child Development Care',
    url: '/register/child-development-care',
  },
  finalSummary: {
    title: 'Final Summary',
    url: '/register/final-summary',
  },
};
export default urls;
