/**
 *  Wires in the actions and state
 *  necessary to render a presentational component
 *
 */

import { connect } from 'react-redux';
import { injectAsyncReducers } from 'client/store';
import counterReducer from 'client/routes/Help/modules/counter';
import { increment, doubleAsync } from '../modules/counter';
import Counter from '../components';

// inject reducers
injectAsyncReducers({
  counter: counterReducer,
});

const mapDispatchToProps = {
  increment: () => increment(1),
  doubleAsync,
};

const mapStateToProps = state => ({
  counter: state.counter,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Counter);
