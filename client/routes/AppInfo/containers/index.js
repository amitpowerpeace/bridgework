import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { injectAsyncReducers } from 'client/store';
import appInfoReducer from 'client/routes/AppInfo/modules';
import AppInfo from 'client/routes/appInfo/components';

// inject reducers
injectAsyncReducers({
  appInfo: appInfoReducer,
});

export const getAppInfo = state => {
  return state.appInfo || {};
};

const getFields = createSelector(getAppInfo, data => data.fields);
const getTabData = createSelector(getAppInfo, data => data.tabData);

const mapStateToProps = state => ({
  fields: getFields(state),
  tabData: getTabData(state),
});

export default connect(mapStateToProps)(AppInfo);
