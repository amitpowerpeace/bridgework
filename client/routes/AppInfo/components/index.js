/**
 * App Registration Component
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesTab from 'client/components/common/BridgesTabs';
import BridgesFormActions from 'client/components/common/BridgesFormActions';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
// REDUX DEPENDENCIES
import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import paths from 'client/routes/urls';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  dividerStyle: {
    margin: '12px 0px',
  },
});

const data = {
  dateofregistration: Date.parse('Thu, 29 Jun 2015 09:05:03'),
  timeofregistration: Date.parse('Thu, 29 Jun 2015 09:05:03'),
  source: 'lorem ipsum',
  writtenlanguage: 'other',
  spokenlanguage: '',
  interpreter: ['ar', 'sl', 'other'],
  otherlang: 'espanol',
};

class AppInfo extends React.Component {
  actions = [
    {
      title: 'Next',
      type: 'next',
      url: paths.appReg.url,
      align: 'right',
    },
  ];

  // Set page title
  componentDidMount() {
    document.title = paths.appInfo.title;
  }

  render() {
    const { classes, fields, tabData, communication_assistance, spokenlanguage } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="title" className={classes.masterHeading} gutterBottom={false}>
              Application Information
            </Typography>
          </Grid>
        </Grid>

        <form onSubmit={() => console.log('submitted')}>
          <BridgesPaper elevate={1} roundcorner>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  Type of Application
                  <span className={classes.headerUnderline} />
                </Typography>
                <BridgesTab headerData={tabData} />
              </Grid>
            </Grid>
            <Divider className={classes.dividerStyle} />

            <BridgesDynamicFields fields={fields.primary} />

            <BridgesDynamicFields fields={fields.spokenLanguage} />
            <BridgesFormEleCollapse stack={spokenlanguage} needle={'en'} dividerFlag={false}>
              <BridgesDynamicFields fields={fields.spokenLanguageDropdown} />
            </BridgesFormEleCollapse>

            <BridgesDynamicFields fields={fields.communicationAssistance} />
            <BridgesFormEleCollapse
              stack={communication_assistance}
              needle={'other'}
              dividerFlag={false}
            >
              <BridgesDynamicFields fields={fields.communicationAssistanceDropdown} />
            </BridgesFormEleCollapse>
          </BridgesPaper>

          <BridgesFormActions actions={this.actions} />
        </form>
      </div>
    );
  }
}

AppInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};

let AppInfoForm = reduxForm({
  form: 'app-reg-form',
})(AppInfo);

const selector = formValueSelector('app-reg-form');
AppInfoForm = connect(state => {
  const { communication_assistance, spokenlanguage } = selector(
    state,
    'communication_assistance',
    'spokenlanguage',
  );
  const initialValues = data;
  return {
    communication_assistance,
    spokenlanguage,
    initialValues,
  };
})(AppInfoForm);

export default withStyles(styles)(AppInfoForm);
