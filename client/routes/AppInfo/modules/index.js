// import { createSelector } from 'reselect';
import { tabData, fields } from './defaults';

/**
 * Reducer
 */

const initialState = {
  tabData,
  fields,
};
export default function appInfoReducer(state = initialState) {
  return state;
}
