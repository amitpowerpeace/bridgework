export const tabData = [
  {
    tabName: '1171',
    tabTitle: '1171',
    tabData: [
      {
        name: '1171Form',
        label: '',
        type: 'checkbuttongroup',
        spreadType: 'vertical',
        viewType: 'normal',
        viewTypeTitle: '',
        options: [
          {
            label: 'Self-Service Application',
            value: 'Self-Service Application',
          },
          {
            label: '1171',
            value: '1171',
          },
          {
            label: '1171-SP',
            value: '1171-SP',
          },
          {
            label: '1171-AR',
            value: '1171-AR',
          },
          {
            label: '1171 (Filling Form)',
            value: '1171 (Filling Form)',
          },
          {
            label: '1171-SP (Filling Form)',
            value: '1171-SP (Filling Form)',
          },
          {
            label: '1171-AR (Filling Form)',
            value: '1171-AR (Filling Form)',
          },
        ],
        validate: [],
        grid: {
          row: true,
          col: 12,
        },
        grouped: false,
      },
    ],
  },
  {
    tabName: 'ser',
    tabTitle: 'SER',
    tabData: [
      {
        name: 'ser',
        label: '',
        type: 'checkbuttongroup',
        spreadType: 'vertical',
        viewType: 'normal',
        viewTypeTitle: '',
        options: [
          {
            label: '1514',
            value: '1514',
          },
          {
            label: '1514 - SP',
            value: '1514 - SP',
          },
        ],
        required: 'false',
        errorMessage: '',
        grid: {
          row: true,
          col: 12,
        },
      },
    ],
  },
  {
    tabName: 'cdc',
    tabTitle: 'CDC',
    tabData: [
      {
        name: 'cdc',
        label: '',
        type: 'checkbuttongroup',
        spreadType: 'vertical',
        viewType: 'normal',
        viewTypeTitle: '',
        options: [
          {
            label: '4583  CDC',
            value: '1',
          },
          {
            label: '4583 Simplified',
            value: '4583 Simplified',
          },
          {
            label: '4583 Flint Emergency',
            value: '4583 Flint Emergency',
          },
        ],
        required: 'false',
        errorMessage: '',
        grid: {
          row: true,
          col: 12,
        },
      },
    ],
  },
  {
    tabName: 'hcc',
    tabTitle: 'HCC',
    tabData: [
      {
        name: 'hcc',
        label: '',
        type: 'checkbuttongroup',
        spreadType: 'vertical',
        viewType: 'normal',
        viewTypeTitle: '',
        options: [
          {
            label: '1426',
            value: '1426',
          },
          {
            label: '4574 LTC',
            value: '4574 LTC',
          },
          {
            label: '3243 Retro',
            value: '3243 Retro',
          },
          {
            label: '1088 BCCP',
            value: '1088 BCCP',
          },
          {
            label: '2565 Newborn',
            value: '2565 Newborn',
          },
          {
            label: 'SSI Medicaid',
            value: 'SSI Medicaid',
          },
          {
            label: 'Adoption Subsidy Medicaid',
            value: 'Adoption Subsidy Medicaid',
          },
          {
            label: 'Foster Care Medicaid',
            value: 'Foster Care Medicaid',
          },
          {
            label: 'Data Collection Tool (DCT)',
            value: 'Data Collection Tool (DCT)',
          },
          {
            label: 'Federally Facilitated Marketplace (FFM)',
            value: 'Federally Facilitated Marketplace (FFM)',
          },
          {
            label: 'Healthy Kids',
            value: 'Healthy Kids',
          },
          {
            label: 'MSA - 1582 Plan First',
            value: 'MSA - 1582 Plan First',
          },
          {
            label: 'Refugee Unaccompanied Minor',
            value: 'Refugee Unaccompanied Minor',
          },
          {
            label: 'Presumptive Eligibility (PE)',
            value: 'Presumptive Eligibility (PE)',
          },
        ],
        required: 'false',
        errorMessage: '',
        grid: {
          row: true,
          col: 12,
        },
      },
    ],
  },
  {
    tabName: 'iaa',
    tabTitle: 'IAA',
    tabData: [
      {
        name: 'iaa',
        label: '',
        type: 'checkbuttongroup',
        spreadType: 'vertical',
        viewType: 'normal',
        viewTypeTitle: '',
        options: [
          {
            label: '4574 - B Assets Declaration',
            value: '4574 - B Assets Declaration',
          },
        ],
        required: 'false',
        errorMessage: '',
        grid: {
          row: true,
          col: 12,
        },
      },
    ],
  },
  {
    tabName: 'other',
    tabTitle: 'Others',
    tabData: [
      {
        name: 'other',
        label: '',
        type: 'checkbuttongroup',
        spreadType: 'vertical',
        viewType: 'normal',
        viewTypeTitle: '',
        options: [
          {
            label: 'Request for Health Care Coverage (HCC) Applicant',
            value: 'Request for Health Care Coverage (HCC) Applicant',
          },
          {
            label:
              'Request for Health Care Coverage (HCC) Applicant - Patient of a Nursing Facility',
            value:
              'Request for Health Care Coverage (HCC) Applicant - Patient of a Nursing Facility',
          },
          {
            label: 'Request for 1171',
            value: 'Request for 1171',
          },
          {
            label: 'Preventative Services for Families (PSF)',
            value: 'Preventative Services for Families (PSF)',
          },
          {
            label: 'Migrant Outreach',
            value: 'Migrant Outreach',
          },
          {
            label: 'Indian Outreach',
            value: 'Indian Outreach',
          },
          {
            label: 'Direct Support Services',
            value: 'Direct Support Services',
          },
          {
            label: 'MiCAP',
            value: 'MiCAP',
          },
          {
            label: '3220 Disaster Cash and Food Assistance',
            value: '3220 Disaster Cash and Food Assistance',
          },
        ],
        required: 'false',
        errorMessage: '',
        grid: {
          row: true,
          col: 12,
        },
      },
    ],
  },
];

export const fields = {
  primary: [
    {
      name: 'source',
      type: 'radio',
      label: 'Source',
      spreadType: 'horizontal',
      options: [
        {
          label: ['Paper Form'],
          value: 'Paper Form',
          name: 'Paper Form',
        },
        {
          label: ['Mail-in'],
          value: 'Mail-in',
          name: 'Mail-in',
        },
        {
          label: ['Drop-off'],
          value: 'Drop-off',
          name: 'Drop-off',
        },
        {
          label: ['ISD'],
          value: 'ISD',
          name: 'ISD',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
    },
    {
      name: 'dateofregistration',
      type: 'datepicker',
      label: 'Date of Birth',
      value: '22-02-2018',
      validate: ['required'],
      grid: {
        row: true,
        col: 3,
      },
    },
    {
      name: 'timeofregistration',
      type: 'timepicker',
      label: 'Time Received',
      value: '05:25 PM',
      validate: ['required'],
      grid: {
        row: false,
        col: 2,
      },
    },
    {
      name: 'written_language',
      type: 'selectbox',
      label: 'Written Language',
      value: '',
      options: [
        {
          label: 'english',
          value: '1',
        },
        {
          label: 'espanol',
          value: '2',
        },
        {
          label: 'hindi',
          value: '3',
        },
        {
          label: 'kannada',
          value: '4',
        },
      ],
      validate: ['required'],
      grid: {
        row: true,
        col: 3,
      },
    },
    {
      name: 'written_language_other',
      type: 'text',
      startIcon: '',
      label: 'Other',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 2,
      },
    },
  ],
  communicationAssistance: [
    {
      name: 'communication_assistance',
      type: 'radio',
      label: 'Does anyone need communication assistance?',
      spreadType: 'horizontal',
      options: [
        {
          label: 'Interpreter',
          value: 'Interpreter',
        },
        {
          label: 'Sign Language',
          value: 'Sign Language',
        },
        {
          label: 'Listening Device',
          value: 'Listening Device',
        },
        {
          label: 'Other',
          value: 'other',
        },
      ],
      validate: [],
      errorMessage: '',
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  communicationAssistanceDropdown: [
    {
      name: 'otherLanguage',
      type: 'text',
      startIcon: '',
      label: 'Other',
      value: '',
      validate: [],
      grid: {
        row: true,
        col: 5,
      },
    },
  ],
  spokenLanguage: [
    {
      name: 'spokenlanguage',
      type: 'singlecheckbox',
      label: 'Is the spoken language different?',
      checked: false,
      iconColor: '#4AA564',
      validate: [],
      grid: {
        row: true,
        col: 12,
      },
    },
  ],
  spokenLanguageDropdown: [
    {
      name: 'spoken_language',
      type: 'selectbox',
      label: 'Spoken Language',
      value: '',
      options: [
        {
          label: 'english',
          value: '1',
        },
        {
          label: 'espanol',
          value: '2',
        },
        {
          label: 'hindi',
          value: '3',
        },
        {
          label: 'kannada',
          value: '4',
        },
      ],
      validate: [],
      grid: {
        row: true,
        col: 3,
      },
    },
    {
      name: 'spoken_language_other',
      type: 'text',
      startIcon: '',
      label: 'Other',
      value: '',
      validate: [],
      grid: {
        row: false,
        col: 2,
      },
    },
  ],
};
