/**
 * Data Collect Component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
// REDUX DEPENDENCIES
import { reduxForm } from 'redux-form';
import { Values } from 'redux-form-website-template';
// COMPONENT DEPENDENCIES 
import MembersArrayForm from 'client/components/common/BridgesMemberDetails/BridgesMemberArray';
// THEME STYLES
import palette from 'client/styles/base/palette';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import paths from 'client/routes/urls';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  root: {
    flexGrow: 1,
  },
});

class DataCollect extends React.Component {
  handleSubmit = values => {
    window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
  };
  // Set page title
  componentDidMount() {
    document.title = paths.dataCollect.title;
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.divFullWidth}> 
        <Typography variant="headline" style={{ color: palette.primary.brand }}>
          Data Collection
        </Typography>

        <h2>Member Array</h2>
        <MembersArrayForm onSubmit={this.handleSubmit} />
        <Values form="fieldArrays" />
      </div>
    );
  }
}

const DataCollectForm = reduxForm({
  form: 'program-select-form',
})(DataCollect);

export default withStyles(styles)(DataCollectForm);
