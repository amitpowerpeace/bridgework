import { connect } from 'react-redux';
import AppLoginForm from '../components';

const mapStateToProps = state => ({
  // fields: getFields(state),
  // searchResults: searchResults(state),
});

const mapDispatchToProps = {
  // searchMembers,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppLoginForm);
