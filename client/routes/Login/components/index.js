import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesPaper from 'client/components/common/BridgesPaper';
import palette from 'client/styles/base/palette';
import BridgesTextField from 'client/components/common/BridgesTextField';
import logoImage from 'client/assets/logo.png';

// THEME STYLES

import pageStyles from 'client/styles/common/pages';
import BridgesButton from 'client/components/common/BridgesButton';
import { borderRadius, boxShadow } from 'client/styles/base/custom';
import { family, lineH, size, weight } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

// REDUX DEPENDENCIES
import { Field, reduxForm } from 'redux-form';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,

  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  loginWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  textFieldInputCover: {
    border: `1px solid ${palette.grey.darkest}`,
    marginBottom: 20,
    width: '400px',
    borderRadius: '5px',
  },
  forgotInfo: {
    color: palette.primary.lightest,
    fontSize: size.display3,
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    marginLeft: 5,
    textDecoration: 'none',
    paddingLeft: 171,
    lineHeight: lineH.lh_15,
  },
  RegisterInfo: {
    color: palette.primary.lightest,
    fontSize: size.display3,
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    marginLeft: 5,
    textDecoration: 'none',
    paddingLeft: 20,
    lineHeight: lineH.lh_15,
  },
  regMtop: {
    top: 25,
    position: 'relative',
  },
  btnWidth: {
    marginTop: 15,
  },
  userNameWrapper: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '25px',
    '& >div': {
      height: '70px',
    },
  },
  passwordWrapper: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '20px',
    '& >div': {
      height: '70px',
    },
  },
  header: {
    flex: '0 0 auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    padding: '40px 0 20px',
  },
});

class Login extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      username: '',
      password: '',
    };
  }

  handleSubmit = () => {
    this.props.history.push('/');
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <div className={classes.header}>
              <img alt="logo" className={classes.logo} src={logoImage} />
              <Typography variant="title" className={classes.title} gutterBottom={false}>
                Application Login
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12}>
            <form
              className={classes.container}
              noValidate
              autoComplete="off"
              onSubmit={this.handleSubmit}
            >
              <div className={classes.loginWrapper}>
                <div className={classes.userNameWrapper}>
                  <span>
                    <label htmlFor="default" className={classes.customLabel}>
                      UserName
                    </label>
                  </span>

                  <Field
                    className={classes.textFieldInputCover}
                    name={'username'}
                    component={BridgesTextField}
                  />
                </div>
                <div className={classes.passwordWrapper}>
                  <span>
                    <label htmlFor="default" className={classes.customLabel}>
                      Password
                    </label>
                    <label htmlFor="default">
                      <a href="javascript:void(0)" className={classes.forgotInfo}>
                        Forgot Password?
                      </a>
                    </label>
                  </span>
                  <Field
                    className={classes.textFieldInputCover}
                    name={'password'}
                    type="password"
                    component={BridgesTextField}
                  />
                </div>
                <div className={classes.btnWidth}>
                  <BridgesButton
                    title="Login"
                    iconAlign="left"
                    buttonType="squarebtn"
                    type="submit"
                  />
                </div>

                <div>
                  <span className={classes.regMtop}>
                    <label htmlFor="default" className={classes.customLabel}>
                      {"Don't have an account?"}
                    </label>
                    <label htmlFor="default">
                      <a href="javascript:void(0)" className={classes.RegisterInfo}>
                        Register Now
                      </a>
                    </label>
                  </span>
                </div>
              </div>
            </form>
          </Grid>
        </Grid>
      </div>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.any,
};

const LoginForm = reduxForm({
  form: 'login-form',
})(Login);

export default withStyles(styles)(LoginForm);
