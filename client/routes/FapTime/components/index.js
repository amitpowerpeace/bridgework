/**
 * Fap Time Component
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import palette from 'client/styles/base/palette';
import paths from 'client/routes/urls';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

class FapTime extends React.Component {
  // Set page title
  componentDidMount() {
    document.title = paths.fapTime.title;
  }

  render() {
    return (
      <div>
        <Typography variant="headline" style={{ color: palette.primary.brand }}>
          Fap Time
        </Typography>
      </div>
    );
  }
}

export default withStyles(styles)(FapTime);
