/**
 * Scheduling Component
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import palette from 'client/styles/base/palette';
import paths from 'client/routes/urls';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

class Scheduling extends React.Component {
  // Set page title
  componentDidMount() {
    document.title = paths.scheduling.title;
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Typography variant="headline" style={{ color: palette.primary.brand }}>
          Scheduling
        </Typography>
      </div>
    );
  }
}

export default withStyles(styles)(Scheduling);
