/**
 * Eligibility Component
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import palette from 'client/styles/base/palette';
import paths from 'client/routes/urls';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

class Eligibility extends React.Component {
  // Set page title
  componentDidMount() {
    document.title = paths.eligibility.title;
  }

  render() {
    return (
      <div>
        <Typography variant="headline" style={{ color: palette.primary.brand }}>
          Eligibility
        </Typography>
      </div>
    );
  }
}

export default withStyles(styles)(Eligibility);
