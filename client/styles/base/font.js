/**
 * Holds variables to customize typography
 *
 * Variables have been named as per material-ui default theme object
 *
 * Available font variants in typography
 * display4, display3, display2, display1
 * headline, title
 * body2, body1
 *
 */

export const size = {
  headline: '32px',
  title: '24px',
  display4: '18px',
  display3: '16px',
  display2: '15px',
  display1: '14px',
  body2: '14px',
  body1: '12px',
};

export const weight = {
  bold: 700,
  medium: 600,
  semi: 500,
  regular: 400,
  light: 200,
};

export const family = {
  primary: 'Montserrat',
  secondary: 'Lato',
  snippet: 'monospace',
  fallback: 'sans-serif',
};

export const lineH = {
  formLabel: '19px',
  textfield: '18px',
  lh_48: '48px',
  lh_42: '42px',
  lh_36: '36px',
  lh_23: '23px',
  lh_18: '18px',
  lh_17: '17px',
  lh_16: '16px',
  lh_15: '15px',
  code: '24px',
};
