const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export default (async function asyncValidate(values /* , dispatch */) {
  await sleep(1000); // simulate server latency
  if (['source', 'source1'].includes(values.Source)) {
    throw { email: 'Invalid source.' };
  }
});
