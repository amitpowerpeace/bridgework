/**
 * Returns Overridden form components based on fieldType
 */
import BridgesTextField from 'client/components/common/BridgesTextField';
import BridgesCheckBoxGroup from 'client/components/common/BridgesCheckBoxGroup';
import BridgesRadioGroup from 'client/components/common/BridgesRadioGroup';
import BridgesRadioBoxGroup from 'client/components/common/BridgesRadioBoxGroup';
import BridgesCheckBox from 'client/components/common/BridgesCheckbox';
import BridgesDatePicker from 'client/components/common/BridgesDatePicker';
import BridgesTimePicker from 'client/components/common/BridgesTimePicker';
import BridgesSelect from 'client/components/common/BridgesSelect';
import BridgesToggle from 'client/components/common/BridgesToggle';
import BridgesButton from 'client/components/common/BridgesButton';
import BridgesCheckButtonGroup from 'client/components/common/BridgesCheckButtonGroup';

const Components = {
  text: BridgesTextField,
  singlecheckbox: BridgesCheckBox,
  checkboxgroup: BridgesCheckBoxGroup,
  checkbuttongroup: BridgesCheckButtonGroup,
  radio: BridgesRadioGroup,
  radiogroup: BridgesRadioBoxGroup,
  selectbox: BridgesSelect,
  datepicker: BridgesDatePicker,
  timepicker: BridgesTimePicker,
  togglebox: BridgesToggle,
  bridgesbutton: BridgesButton,
};

export const getFieldComponent = type => {
  return Components[type];
};

export default getFieldComponent;
