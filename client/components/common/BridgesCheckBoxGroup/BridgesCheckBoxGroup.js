/**
 * Renders group checkbox component
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
// ICON DEPENDENCIES
import CheckCircle from '@material-ui/icons/CheckCircle';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
// EXTERNAL DEPENDENCIES
import renderHTML from 'react-render-html';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  ...formStyles,
  checkboxButtonStyle: {
    background: palette.white.base,
    borderRadius: borderRadius.checkboxButtonNormal,
    border: `1px solid ${palette.grey.darkest}`,
    color: palette.primary.brand,
    height: 40,
    float: 'left',
    marginLeft: 0,
    marginBottom: 12,
    marginRight: 12,
  },
  checkboxButtonCompactStyle: {
    borderRadius: borderRadius.checkboxButtonCompact,
    height: 'auto!important',
    border: `1px solid ${palette.grey.semidark}`,
    '& > span:nth-of-type(1)': {
      width: 40,
      marginLeft: 3,
    },
  },
  checkboxButtonStyleChecked: {
    borderColor: palette.primary.lightest,
  },
  checkboxLabelStyle: {
    fontSize: size.display2,
    paddingRight: 15,
  },
  checkboxLabelSelected: {
    color: `${palette.primary.lightest}!important`,
  },
  checkboxLabelUnSelected: {
    color: palette.grey.darkest,
  },
  checkboxLabelCompact: {
    fontSize: `${size.body2}!important`,
    color: palette.grey.semidark,
  },
  iconColorChecked: {
    color: palette.notification.success,
  },
  iconColorUnChecked: {
    color: palette.grey.darkest,
  },
  error: {
    fontFamily: family.secondary,
    color: palette.notification.danger,
    fontSize: size.body1,
    fontWeight: weight.semi,
    lineHeight: lineH.lh_15,
    width: '100%',
  },
  divShortWidth: {
    float: 'left',
    width: 'auto',
    marginBottom: 12,
  },
});
const checkd = [];
class BridgesCheckboxesGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: props.field.options,
      inputValue: props.input.value,
    };
  }

  componentWillMount(props) {
    this.state.fields.map((o, index) => {
      this.setState({ [o.value]: this.state.inputValue.indexOf(o.value) !== -1 });
    });
  }

  render() {
    const {
      classes,
      field,
      input,
      meta: { touched, error, warning },
    } = this.props;

    return (
      <FormControl fullWidth>
        <FormGroup row>
          {field.options.map((o, index) => (
            <div
              key={index}
              className={classNames({
                [classes.divFullWidth]: field.spreadType === 'vertical',
                [classes.divShortWidth]: field.spreadType === '',
              })}
            >
              <FormControlLabel
                className={classNames(
                  classes.checkboxButtonStyle,
                  this.state[`${o.value}`] ? classes.checkboxButtonStyleChecked : null,
                  field.compact === true ? classes.checkboxButtonCompactStyle : null,
                )}
                classes={{
                  label: classNames(
                    classes.checkboxLabelStyle,
                    this.state[`${o.value}`]
                      ? classes.checkboxLabelSelected
                      : classes.checkboxLabelUnSelected,
                    field.compact === true ? classes.checkboxLabelCompact : null,
                  ),
                }}
                control={
                  <Checkbox
                    onChange={(event, value) => {
                      if (event.target.checked) {
                        checkd.push(o.value);
                      } else {
                        checkd.splice(checkd.indexOf(o.value), 1);
                      }
                      this.setState({ [o.value]: event.target.checked });
                      return input.onChange(checkd.join(','));
                    }}
                    checked={this.state[`${o.value}`]}
                    value={o.value}
                    icon={<AddCircleOutline className={classes.iconColorUnChecked} />}
                    checkedIcon={<CheckCircle className={classes.iconColorChecked} />}
                  />
                }
                label={renderHTML(o.label)}
              />
            </div>
          ))}
          {touched &&
            ((error && <span className={classes.error}>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </FormGroup>
      </FormControl>
    );
  }
}

BridgesCheckboxesGroup.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.any,
};

BridgesCheckboxesGroup.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesCheckboxesGroup);
