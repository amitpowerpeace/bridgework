/**
 * Renders timepicker component
 *
 */
// REACT DEPENDENCIES
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import { TimePicker } from 'material-ui-pickers';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import { borderRadius, boxShadow } from 'client/styles/base/custom';

const styles = theme => ({
  customTextBox: {
    width: '100%',
    border: `1px solid ${palette.grey.darkest}`,
    borderRadius: borderRadius.timepickerField,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 5,
    paddingBottom: 5,
    position: 'relative',
    overflow: 'hidden',
    '& > div > input': {
      paddingLeft: 10,
      paddingRight: 10,
    },
    '& > div > input + div': {
      width: 35,
      height: 35,
    },
    '& > div button': {
      position: 'absolute',
    },
    '&:hover': {
      boxShadow: boxShadow.timepickerHover,
    },
  },
  timeInputStyle: {
    fontFamily: family.secondary,
    padding: '0 10px',
    fontSize: size.display2,
    color: palette.grey.darker,
  },
  hightlightTextBox: {
    border: `1px solid ${palette.primary.brand}`,
    '& > div button': {
      color: palette.primary.brand,
    },
  },
  error: {
    fontFamily: family.secondary,
    color: palette.notification.danger,
    fontSize: size.body1,
    fontWeight: weight.semi,
    lineHeight: lineH.lh_15,
    width: '100%',
  },
});

class BridgesTimePicker extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedDate: props.input.value !== '' ? props.input.value : new Date() };
  }

  handleDateChange = date => {
    this.setState({ selectedDate: date });
  };

  render() {
    const {
      classes,
      input,
      field,
      meta: { touched, error, warning },
    } = this.props;
    const { selectedDate } = this.state;

    return (
      <div>
        <TimePicker
          autoOk
          InputProps={{
            disableUnderline: true,
            className: classes.timeInputStyle,
          }}
          className={classNames(classes.customTextBox)}
          label=""
          mask={[/\d/, /\d/, ':', /\d/, /\d/, ' ', /a|p/i, 'M']}
          placeholder="08:00 AM"
          value={selectedDate}
          onChange={this.handleDateChange}
        />

        {touched &&
          ((error && <span className={classes.error}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  }
}

BridgesTimePicker.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.any,
};

BridgesTimePicker.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesTimePicker);
