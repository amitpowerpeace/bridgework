// REACT DEPENDENCIES
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
// MATERIAL DEPENDENCIES
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import { grey } from '@material-ui/core/colors';
// ICON DEPENDENCIES
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size } from 'client/styles/base/font';

const styles = {
  root: {
    height: 45,
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 10px 0 0',
    width: 'auto',
    backgroundColor: palette.background.lightest,
    boxShadow:
      'inset 0 0px 0 0 #E6E6E6, inset 2px -1px 2px 0px #E6E6E6, 0 0px 0px 0 rgba(0,0,0,0.1)',
    'border-bottom-left-radius': '0px',
    'border-top-left-radius': '0px',
    border: 0,
  },
  iconButton: {
    transform: 'scale(1, 1)',
    transition: 'transform 200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
  },
  iconButtonHidden: {
    transform: 'scale(0, 0)',
    '& > $icon': {
      opacity: 0,
    },
  },
  iconButtonDisabled: {
    opacity: 0.38,
  },
  searchIconButton: {
    marginRight: -44,
    marginLeft: 20,
    '& span svg': {
      color: `${palette.grey.dark}!important`,
    },
  },
  icon: {
    opacity: 0.54,
    transition: 'opacity 200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
  },
  input: {
    width: '100%',
    fontSize: 14,
    backgroundColor: `${palette.background.lightest}!important`,
    '& > input': {
      fontFamily: family.secondary,
      fontSize: size.display3,
      backgroundColor: `${palette.background.lightest}!important`,
      boxShadow: 'none!important',
    },
  },
  searchContainer: {
    margin: 'auto 16px',
    width: '100%',
    marginLeft: 46,
  },
};

/**
 * Material design search bar
 * @see [Search patterns](https://material.io/guidelines/patterns/search.html)
 */
class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
      value: this.props.value,
      active: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ ...this.state, value: nextProps.value });
    }
  }

  handleFocus = e => {
    this.setState({ focus: true });
    if (this.props.onFocus) {
      this.props.onFocus(e);
    }
  };

  handleBlur = e => {
    this.setState({ focus: false });
    if (this.state.value.trim().length === 0) {
      this.setState({ value: '' });
    }
    if (this.props.onBlur) {
      this.props.onBlur(e);
    }
  };

  handleInput = e => {
    this.setState({ value: e.target.value });
    if (this.props.onChange) {
      this.props.onChange(e.target.value);
    }
  };

  handleCancel = () => {
    this.setState({ active: false, value: '' });
    this.props.onChange && this.props.onChange('');
  };

  handleKeyUp = e => {
    if (e.charCode === 13 || e.key === 'Enter') {
      this.props.onRequestSearch(this.state.value);
    }
    if (this.props.onKeyUp) {
      this.props.onKeyUp(e);
    }
  };

  render() {
    const { value } = this.state;
    const {
      classes,
      closeIcon,
      disabled,
      onRequestSearch, // eslint-disable-line
      searchIcon,
      style,
      ...inputProps
    } = this.props;

    return (
      <Paper className={classes.root} style={style}>
        <IconButton
          classes={{
            root: classNames(classes.iconButton, classes.searchIconButton, {
              [classes.iconButtonHidden]: value !== '',
            }),
            disabled: classes.iconButtonDisabled,
          }}
          disabled={disabled}
        >
          {React.cloneElement(searchIcon, {
            classes: { root: classes.icon },
          })}
        </IconButton>

        <div className={classes.searchContainer}>
          <Input
            {...inputProps}
            onBlur={this.handleBlur}
            value={value}
            onChange={this.handleInput}
            onKeyUp={this.handleKeyUp}
            onFocus={this.handleFocus}
            fullWidth
            className={classes.input}
            disableUnderline
            disabled={disabled}
          />
        </div>

        <IconButton
          onClick={this.handleCancel}
          classes={{
            root: classNames(classes.iconButton, {
              [classes.iconButtonHidden]: value === '',
            }),
            disabled: classes.iconButtonDisabled,
          }}
          disabled={disabled}
        >
          {React.cloneElement(closeIcon, {
            classes: { root: classes.icon },
          })}
        </IconButton>
      </Paper>
    );
  }
}

SearchBar.defaultProps = {
  closeIcon: <ClearIcon style={{ color: grey[500] }} />,
  disabled: false,
  placeholder: 'Search..',
  searchIcon: <SearchIcon style={{ color: grey[500] }} />,
  style: null,
  value: '',
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired,

  closeIcon: PropTypes.node,

  disabled: PropTypes.bool,

  onBlur: PropTypes.func,

  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onKeyUp: PropTypes.func,
  onRequestSearch: PropTypes.func,

  placeholder: PropTypes.string,

  searchIcon: PropTypes.node,

  style: PropTypes.object,

  value: PropTypes.string,
};

export default withStyles(styles)(SearchBar);
