/**
 * BridgesTypography for demo display
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

class BridgesTypography extends React.Component {
  render() {
    return (
      <div>
        <Typography variant="headline">Headline</Typography>
        <Typography variant="subheading">Subheading</Typography>
        <Typography variant="display4">Display 4</Typography>
        <Typography variant="display3">Display 3</Typography>
        <Typography variant="display2">Display 2</Typography>
        <Typography variant="display1">Display 1</Typography>
        <Typography variant="body2">Body 2</Typography>
        <Typography variant="body1">Body 1</Typography>
      </div>
    );
  }
}

export default BridgesTypography;
