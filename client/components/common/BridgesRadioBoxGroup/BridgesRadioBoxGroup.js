/**
 * Renders group / single radiobutton component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes, { func } from 'prop-types';
import renderHTML from 'react-render-html';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
// ICON DEPENDENCIES
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonChecked from '@material-ui/icons/RadioButtonChecked';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  formControl: {
    margin: theme.spacing.unit,
  },
  withoutLabel: {
    marginTop: theme.spacing.unit * 3,
  },
  checkboxButtonStyle: {
    background: palette.white.base,
    borderRadius: 20,
    border: `1px solid ${palette.grey.darkest}`,
    height: 40,
    float: 'left',
    marginLeft: 0,
    marginBottom: 12,
    marginRight: 12,
  },
  checkboxButtonCompactStyle: {
    maxWidth: 165,
    borderRadius: 26,
    height: 'auto!important',
    border: `1px solid ${palette.grey.semidark}`,
  },
  checkboxButtonStyleChecked: {
    borderColor: palette.primary.lightest,
  },
  checkboxLabelStyle: {
    fontSize: size.display2,
    paddingRight: 15,
  },
  checkboxLabelSelected: {
    color: palette.primary.lightest,
  },
  checkboxLabelUnSelected: {
    color: palette.grey.darkest,
  },
  checkboxLabelCompact: {
    fontSize: `${size.body2}!important`,
    color: palette.grey.semidark,
  },
  iconColorChecked: {
    color: palette.notification.success,
  },
  iconColorUnChecked: {
    color: palette.grey.darkest,
  },
  error: {
    fontFamily: family.secondary,
    color: palette.notification.danger,
    fontSize: size.body1,
    fontWeight: weight.semi,
    lineHeight: lineH.lh_15,
    width: '100%',
  },
});

class BridgesRadioBoxGroup extends React.Component {
  render() {
    const {
      classes,
      input,
      field,
      meta: { touched, error, warning },
    } = this.props;
    const dispStructure = field.display || 'horizontal';

    const radioGroupWrapper = classNames({
      [classes.divFullWidth]: field.display === 'vertical',
    });

    return (
      <FormControl fullWidth>
        <RadioGroup row>
          {field.options.map((o, index) => (
            <div key={index} className={radioGroupWrapper}>
              <FormControlLabel
                className={classNames(classes.checkboxButtonStyle, {
                  [classes.checkboxButtonStyleChecked]: o.value === input.value,
                  [classes.checkboxButtonCompactStyle]: field.compact === true,
                })}
                classes={{
                  label: classNames(classes.checkboxLabelStyle, {
                    [classes.checkboxLabelSelected]: o.value === input.value,
                    [classes.checkboxLabelUnSelected]: o.value !== input.value,
                    [classes.checkboxLabelCompact]: field.compact === true,
                  }),
                }}
                value={o.value}
                control={
                  <Radio
                    onChange={(event, value) => {
                      return input.onChange(o.value);
                    }}
                    checked={o.value === input.value}
                    icon={<RadioButtonUnchecked className={classes.iconColorUnChecked} />}
                    checkedIcon={<RadioButtonChecked className={classes.iconColorChecked} />}
                  />
                }
                label={renderHTML(o.label)}
              />
            </div>
          ))}
          {touched &&
            ((error && <span className={classes.error}>{error}</span>) ||
              (warning && <span>{warning}</span>))}
        </RadioGroup>
      </FormControl>
    );
  }
}

BridgesRadioBoxGroup.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.any,
};

BridgesRadioBoxGroup.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesRadioBoxGroup);
