/**
 * Renders bridges group / single checkbox component
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';

const styles = theme => ({
  root: {},
  customCheckbox: {
    padding: 0,
    textAlign: 'left',
    display: 'inherit',
    width: 28,
    height: 28,
    '& > span': {
      width: 28,
      height: 28,
    },
    '& input': {
      width: 28,
      height: 28,
    },
  },
  iconStyle: {
    width: 28,
    height: 28,
  },
});
class BridgesCheckbox extends React.Component {
  render() {
    const {
      classes,
      input,
      field,
      meta: { touched, error, warning },
    } = this.props;

    return (
      <div className={classes.root}>
        <Checkbox
          checked={!!input.value}
          className={classes.customCheckbox}
          onChange={input.onChange}
          icon={<CheckBoxOutlineBlankIcon color="action" className={classes.iconStyle} />}
          checkedIcon={<CheckBoxIcon nativeColor={field.iconColor} className={classes.iconStyle} />}
        />
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    );
  }
}

BridgesCheckbox.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.any,
};

BridgesCheckbox.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesCheckbox);
