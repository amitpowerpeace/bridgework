/**
 * Renders collapse component
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// ICON DEPENDENCIES
import CheckIcon from '@material-ui/icons/Check';
import ErrorOutline from '@material-ui/icons/ErrorOutline';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Delete from '@material-ui/icons/Delete';
// COMPONENT DEPENDENCIES
import BridgesButton from 'client/components/common/BridgesButton';
// EXTERNAL DEPENDENCIES
import renderHTML from 'react-render-html';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, weight, size } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  actions: {
    display: 'flex',
    height: 'auto',
    padding: '20px 24px',
    position: 'relative',
  },
  expand: {
    width: 32,
    height: 31,
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    position: 'absolute',
    top: 28,
    right: 15,
    '& svg': {
      fontSize: 30,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
    color: palette.primary.lightest,
  },
  bottomPadding20: {
    paddingBottom: 20,
  },
  collapseHeading: {
    fontFamily: family.primary,
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    position: 'relative',
    letterSpacing: 0,
  },
  collapseHeadingLeftGap: {
    marginLeft: 0,
  },
  programSelectOpt: {
    color: palette.primary.brand,
    fontWeight: weight.semi,
    marginLeft: 'auto',
    position: 'absolute',
    top: 23,
    right: 100,
  },
  memberStatusIcon: {
    position: 'absolute',
    top: 25,
    left: 24,
    '& + h1': {
      paddingLeft: 36,
    },
  },
  /*  deleteHouseHoldMember: {
    color: palette.notification.danger,
    marginLeft: 'auto',
    marginRight: 39,
    marginTop: 0,
    width: 32,
    height: 31,
  }, */
  headingTeaser: {
    fontSize: size.display3,
    margin: 0,
  },
});

class BridgesCollapse extends React.Component {
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    const { classes, cData, children } = this.props;
    // let programSelectOpt = ['Healthcare', 'Food', 'Childcare'];
    const programSelectOpt = [];
    return (
      <div className={classes.bottomPadding20}>
        <Card className={classes.card}>
          <CardActions className={classes.actions} disableActionSpacing>
            {cData.fStatus !== '' &&
              !this.state.expanded && (
                <Icon aria-label="Add to favorites" className={classes.memberStatusIcon}>
                  {cData.fStatus === 'success' && <CheckIcon className={classes.successIcon} />}
                  {cData.fStatus === 'error' && <ErrorOutline className={classes.errorIcon} />}
                </Icon>
              )}
            <Typography
              variant="display4"
              className={classNames(classes.collapseHeading, {
                [classes.collapseHeadingLeftGap]: cData.fStatus === '',
              })}
            >
              {cData.header}
              {this.state.expanded &&
                cData.gender !== undefined &&
                cData.age !== undefined &&
                `, ${cData.gender} - ${cData.age}`}

              {!this.state.expanded &&
                cData.headerTagShort !== '' && (
                  <p className={classes.headingTag}> {cData.headerTagShort} </p>
                )}
              {!this.state.expanded &&
                cData.headingTeaser !== '' && (
                  <p className={classes.headingTeaser}> {cData.headingTeaser} </p>
                )}
            </Typography>
            {!this.state.expanded &&
              programSelectOpt.length > 0 && (
                <Typography variant="display2" className={classes.programSelectOpt}>
                  {renderHTML(programSelectOpt.join('&nbsp;&nbsp;&nbsp;'))}
                </Typography>
              )}

            {/*  {!this.state.expanded && (
              <IconButton
                className={classes.deleteHouseHoldMember}
                aria-expanded={this.state.expanded}
                aria-label="Delete"
              >
                <Delete />
              </IconButton>
            )} */}

            {this.state.expanded && (
              <Grid
                container
                spacing={16}
                alignItems={'flex-start'}
                direction={'row'}
                justify={'flex-end'}
                style={{
                  paddingRight: 54,
                  width: '50%',
                  marginLeft: 'auto',
                }}
              >
                {/* <Grid item>
                  <BridgesButton
                    title="Delete member"
                    iconAlign="left"
                    buttonType="squarebtn"
                    buttonPos="right"
                    buttonIcon="delete"
                    buttonWidth="min"
                    buttonColor="red"

                  />
                </Grid> */}

                <Grid item>
                  <BridgesButton
                    title="Edit"
                    iconAlign="left"
                    buttonType="squarebtn"
                    buttonPos="right"
                    buttonIcon="edit"
                    buttonWidth="min"
                  />
                </Grid>
              </Grid>
            )}
            <IconButton
              className={classNames(classes.expand, {
                [classes.expandOpen]: this.state.expanded,
              })}
              onClick={this.handleExpandClick}
              aria-expanded={this.state.expanded}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
            <CardContent>{children}</CardContent>
          </Collapse>
        </Card>
      </div>
    );
  }
}

BridgesCollapse.propTypes = {
  cData: PropTypes.object.isRequired,
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
};

BridgesCollapse.defaultProps = {
  cData: {},
};

export default withStyles(styles)(BridgesCollapse);
