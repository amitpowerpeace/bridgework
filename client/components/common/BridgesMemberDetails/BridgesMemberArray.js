import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// REDUX DEPENDENCIES
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm, formValueSelector } from 'redux-form';
import validate from './validate';
// MATERIAL DEPENDENCIES
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Collapse from '@material-ui/core/Collapse';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesPaper from 'client/components/common/BridgesPaper';
import BridgesFormEleCollapse from 'client/components/common/BridgesFormEleCollapse';
import BridgesButton from 'client/components/common/BridgesButton';
// MATERIAL ICONS
import CheckCircle from '@material-ui/icons/CheckCircle';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
// JSON DATA
import { memberData as members, people } from './default'
// STYLES
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import palette from 'client/styles/base/palette';
import { family, lineH, size, weight } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,
  checkboxButtonStyle: {
    background: palette.white.base,
    borderRadius: borderRadius.checkboxButtonNormal,
    border: `1px solid ${palette.grey.darkest}`,
    height: 40,
    float: 'left',
    marginLeft: 0,
    marginBottom: 12,
  },
  memberCollapseRoot: {
    paddingLeft: 3,
    paddingRight: 3,
  },
  checkboxButtonStyleChecked: {
    borderColor: palette.primary.lightest,
  },
  checkboxLabelStyle: {
    fontFamily: family.secondary,
    fontSize: size.display2,
    paddingRight: 15,
    color: palette.grey.darkest,
  },
  checkboxLabelStyleSel: {
    fontFamily: family.secondary,
    fontSize: size.display2,
    paddingRight: 15,
    color: palette.primary.lightest,
  },
  iconColorChecked: {
    color: palette.notification.success,
  },
  iconColorUnChecked: {
    color: palette.grey.darkest,
  },
  customLabel: {
    fontSize: size.display2,
    fontFamily: family.primary,
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    lineHeight: lineH.textfield,
    textAlign: 'left',
    marginBottom: 6,
  },
  dividerStyle: {
    margin: '20px 0px 23px!important',
    borderBottom: `1px solid ${palette.grey.light}`,
  },
  radioLabelUnChecked: {
    color: palette.grey.darkest,
  },
  radioLabelChecked: {
    color: palette.primary.lightest,
  },
  membersWrapperPaddingBottom: {
    paddingBottom: 3,
  },
  collapseWithoutDivider: {
    marginBottom: 10,
  },
  checkboxButtonGrouped: {
    position: 'relative',
    marginLeft: 29,
    '&:before': {
      content: '""',
      width: 3,
      left: '-28px',
      top: 0,
      position: 'absolute',
      height: '68px',
      backgroundColor: palette.primary.light,
    },
  },
  levelOneDrawer: {
    paddingTop: 12,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0
  },
  levelOneDrawerLabel: {
    paddingTop: 8,
    paddingRight: 0,
    paddingBottom: 8,
    paddingLeft: 0,
    display: 'block'
  },
  levelTwoDrawer: {
    paddingTop: 8,
    paddingRight: 0,
    paddingBottom: 8,
    paddingLeft: 0,
  },
  dynamicData: {
    padding: "10px 0px 20px"
  },
  dynamicDataWrapper: {
    padding: 0,
    width: '100%',
    paddingBottom: 10
  }
});


/* Parent Form */
class FieldArrays extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedMember: null,
      checkedValue: [],
      proplist: props
    };
  }

  expensesFields = (fieldNames) => {
    return [
      {
        name: `${fieldNames}.type`,
        type: 'selectbox',
        label: 'Expense Type',
        options: [
          {
            label: 'Student Loan In',
            value: 'Student Loan In'
          },
          {
            label: 'Tax Dedutible Expenses',
            value: 'Tax Dedutible Expenses'
          },
        ],
        validate: [],
        grid: {
          row: true,
          col: 4,
        },
        grouped: true
      },
      {
        name: `${fieldNames}.amount`,
        type: 'text',
        startIcon: '',
        label: 'Amount',
        value: '',
        validate: [],
        grid: {
          row: true,
          col: 4,
        },
        grouped: true
      },
      {
        name: `${fieldNames}.period`,
        type: 'selectbox',
        label: 'How Often Paid',
        options: [
          {
            label: 'Day',
            value: '1',
          },
          {
            label: 'Week',
            value: '2',
          },
          {
            label: 'Monthly',
            value: '3',
          },
        ],
        validate: [],
        grid: {
          row: false,
          col: 4
        },
        grouped: false
      }]
  };

  // Handles click on member
  handleMemberClick = (event, index) => {
    this.setState({ selectedMember: event.target.value });
    if (this.state.checkedValue.indexOf(event.target.value) === -1) {
      this.state.checkedValue.push(event.target.value)
    } else {
      this.state.checkedValue.splice(this.state.checkedValue.indexOf(event.target.value), 1)
    }
  };

  /**
 * Renders field
 */
  renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} type={type} placeholder={label} />
        {touched && error && <span>{error}</span>}
      </div>
    </div>
  );

  /**
   * Create fields
   */
  getExpenseFields = expense => {
    return (
      <BridgesDynamicFields fields={this.expensesFields(expense)} />
    );
  };

  /**
   * Renders level 1 fields of members
   */
  renderExpense = ({ fields, meta: { error } }) => {
    fields.length === 0 && fields.push()
    return (
      <div className={this.props.classes.dynamicDataWrapper}>
        <div className={this.props.classes.divFullWidth}>
          {fields.map((field, i) => (
            [<div key={i} className={
                  classNames(
                    this.props.classes.divFullWidth,
                    this.props.classes.dynamicData
                  )
                }>
              {this.getExpenseFields(field)}
            </div>,
            <div className={this.props.classes.divFullWidth}>
              {fields.length - 1 === i &&
                <BridgesButton
                  title="Add Expense"
                  iconAlign="left"
                  buttonPos="right"
                  buttonType="squarebtn"
                  buttonColor="hollowBlue"
                  buttonIcon="add"
                  onClick={() => {
                    fields.push();
                  }}
                />
              }
              <BridgesButton
                title="Remove Expense"
                iconAlign="left"
                buttonPos="right"
                buttonType="squarebtn"
                buttonIcon="delete"
                buttonColor="hollowRed"
                onClick={() => {
                  fields.remove(i)
                }}
              />
            </div>]
          ))
          }
          {error && <li className="error">{error}</li>}
        </div>
      </div>
    )
  };

  render() {
    const { classes, members } = this.props;
    const { selectedMember, checkedValue } = this.state;

    return (
      <FieldArray name="expenses" component={({ fields, meta: { error, submitFailed } }) => (
        <div className={classes.levelOneDrawer}>
          <label htmlFor="default" className={
            classNames(
              classes.customLabel, classes.levelOneDrawerLabel
            )
          }>
            {people.label}
          </label>
          {people.options.map((ppl, i) => (
            <div key={i}>
              <Field
                label=""
                name={`member_${[i + 1]}.name`}
                component={({ input }) => (
                  <div className={classNames({ [this.props.classes.divFullWidth]: people.spreadType === 'vertical' })}>
                    <FormControlLabel
                      className={classNames(this.props.classes.checkboxButtonStyle, {
                        [this.props.classes.checkboxButtonStyleChecked]:
                        checkedValue.indexOf(ppl.value) !== -1,
                        [this.props.classes.checkboxButtonGrouped]:
                          people.grouped && ppl.value === selectedMember,
                      })}
                      classes={{
                        label:
                        checkedValue.indexOf(ppl.value) !== -1
                            ? this.props.classes.checkboxLabelStyleSel
                            : this.props.classes.checkboxLabelStyle,
                      }}
                      control={
                        <Checkbox
                          onChange={event => (this.handleMemberClick(event, i), input.onChange(event.target.value))}
                          checked={
                            checkedValue.indexOf(ppl.value) !== -1
                          }
                          value={ppl.value}
                          icon={<AddCircleOutline className={this.props.classes.iconColorUnChecked} />}
                          checkedIcon={<CheckCircle className={this.props.classes.iconColorChecked} />}
                        />
                      }
                      label={ppl.label}
                    />
                  </div>
                )}
              />
              <Collapse
                in={
                  checkedValue.indexOf(ppl.value) !== -1
                }
                className={classNames(classes.memberCollapseRoot, classes.divFullWidth)}
                timeout={400}>

                <FieldArray name={`member_${[i + 1]}.expense`} component={this.renderExpense} />

              </Collapse>
            </div>
          ))}
        </div>
      )} />
    )
  }
};

FieldArrays.propTypes = {
  classes: PropTypes.object,
  members: PropTypes.object.isRequired,
  details: PropTypes.array.isRequired
};

FieldArrays.defaultProps = {
  label: "",
  members: {},
  details: []
}

export default withStyles(styles)(FieldArrays);
