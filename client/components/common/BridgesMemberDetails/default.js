export const memberData = [];

export const people = {
  name: 'peopleInd',
  type: 'checkboxgroup',
  label: 'Who ?',
  spreadType: 'vertical',
  compact: false,
  options: [
    {
      label: 'Leone Ramsey 40M',
      value: 'Leone Ramsey 40M',
    },
    {
      label: 'Owen Ramsey 36F',
      value: 'Owen Ramsey 36F',
    },
    {
      label: 'Nathen Ramsey 10M',
      value: 'Nathen Ramsey 10M',
    },
    {
      label: 'Molley Ramsey 13F',
      value: 'Molley Ramsey 13F',
    },
  ],
  validate: [],
  grid: {
    row: true,
    col: 12,
  },
  grouped: false,
};
