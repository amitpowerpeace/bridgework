/**
 * Renders member details component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Checkbox from '@material-ui/core/Checkbox';
import Collapse from '@material-ui/core/Collapse';
// ICON DEPENDENCIES
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonChecked from '@material-ui/icons/RadioButtonChecked';
import CheckCircle from '@material-ui/icons/CheckCircle';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import palette from 'client/styles/base/palette';
import { family, lineH, size, weight } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  ...formStyles,
  checkboxButtonStyle: {
    background: palette.white.base,
    borderRadius: borderRadius.checkboxButtonNormal,
    border: `1px solid ${palette.grey.darkest}`,
    height: 40,
    float: 'left',
    marginLeft: 0,
    marginBottom: 12,
  },
  memberCollapseRoot: {
    paddingLeft: 3,
    paddingRight: 3,
  },
  checkboxButtonStyleChecked: {
    borderColor: palette.primary.lightest,
  },
  checkboxLabelStyle: {
    fontFamily: family.secondary,
    fontSize: size.display2,
    paddingRight: 15,
    color: palette.grey.darkest,
  },
  checkboxLabelStyleSel: {
    fontFamily: family.secondary,
    fontSize: size.display2,
    paddingRight: 15,
    color: palette.primary.lightest,
  },
  iconColorChecked: {
    color: palette.notification.success,
  },
  iconColorUnChecked: {
    color: palette.grey.darkest,
  },
  customLabel: {
    fontSize: size.display2,
    fontFamily: family.primary,
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    lineHeight: lineH.textfield,
    textAlign: 'left',
    marginBottom: 6,
  },
  dividerStyle: {
    margin: '20px 0px 23px!important',
    borderBottom: `1px solid ${palette.grey.light}`,
  },
  radioLabelUnChecked: {
    color: palette.grey.darkest,
  },
  radioLabelChecked: {
    color: palette.primary.lightest,
  },
  membersWrapperPaddingBottom: {
    paddingBottom: 3,
  },
  collapseWithoutDivider: {
    marginBottom: 10,
  },
  checkboxButtonGrouped: {
    position: 'relative',
    marginLeft: 29,
    '&:before': {
      content: '""',
      width: 3,
      left: '-28px',
      top: 0,
      position: 'absolute',
      height: '68px',
      backgroundColor: palette.primary.light,
    },
  },
});
const checkd = [];
class BridgesMemberDetails extends React.Component {
  state = {
    selectedMember: null,
  };

  // Handles click on member
  handleMemberClick = event => {
    this.setState({ selectedMember: event.target.value });
  };

  handleMembersClick = data => {
    this.setState({ selectedMember: data });
  };

  render() {
    const { classes, members, details } = this.props;

    return (
      <FormControl fullWidth>
        <FormGroup row className={classes.membersWrapperPaddingBottom}>
          {members &&
            members.label !== '' && (
              <label
                htmlFor="name"
                className={classNames(
                  classes.customLabel,
                  members.display === '' ? classes.divFullWidth : null,
                )}
              >
                {members.label}
                <input type="hidden" id="name" />
                {members.showMoreOption && (
                  <a href="javascript:void(0)" className={classes.showAllFont}>
                    {' '}
                    Show All{' '}
                  </a>
                )}
              </label>
            )}

          {members &&
            members.options.map((member, index) => (
              <div
                key={index}
                className={classNames({ [classes.divFullWidth]: members.display === 'vertical' })}
              >
                {members.type === 'radiogroup' && (
                  <FormControlLabel
                    className={classNames(classes.checkboxButtonStyle, {
                      [classes.checkboxButtonStyleChecked]:
                        member.value === this.state.selectedMember,
                      [classes.checkboxButtonGrouped]:
                        members.grouped && member.value === this.state.selectedMember,
                    })}
                    classes={{
                      label:
                        member.value === this.state.selectedMember
                          ? classes.checkboxLabelStyleSel
                          : classes.checkboxLabelStyle,
                    }}
                    control={
                      <Checkbox
                        name={`${member.label}[${index}]`}
                        onChange={event => this.handleMemberClick(event)}
                        checked={member.value === this.state.selectedMember}
                        value={member.value}
                        icon={<RadioButtonUnchecked className={classes.iconColorUnChecked} />}
                        checkedIcon={<RadioButtonChecked className={classes.iconColorChecked} />}
                      />
                    }
                    label={member.label}
                  />
                )}

                {members.type === 'checkboxgroup' && (
                  <FormControlLabel
                    className={classNames(
                      classes.checkboxButtonStyle,
                      this.state[`${member.value}`] ? classes.checkboxButtonStyleChecked : null,
                      members.compact === true ? classes.checkboxButtonCompactStyle : null,
                    )}
                    classes={{
                      label: classNames(
                        classes.checkboxLabelStyle,
                        this.state[`${member.value}`]
                          ? classes.checkboxLabelSelected
                          : classes.checkboxLabelUnSelected,
                        members.compact === true ? classes.checkboxLabelCompact : null,
                      ),
                    }}
                    control={
                      <Checkbox
                        onChange={(event, value) => {
                          if (event.target.checked) {
                            checkd.push(member.value);
                          } else {
                            checkd.splice(checkd.indexOf(member.value), 1);
                          }
                          this.setState({ [member.value]: event.target.checked });
                          this.state.selectedMember = checkd.join(',');
                        }}
                        checked={this.state[`${member.value}`]}
                        value={member.value}
                        icon={<AddCircleOutline className={classes.iconColorUnChecked} />}
                        checkedIcon={<CheckCircle className={classes.iconColorChecked} />}
                      />
                    }
                    label={member.label}
                  />
                )}

                {console.log(`${this.state.selectedMember} ${member.value}`)}

                {details && (
                  <Collapse
                    in={
                      // this.state.selectedMember &&
                      // this.state.selectedMember.includes(member.value) ||
                      this.state.selectedMember === member.value
                    }
                    timeout={400}
                    className={classNames(classes.memberCollapseRoot, classes.divFullWidth, {
                      [classes.collapseWithoutDivider]: index === members.options.length - 1,
                    })}
                  >
                    <BridgesDynamicFields fields={details} />

                    {index < members.options.length - 1 && (
                      <Divider className={classNames(classes.divFullWidth, classes.dividerStyle)} />
                    )}
                  </Collapse>
                )}
              </div>
            ))}
        </FormGroup>
      </FormControl>
    );
  }
}

BridgesMemberDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  members: PropTypes.any,
};

BridgesMemberDetails.defaultProps = {
  members: {
    label: '',
    display: '',
    options: [],
  },
  details: [],
};

export default withStyles(styles)(BridgesMemberDetails);
