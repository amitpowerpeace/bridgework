/**
 * Renders textfield component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
// EXTERNAL DEPENDENCIES
import renderHTML from 'react-render-html';
// COMPONENT DEPENDENCIES
import TextMaskField from '../BridgesTextMask';
import BridgesNumberFormate from '../BridgesNumberFormate';
// ICON DEPENDENCIES
import ErrorOutline from '@material-ui/icons/ErrorOutline';
import Add from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { borderRadius, boxShadow } from 'client/styles/base/custom';
import { family, size } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  textFieldInputCover: {
    border: `1px solid ${palette.grey.darkest}`,
    borderRadius: borderRadius.textField,
    height: 42,
    position: 'relative',
    overflow: 'hidden',
    '&:hover': {
      boxShadow: boxShadow.textfieldCover,
    },
  },
  textFieldInput: {
    borderRadius: borderRadius.textField,
    fontFamily: family.secondary,
    fontSize: size.display3,
    color: palette.grey.darkest,
    padding: '14px 12px',
    boxShadow: boxShadow.textfieldInput,
  },
  textFieldRoot: {
    padding: 0,
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  hightlightTextBox: {
    border: `1px solid ${palette.primary.brand}`,
    '& > div button': {
      color: palette.primary.brand,
    },
  },
  startIconCss: {
    height: '42px',
    marginTop: '5px',
    marginLeft: '8px',
    '& p': {
      color: palette.grey.dark,
      fontSize: size.display3,
    },
  },
  iconButton: {
    width: 25,
    height: 25,
  },
  textboxFormControl: {
    height: 42,
  },
});

class BridgesTextField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      index: this.props.componentindex,
    };
  }

  clrIndex = data => {
    this.props.getClearIndex(data);
  };

  handleChange = event => {
    if (event.target.value.length > 0 && event.target.value.length !== '') {
      this.setState({ value: event.target.value });
      this.props.onChange(event.target.value);
    }
  };

  render() {
    const {
      classes,
      input,
      field,
      isDisabled,
      meta: { touched, error, warning },
      ...custom
    } = this.props;

    const { index, value } = this.state;

    return (
      <FormControl fullWidth className={classes.textboxFormControl}>
        <TextField
          disabled={isDisabled}
          onChange={this.handleChange}
          className={classes.textFieldInputCover}
          placeholder={field.placeholder}
          InputProps={{
            disableUnderline: true,
            inputProps: {
              masktype: field.maskType && field.maskType.length > 0 && field.maskType,
            },
            inputComponent:
              (field.maskType && field.maskType.length > 0 && TextMaskField) ||
              (field.numberOnly && BridgesNumberFormate) ||
              '',
            classes: {
              root: classes.textFieldRoot,
              input: classes.textFieldInput,
            },
            startAdornment: field &&
              field.startIcon &&
              field.startIcon !== '' && (
                <InputAdornment position="start" className={classNames(classes.startIconCss)}>
                  {renderHTML(field.startIcon)}
                </InputAdornment>
              ),
            endAdornment:
              (field.incrementor && (
                <InputAdornment position="end" className={classes.endAdornment}>
                  <IconButton aria-label="Add" className={classes.iconButton}>
                    <Add className={classes.incrementIcon} />
                  </IconButton>
                </InputAdornment>
              )) ||
              (field.reset && (
                <InputAdornment position="end" className={classes.endAdornment}>
                  <IconButton
                    aria-label="Reset"
                    className={classes.iconButton}
                    onClick={() => {
                      this.clrIndex(index);
                    }}
                  >
                    <Close className={classes.resetIcon} />
                  </IconButton>
                </InputAdornment>
              )) ||
              (!field.incrementor &&
                !field.reset &&
                touched &&
                error && (
                  <InputAdornment position="end" className={classes.endAdornment}>
                    <ErrorOutline className={touched && error ? classes.errorIcon : classes.hide} />
                  </InputAdornment>
                )),
          }}
          {...input}
          {...custom}
        />

        {touched &&
          ((error && <span className={classes.error}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </FormControl>
    );
  }
}

BridgesTextField.propTypes = {
  // OBJECT TO HOLD STYLE PROPERTIES
  classes: PropTypes.object.isRequired,
  // OBJECT FIELD TO POPULATE TEXTBOX AS PER USER REQUIREMENT
  componentindex: PropTypes.number,
  // OBJECT FIELD TO MONITOR INTERACTIONS WITH TEXTBOX AS PER REDUX FIELD PROPERTIES
  field: PropTypes.object.isRequired,
  // OBJECT FIELD TO MONITOR INTERACTIONS WITH TEXTBOX AS PER REDUX FIELD VALIDATION RULES
  getClearIndex: PropTypes.func,
  // BOOLEAN FIELD TO SHOW / HIDE PLUS-ICON
  incrementor: PropTypes.bool,
  // BOOLEAN FIELD TO SHOW / HIDE CLOSE-ICON
  input: PropTypes.object.isRequired,
  // FUNCTION FIELD TO READ TEXTBOX INTERACTIONS
  isDisabled: PropTypes.bool,
  // FUNCTION FIELD TO GET THE DESIRED INDEX TO BE CLEARED ( USED FOR DECREMENT OPERATION Eg: Add Amount )
  meta: PropTypes.any,
  // BOOLEAN FIELD TO ENABLE / DISABLE TEXTBOX
  onChange: PropTypes.func,
  // RECORD INDEX FOR INCREMENTED TEXTBOX TO BE USED BY PARENT COMPONENT
  reset: PropTypes.bool,
};

BridgesTextField.defaultProps = {
  input: {},
  field: {
    startIcon: '',
    options: [],
    incrementor: '',
    reset: '',
    // MASKTYPE USED TO FORMATE SPECIFIC INPUTS FROM USERS Eg: SSN Code / Phone Number / Zip-Code
    maskType: '',
    // BOOLEAN FIELD TO RESTRICT THE USER INPUT TO NUMBER ONLY
    numberOnly: false,
  },
  meta: {},
  isDisabled: false,
  componentindex: 0,
};

export default withStyles(styles)(BridgesTextField);
