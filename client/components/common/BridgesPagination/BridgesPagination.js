// REACT DEPENDENCIES
import PropTypes from 'prop-types';
import React from 'react';
import { computePagination } from './core';
// MATERIAL DEPENDENCIES
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
// ICON DEPENDENCIES
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import MoreHorizontal from '@material-ui/icons/MoreHoriz';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';

const styles = theme => ({
  root: {
    textAlign: 'right',
    marginTop: 26,
    '& button': {
      borderTop: `1px solid ${palette.grey.darkest}`,
      borderBottom: `1px solid ${palette.grey.darkest}`,
      borderRadius: 0,
      lineHeight: `${lineH.lh_24}!important`,
      width: 32,
      height: 32,
      padding: 0,
      minWidth: 0,
      color: palette.grey.darkest,
      fontFamily: family.primary,
      fontSize: size.display3,
      fontWeight: weight.semi,
      verticalAlign: 'text-top',
    },
    '& button:not(:last-child)': {
      borderRight: `1px solid ${palette.grey.darkest}`,
    },
    '& button:first-child': {
      borderLeft: `1px solid ${palette.grey.darkest}`,
      color: palette.primary.lightest,
    },
    '& button:last-child': {
      borderRight: `1px solid ${palette.grey.darkest}`,
      color: palette.grey.darkest,
    },
    '& svg': {
      color: palette.primary.lightest,
    },
  },
  paginationArrow: {
    '& svg': {
      fontSize: '18px!important',
    },
  },
  paginationBtn: {
    backgroundColor: palette.primary.lightest,
    color: `${palette.white.base}!important`,
    '&:hover': {
      backgroundColor: palette.primary.lightest,
      color: `${palette.white.base}!important`,
    },
  },
});

class BridgesPagination extends React.Component {
  reduced = true;

  handleClick = targetPage => event => {
    this.props.onChangePage(event, targetPage - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  renderCurrentButton(targetPage, classes, mapIndex) {
    return (
      <Button
        color="secondary"
        className={this.props.page + 1 === targetPage ? classes.paginationBtn : ''}
        disabled={this.props.disabled || this.props.total <= 0}
        key={mapIndex}
      >
        {targetPage}
      </Button>
    );
  }

  renderOtherButton(targetPage, mapIndex) {
    return (
      <Button
        disabled={this.props.disabled || this.props.total <= 0}
        onClick={this.handleClick(targetPage)}
        key={mapIndex}
      >
        {`${targetPage}`}
      </Button>
    );
  }

  renderEllipsisButton(mapIndex) {
    return (
      <Button key={mapIndex} disabled>
        <MoreHorizontal />
      </Button>
    );
  }

  renderFirstPageButton = (page, classes, mapIndex) => (
    <Button
      className={classes.paginationArrow}
      disabled={page <= 0}
      onClick={this.handleClick(page)}
      key={mapIndex}
    >
      <KeyboardArrowLeft />
    </Button>
  );

  renderLastButton = (page, rowsPerPage, count, classes, mapIndex) => (
    <Button
      className={classes.paginationArrow}
      key={mapIndex}
      disabled={page >= Math.ceil(count / rowsPerPage) - 1}
      onClick={this.handleNextButtonClick}
    >
      <KeyboardArrowRight />
    </Button>
  );

  render() {
    const { classes, page, rowsPerPage, count } = this.props;

    return (
      <div className={classes.root}>
        {computePagination(page, rowsPerPage, count, this.reduced ? 1 : 2).map((o, i) => {
          if (o.isCurrent) {
            return this.renderCurrentButton(o.page, classes, i);
          }
          if (o.isEnd) {
            if (o.isLowSide) {
              return this.renderFirstPageButton(page, classes, i);
            }
            return this.renderLastButton(page, rowsPerPage, count, classes, i);
          }
          if (o.isEllipsis) {
            return this.renderEllipsisButton(o.isLowSide ? 'le' : 'he', i);
          }
          return this.renderOtherButton(o.page);
        })}
      </div>
    );
  }
}

BridgesPagination.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  disabled: PropTypes.any,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  total: PropTypes.any,
};

export default withStyles(styles)(BridgesPagination);
