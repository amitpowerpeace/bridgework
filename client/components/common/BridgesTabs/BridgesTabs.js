/**
 * Renders tab component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
import BridgesCheckButtonGroup from 'client/components/common/BridgesCheckButtonGroup';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, weight, size } from 'client/styles/base/font';
import forms from 'client/styles/common/forms';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: '10px 0 0 0' }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node,
};

const styles = theme => ({
  ...forms,
  root: {
    backgroundColor: theme.palette.background.paper,
    marginTop: 20,
  },
  header: {
    backgroundColor: '#ffffff',
    border: `1px solid ${palette.primary.lightest}`,
    boxShadow: 'none',
  },
  tabBtn: {
    fontFamily: family.primary,
    fontWeight: weight.semi,
    textTransform: 'capitalize',
    color: palette.grey.dark,
    height: 42,
    minHeight: 42,
    minWidth: 149,
    float: 'left',
    "&[aria-selected='true']": {
      backgroundColor: palette.primary.lightest,
      color: '#ffffff!important',
    },
    "&:nth-of-type(1)": {
      borderRight: `1px solid ${palette.primary.lightest}`,
    },
    "&:nth-of-type(4)": {
      borderLeft: `1px solid ${palette.primary.lightest}`,
    }
  },
  radioLabelStyle: {
    fontFamily: family.secondary,
    fontWeight: weight.medium,
    fontSize: size.display2,
    color: palette.grey.darkest,
  },
  tabHeaderStrip: {
    minHeight: '42px!important',
    '& > div button > svg': {
      color: palette.grey.darkest,
    },
  },
  tabBtnWrapper: {
    position: 'relative',
    margin: '0 auto',
    width: 'auto'
  },
  tabBtnLabelContainer: {
    paddingLeft: 22,
    paddingRight: 22,
  },
  tabBtnLabel: {
    fontFamily: family.primary,
    fontWeight: 500,
    fontSize: size.display2,
  },
  tabStatusIcon: {
    right: 10,
    position: 'absolute',
    fontSize: '8px!important'
  },
  tabBtnSelected: {
    "& $tabStatusIcon": {
      color: 'transparent'
    }
  }
});

class GeneralTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  initTabs = () => {
    this.setState({ value: 0 });
  };

  handleChangeForRadio = (event, value) => {
    this.setState({ formRadioValue: value });
  };

  groupCheckedValues = (data) => {
    // COLLECTION OF CHECKED FORM ITEMS
    console.log(data)
  };
  
  render() {
    const { classes, headerData } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.header}>
          <Tabs
            value={value}
            onChange={this.handleChange}
            className={classes.tabHeaderStrip}
            indicatorColor="primary"
            textColor="secondary"
          >
            {typeof this.props.headerData === 'object' &&
              this.props.headerData.map((response, i) => (
                <Tab key={Math.random()}
                  label={response.tabTitle}
                  className={classes.tabBtn}
                  classes={
                    {
                      wrapper: classes.tabBtnWrapper,
                      labelContainer: classes.tabBtnLabelContainer,
                      label: classes.tabBtnLabel,
                      selected: classes.tabBtnSelected
                    }
                  }
                  icon={<Icon className={classNames(classes.tabStatusIcon,classes.successIcon)}> lens </Icon>} />
              ))}

          </Tabs>
        </AppBar>

        <TabContainer className={classes.tabContainer}>
          {typeof headerData === 'object' && (
            <BridgesCheckButtonGroup getCheckedValues={this.groupCheckedValues} field={headerData[value].tabData[0]} />
          )}
        </TabContainer>
      </div>
    );
  }
}

GeneralTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  headerData: PropTypes.array,
};

export default withStyles(styles)(GeneralTabs);
