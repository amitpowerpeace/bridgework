import React, { Children } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import BridgesDynamicFields from 'client/components/DynamicFields';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Checkbox from '@material-ui/core/Checkbox';
// THEME STYLES
import palette from 'client/styles/base/palette';
import formStyles from 'client/styles/common/forms';
import { family, size, weight, lineH } from 'client/styles/base/font';

const styles = theme => ({
  table: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'stretch',
    height: 30,
    width: '100%',
    borderBottom: `1px solid${palette.grey.lightergrey}`,
  },
  shadow: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'stretch',
    height: 30,
    width: '100%',
    borderBottom: `1px solid${palette.grey.lightergrey}`,
  },
  hcol: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'stretch',
    height: 30,
    width: '100%',
    padding: '0px 40px 0px 12px',
    borderBottom: `1px solid${palette.grey.lightergrey}`,
  },
  tBody: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
    top: 32,
    width: '100%',
  },
  content: {
    width: 'initial',
    height: 0,
    opacity: 0,
    transition: 'height 0.2s',
    padding: '0 10px',
    overflow: 'hidden',
  },
  heading: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'stretch',
    width: '100%',
    height: 48,
    cursor: 'pointer',
    transition: 'background 0.5s',
    '&>div:nth-child(8)': {
      color: palette.primary.lightest,
    },
  },
  col: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    padding: '0 10px',
    height: 48,
    textAlign: 'center',
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    fontSize: size.display3,
    lineHeight: lineH.textfield,
    justifyContent: 'center',
    '&>span>svg': {
      color: palette.grey.darkest,
    },
  },
  tRow: {
    width: '100%',
    minHeight: 48,
    borderBottom: `1px solid${palette.grey.gainsboro}`,
  },
  contentOpen: {
    height: 260,
    opacity: 1,
    overflow: 'visible',
    paddingTop: '5px',
    paddingLeft: '120px',
    backgroundColor: palette.background.whitesmoke,
    '&>div>div:nth-child(2)': {
      marginTop: '-55px',
    },
    '&>div>div>div': {
      color: palette.primary.lightest,
      boxShadow: 'none',
      backgroundColor: palette.background.whitesmoke,
    },
    '&>div>div>div>div>div>input': {
      color: palette.primary.lightest,
      boxShadow: 'none!important',
      backgroundColor: palette.background.whitesmoke,
    },
    '&>div>div>div:nth-child(n)': {
      marginRight: '55px',
      '&>div>div': {
        marginBottom: '12px',
      },
    },
    '&>div>div>div>div:nth-of-type(1)': {
      width: 'auto',
    },
    '&>div': {
      paddingLeft: '0px',
    },
  },
  headingOpen: {
    backgroundColor: palette.primary.lightcyan,
    color: palette.white.base,
  },
  colOpen: {
    border: 'none',
    '&>span>svg': {
      color: palette.primary.lightest,
      paddingLeft: '50px',
    },
  },
  colOpencolor: {
    border: 'none',
    paddingLeft: '35px',
    color: palette.primary.lightest,
  },
  colOpenLeft: {
    // paddingLeft: '70px',
    border: 'none',
  },
  colOpenData: {
    border: 'none',
    paddingLeft: '90px',
  },
  iconStyle: {
    width: 22,
    height: 22,
  },
  expandIconcolor: {
    color: palette.primary.lightest,
  },
  anchor: {
    cursor: 'pointer',
    color: 'blue !important',
    textDecoration: 'underline',
    '&:hover': {
      textDecoration: 'none',
      textShadow: `1px 1px 1px${palette.grey.darkest}`,
    },
  },
});

class RowItem extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
      selected: [],
    };
  }

  toggleRow(e) {
    this.setState({
      open: !this.state.open,
    });
  }

  renderCheckbox = (classes, isSelected) => (
    <Checkbox
      checked={isSelected}
      icon={<CheckBoxOutlineBlankIcon color="action" className={classes.iconStyle} />}
      checkedIcon={<CheckBoxIcon nativeColor="#4AA564" className={classes.iconStyle} />}
    />
  );

  enableDisableBtn = value => {
    this.props.updateEnableDisableBtn(value);
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
    console.log(event.target.checked);
    const chkTicked = event.target.checked;
    const selectedItem = this.state.selected;
    if (selectedItem.length === 0) {
      this.enableDisableBtn(true);
    } else if (selectedItem.length === 1 && chkTicked === true) {
      this.enableDisableBtn(true);
    } else if (selectedItem.length === 1 && chkTicked === false) {
      this.enableDisableBtn(false);
    } else {
      this.enableDisableBtn(true);
    }
  };

  render() {
    const { classes, rowData } = this.props;
    const { open } = this.state;
    const isSelected = this.isSelected(rowData.id);

    const headOpen = classNames(classes.heading, {
      [classes.headingOpen]: open,
    });
    const colOpen = classNames(classes.col, {
      [classes.colOpen]: open,
    });
    const colOpenLeft = classNames(classes.col, {
      [classes.colOpenLeft]: open,
    });
    const colOpencolor = classNames(classes.col, {
      [classes.colOpencolor]: open,
    });

    const colOpenData = classNames(classes.col, {
      [classes.colOpenData]: open,
    });
    const contentOpen = classNames(classes.content, {
      [classes.contentOpen]: open,
    });

    return (
      <li key={Math.Random} className={classes.tRow}>
        <div
          className={headOpen}
          role="presentation"
          onClick={event => this.handleClick(event, rowData.id)}
        >
          <div className={colOpen}>{this.renderCheckbox(classes, isSelected)}</div>
          <div className={colOpen}>{rowData.CaseAppNo}</div>
          <div className={colOpen}>{rowData.CaseStatus}</div>
          <div className={colOpen}>{rowData.HouseHoldStatus}</div>
          <div className={colOpen}>{rowData.HOH}</div>
          <div className={colOpen}>{rowData.Programs}</div>
          <div className={colOpen}>{rowData.Office}</div>

          <div className={colOpen}>
            <span className={classNames.anchor}>
              {!this.state.open && (
                <ExpandMoreIcon
                  className={classes.expandIconcolor}
                  onClick={() => this.toggleRow()}
                />
              )}

              {this.state.open && (
                <ExpandLessIcon
                  className={classes.expandIconcolor}
                  onClick={() => this.toggleRow()}
                />
              )}
            </span>
          </div>
        </div>
        <div className={contentOpen}>
          <BridgesDynamicFields fields={rowData && rowData.inDrop} />
        </div>
      </li>
    );
  }
}

RowItem.propTypes = {
  classes: PropTypes.object.isRequired,
  rowData: PropTypes.any,
  updateEnableDisableBtn: PropTypes.any,
};

export default withStyles(styles)(RowItem);
