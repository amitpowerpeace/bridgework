// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// COMPONENT DEPENDENCIES
import BridgesDynamicFields from 'client/components/DynamicFields';
// EXTERNAL DEPENDENCIES
import ReactTable, { ReactTableDefaults } from 'react-table';
import checkboxHOC from 'react-table/lib/hoc/selectTable';
import renderHTML from 'react-render-html';
// ICONS
import Icon from '@material-ui/core/Icon';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { borderRadius, boxShadow } from 'client/styles/base/custom';
import { family, lineH, size, weight } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import 'react-table/react-table.css';

const CheckboxTable = checkboxHOC(ReactTable);
const styles = theme => ({
  ...pageStyles,
  ...formStyles,
  nestedTableData: {
    padding: 20,
    backgroundColor: palette.grey.lightest,
  },
  expandableTable: {
    border: '0!important',
    padding: '0px 8px',
    '& .rt-table': {
      '& .rt-thead': {
        backgroundColor: palette.primary.dark,
        color: palette.white.base,
        fontWeight: weight.semi,
        fontFamily: family.secondary,
        fontSize: size.display3,
        lineHeight: lineH.textfield,
        color: palette.white.base,
        textAlign: 'left',
        paddingTop: 12,
        paddingBottom: 12,
        '&:nth-of-type(1)': {
          display: 'none',
        },
        '& .-sort-asc': {
          boxShadow: 'inset 0 0 0 0 transparent!important',
          position: 'relative',
          '&:after': {
            content: '"↑"',
            fontSize: size.display3,
            color: palette.white.base,
            position: 'absolute',
            right: 0,
            top: 2,
          },
        },
        '& .-sort-desc': {
          boxShadow: 'inset 0 0 0 0 transparent!important',
          position: 'relative',
          '&:after': {
            content: '"↓"',
            fontSize: size.display3,
            color: palette.white.base,
            position: 'absolute',
            right: 0,
            top: 2,
          },
        },
        "& input[type='checkbox']": {
          display: 'none',
        },
      },
      '& .rt-tbody': {
        textAlign: 'center',
        color: palette.grey.darkest,
        fontWeight: weight.semi,
        fontFamily: family.secondary,
        fontSize: size.display3,
        lineHeight: lineH.textfield,
        borderBottom: `1px solid ${palette.grey.dark}`,
        '& .rt-tr-group': {
          borderBottom: `1px solid ${palette.grey.dark}`,
          '& .rt-tr': {
            padding: 10
          },
        },
        '& .rt-td': {
          borderRight: 'none',
          '&.rt-expandable': {
            paddingTop: '6px!important',
          },
        },
        "& input[type='checkbox']": {
          appearance: 'none',
          boxShadow: `0 0 0 2px ${palette.notification.success} inset`,
          width: 20,
          height: 20,
          borderRadius: '100%',
          margin: 0,
          position: 'relative',
          cursor: 'pointer',
        },
        "& input[type='checkbox']:checked": {
          boxShadow: `0 0 0 2px ${palette.notification.success} inset`,
          width: 20,
          height: 20,
          borderRadius: '100%',
          margin: 0,
          position: 'relative',
        },
        "& input[type='checkbox']:checked::after": {
          content: '"●"',
          position: 'absolute',
          top: 1,
          fontSize: 14,
          left: 4,
          color: palette.notification.success,
        },
      },
    },
    '& label': {
      color: palette.grey.darkest,
      fontWeight: weight.medium,
    },
    "& input[type='text']": {
      color: palette.grey.darkest,
    },
    '& .-pagination': {
      borderTop: 'none!important',
      width: 400,
      marginLeft: 'auto',
      boxShadow: 'none!important',
      padding: '24px 0 0!important',
      display: 'flex',
      flexDirection: 'row',
      '& select': {
        padding: '10px 12px!important',
      },
      '& .-previous': {
        '-webkit-box-flex': '0!important',
        '-ms-flex': '0!important',
        flex: '0!important',
        float: 'left',
        textAlign: 'left',
        order: 2,
        '& button': {
          width: 'auto',
          borderRadius: 0,
          backgroundColor: 'transparent',
          color: `${palette.primary.brand}!important`,
          display: 'table',
          '& span': {
            display: 'table-cell',
            verticalAlign: 'middle',
          },
          '&:hover': {
            backgroundColor: 'transparent!important',
          },
          '&:focus': {
            backgroundColor: 'transparent!important',
          },
        },
      },
      '& .-next': {
        '-webkit-box-flex': '0!important',
        '-ms-flex': '0!important',
        flex: '0!important',
        float: 'left',
        textAlign: 'left',
        order: 3,
        '& button': {
          width: 'auto',
          borderRadius: 0,
          backgroundColor: 'transparent',
          color: `${palette.primary.brand}!important`,
          display: 'table',
          '& span': {
            display: 'table-cell',
            verticalAlign: 'middle',
          },
          '&:hover': {
            backgroundColor: 'transparent!important',
          },
          '&:focus': {
            backgroundColor: 'transparent!important',
          },
        },
      },
      '& .-center': {
        width: 'auto',
        float: 'left',
        order: 1,
        display: 'flex',
        flexDirection: 'row',
        '& span': {
          '&.-pageInfo': {
            color: palette.black.base,
            fontWeight: weight.semi,
            fontFamily: family.secondary,
            fontSize: size.display3,
            marginLeft: 0,
          },
          '&.select-wrap': {
            margin: '0px 20px 0 auto',
            '& select': {
              color: palette.grey.dark,
              fontWeight: weight.semi,
              fontFamily: family.secondary,
              fontSize: size.display2,
              borderColor: palette.grey.darkest,
              borderRadius: borderRadius.toggleBtn,
            },
          },
          "& input[type='number']": {
            backgroundColor: palette.white.base,
            border: 0,
            '-webkit-user-select': 'none',
            '-moz-user-select': 'none',
            '-ms-user-select': 'none',
            'user-select': 'none',
            padding: 0,
            width: 60,
            textAlign: 'center',
          },
          '&:nth-of-type(1)': {
            order: 2,
          },
          '&:nth-of-type(2)': {
            order: 1,
          },
        },
      },
    },
  },
  nestedTableDataHeading: {
    fontWeight: weight.semi,
    textAlign: 'left',
    color: palette.grey.darkest,
    marginBottom: 2,
  },
});

class AccordionTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selection: [],
      selectAll: false,
    };
  }

  toggleSelection = (key, shift, row) => {
    /*
      Implementation of how to manage the selection state is up to the developer.
      This implementation uses an array stored in the component state.
      Other implementations could use object keys, a Javascript Set, or Redux... etc.
    */
    // start off with the existing state
    let selection = [...this.state.selection];
    const keyIndex = selection.indexOf(key);
    // check to see if the key exists
    if (keyIndex >= 0) {
      // it does exist so we will remove it using destructing
      selection = [...selection.slice(0, keyIndex), ...selection.slice(keyIndex + 1)];
    } else {
      // it does not exist so add it
      selection.push(key);
    }
    // update the state
    this.setState({ selection });
  };

  toggleAll = () => {
    /*
      'toggleAll' is a tricky concept with any filterable table
      do you just select ALL the records that are in your data?
      OR
      do you only select ALL the records that are in the current filtered data?

      The latter makes more sense because 'selection' is a visual thing for the user.
      This is especially true if you are going to implement a set of external functions
      that act on the selected information (you would not want to DELETE the wrong thing!).

      So, to that end, access to the internals of ReactTable are required to get what is
      currently visible in the table (either on the current page or any other page).

      The HOC provides a method call 'getWrappedInstance' to get a ref to the wrapped
      ReactTable and then get the internal state and the 'sortedData'.
      That can then be iterrated to get all the currently visible records and set
      the selection state.
    */
    const selectAll = !this.state.selectAll;
    const selection = [];
    if (selectAll) {
      // we need to get at the internals of ReactTable
      const wrappedInstance = this.checkboxTable.getWrappedInstance();
      // the 'sortedData' property contains the currently accessible records based on the filter and sort
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      // we just push all the IDs onto the selection array
      currentRecords.forEach(item => {
        selection.push(item._original._id);
      });
    }
    this.setState({ selectAll, selection });
  };

  isSelected = key => {
    /*
      Instead of passing our external selection state we provide an 'isSelected'
      callback and detect the selection state ourselves. This allows any implementation
      for selection (either an array, object keys, or even a Javascript Set object).
    */
    return this.state.selection.includes(key);
  };

  logSelection = () => {
    console.log('selection:', this.state.selection);
  };

  render() {
    const { toggleSelection, toggleAll, isSelected, logSelection } = this;
    const { selectAll } = this.state;
    const { classes, TableData } = this.props;

    const checkboxProps = {
      selectAll,
      isSelected,
      toggleSelection,
      toggleAll,
      selectType: 'checkbox',
      getTrProps: (s, r) => {
        const selected = this.isSelected(r.original._id);
        return {
          style: {
            backgroundColor: selected ? palette.primary.lightcyan : 'inherit',
          },
        };
      },
      previousText: <Icon>keyboard_arrow_left</Icon>,
      nextText: <Icon>keyboard_arrow_right</Icon>,
      showPageJump: false,
      pageText: '',
      ofText: 'of  ',
      rowsText: '',
      loadingText: 'Loading...',
      noDataText: 'No rows found',
      resizable: false,
      filterable: false,
    };

    return (
      <React.Fragment>
        <CheckboxTable
          ref={r => (this.checkboxTable = r)}
          data={TableData.data}
          columns={TableData.meta}
          defaultPageSize={10}
          className={classes.expandableTable}
          {...checkboxProps}
          SubComponent={row => {
            return (
              <Typography component="div" className={classes.nestedTableData}>
                <Typography variant="display2" className={classes.nestedTableDataHeading}>
                  {'All household members in this case'}
                </Typography>
                {row.original.subComponent.length > 0 && (
                  <BridgesDynamicFields fields={row.original.subComponent} />
                )}
              </Typography>
            );
          }}
        />
      </React.Fragment>
    );
  }
}

AccordionTable.propTypes = {
  classes: PropTypes.object.isRequired,
  TableData: PropTypes.object.isRequired,
};

export default withStyles(styles)(AccordionTable);
