import React from 'react';
import { assert } from 'chai';
import { createShallow, createRender, getClasses } from '@material-ui/core/test-utils';
import Button from './BridgesButton';

describe('<Button />', () => {
  let shallow;
  let render;
  let classes;

  beforeAll(() => {
    shallow = createShallow({ dive: true });
    render = createRender();
    classes = getClasses(<Button>Hello World</Button>);
  });

  it('should work', () => {
    const wrapper = shallow(<Button />);
  });

  // it('should render with the root, rounded classes but no others', () => {
  //   const wrapper = shallow(<Button>Hello World</Button>);
  //   assert.strictEqual(wrapper.hasClass(classes.root), true, 'should have the root class');
  //   assert.strictEqual(wrapper.hasClass(classes.rounded), true, 'should have the rounded class');
  // });
});
