/**
 * Renders button with either square / rounded corners alomg with icons on left / right side of button
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
// THEME STYLES
import spacing from 'client/styles/base/spacing';
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  root: {
    padding: 12,
    fontFamily: family.primary,
    fontSize: size.display3,
    fontWeight: weight.semi,
    lineHeight: lineH.textfield,
    textTransform: 'initial',
  },
  rootIcon: {
    width: 22,
    height: 22,
    marginRight: 8,
  },
  btnWidthMin: {
    width: 'initial',
  },
  btnWidthMax: {
    width: '100%',
  },
  rounded: {
    background: palette.white.base,
    borderRadius: borderRadius.btnCapsule,
    border: `1px solid ${palette.primary.lightest}`,
    marginBottom: spacing.space4,
    color: palette.primary.lightest,
    height: 42,
    lineHeight: lineH.lh_42,
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: family.primary,
    fontSize: size.body2,
    '& svg': {
      marginTop: 3,
    },
    '&:hover': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:active': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:focus': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
  },
  square: {
    background: palette.white.base,
    borderRadius: 0,
    border: `1px solid ${palette.primary.lightest}`,
    marginBottom: 0,
    color: palette.primary.lightest,
    height: 42,
    lineHeight: lineH.lh_42,
    paddingTop: 0,
    paddingBottom: 0,
    fontFamily: family.primary,
    fontSize: size.body2,
    '& svg': {
      marginTop: 3,
    },
    '&:hover': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:active': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:focus': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
  },
  red: {
    background: palette.white.base,
    borderRadius: 0,
    color: palette.notification.danger,
    border: `1px solid ${palette.notification.danger}`,
    '& svg': {
      marginTop: 3,
      color: palette.notification.danger,
    },
    '&:hover': {
      background: palette.white.base,
      color: palette.notification.danger,
    },
    '&:active': {
      background: palette.white.base,
      color: palette.notification.danger,
    },
    '&:focus': {
      background: palette.white.base,
      color: palette.notification.danger,
    },
  },
  hollowRed: {
    borderRadius: 5,
    background: palette.white.base,
    color: palette.notification.danger,
    border: `1px solid ${palette.notification.danger}`,
    '& svg': {
      marginTop: 3,
      color: palette.notification.danger,
    },
    '&:hover': {
      background: palette.notification.danger,
      color: palette.white.base,
    },
    '&:active': {
      background: palette.notification.danger,
      color: palette.white.base,
    },
    '&:focus': {
      background: palette.notification.danger,
      color: palette.white.base,
    },
  },
  blue: {
    borderRadius: 0,
    color: palette.white.base,
    background: palette.primary.lightest,
    border: `1px solid ${palette.primary.lightest}`,
    '& svg': {
      marginTop: 3,
      color: palette.white.base,
      background: palette.primary.lightest,
    },
    '&:hover': {
      color: palette.white.base,
      background: palette.primary.lightest,
    },
    '&:active': {
      color: palette.white.base,
      background: palette.primary.lightest,
    },
    '&:focus': {
      color: palette.white.base,
      background: palette.primary.lightest,
    },
  },
  hollowBlue: {
    borderRadius: 5,
    color: palette.primary.lightest,
    background: palette.white.base,
    border: `1px solid ${palette.primary.lightest}`,
    '& svg': {
      marginTop: 3,
      color: palette.white.base,
      background: palette.primary.lightest,
    },
    '&:hover': {
      color: palette.white.base,
      background: palette.primary.lightest,
    },
    '&:active': {
      color: palette.white.base,
      background: palette.primary.lightest,
    },
    '&:focus': {
      color: palette.white.base,
      background: palette.primary.lightest,
    },
  },
  floatRight: {
    float: 'right',
    marginLeft: 10,
  },
  floatLeft: {
    float: 'left',
  },
  leftIcon: {
    float: 'left',
  },
  rightIcon: {
    marginRight: 2,
    float: 'right',
  },
  buttonLabelCss: {
    width: 'initial',
  },
  dynamicButtonTopSpace: {
    marginTop: 37,
  },
});

function BridgesButton(props) {
  const {
    classes,
    title,
    buttonIcon,
    iconAlign,
    buttonType,
    buttonColor,
    buttonPos,
    buttonWidth,
    buttonDynamic,
    field,
    input,
    ...custom
  } = props;

  const buttonClassName = classNames(classes.root, {
    [classes.btnWidthMin]: (buttonWidth || (field && field.buttonWidth)) === 'min',
    [classes.btnWidthMax]: (buttonWidth || (field && field.buttonWidth)) === 'max',
    [classes.hollowRed]: (buttonColor || (field && field.buttonColor)) === 'hollowRed',
    [classes.hollowBlue]: (buttonColor || (field && field.buttonColor)) === 'hollowBlue',
    [classes.red]: (buttonColor || (field && field.buttonColor)) === 'red',
    [classes.blue]: (buttonColor || (field && field.buttonColor)) === 'blue',
    [classes.rounded]: (buttonType || (field && field.buttonType)) === 'roundbtn',
    [classes.square]: (buttonType || (field && field.buttonType)) === 'squarebtn',
    [classes.floatRight]: (buttonPos || (field && field.buttonPos)) === 'right',
    [classes.floatLeft]: (buttonPos || (field && field.buttonPos)) === 'left',
    [classes.dynamicButtonTopSpace]: buttonDynamic || (field && field.buttonDynamic),
  });

  const iconClass = classNames(classes.rootIcon, {
    [classes.leftIcon]: iconAlign || (field && field.iconAlign) === 'left',
    [classes.rightIcon]: iconAlign || (field && field.iconAlign) === 'right',
  });

  return (
    <Button
      classes={{
        label: classes.buttonLabelCss,
      }}
      className={buttonClassName}
      onClick={props.onClick}
      {...input}
      {...custom}
    >
      {(buttonIcon || (field && field.buttonIcon)) && (
        <Icon className={iconClass}>{buttonIcon || (field && field.buttonIcon)}</Icon>
      )}
      {title || (field && field.title) || 'Button'}
    </Button>
  );
}

BridgesButton.propTypes = {
  buttonColor: PropTypes.string,
  buttonDynamic: PropTypes.bool,
  buttonIcon: PropTypes.string,
  buttonPos: PropTypes.string,
  buttonType: PropTypes.string,
  buttonWidth: PropTypes.string,
  classes: PropTypes.object.isRequired,
  iconAlign: PropTypes.string,
  onClick: PropTypes.func,
  title: PropTypes.string,
};

BridgesButton.defaultProps = {
  title: '',
  buttonIcon: '',
  iconAlign: '',
  buttonType: 'squarebtn',
  buttonColor: '',
  buttonPos: '',
  buttonWidth: 'min',
  buttonDynamic: false,
};

export default withStyles(styles)(BridgesButton);
