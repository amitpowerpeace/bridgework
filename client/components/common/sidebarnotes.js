// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import LensIcon from '@material-ui/icons/Lens';
// THEME STYLES
import { family, size, weight } from 'client/styles/base/font';
import palette from 'client/styles/base/palette';
import formStyles from 'client/styles/common/forms';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  ...formStyles,
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    float: 'left',
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  notesHeader: {
    fontFamily: family.primary,
    fontSize: size.display2,
    fontWeight: weight.semi,
    color: palette.grey.darkest,
    textAlign: 'left',
    marginTop: 40,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0,
  },
  notesBox: {
    border: '1px solid #eaeaea',
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: borderRadius.notesBox,
    width: '91%',
    fontFamily: family.secondary,
    color: 'red',
    '& > div': {
      fontFamily: family.secondary,
      fontSize: size.body2,
      color: palette.grey.darkest,
    },
  },
  textFieldCover: {
    marginTop: 0,
    paddingLeft: 10,
    paddingRight: 10,
    width: '100%',
  },
  feedTimeStyle: {
    margin: 0,
    position: 'relative',
    paddingTop: 12,
    paddingBottom: 12,
  },
  timeStatus: {
    width: 8,
    float: 'right',
    marginTop: 4,
    marginRight: 4,
    height: 8,
  },
  currTime: {
    float: 'right',
    color: palette.grey.darkest,
    fontFamily: family.secondary,
    fontSize: size.body1,
    paddingRight: 0,
  },
});

class SidebarNotes extends React.Component {
  state = {
    multiline: '',
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <div className={classes.textFieldCover}>
          <p className={classNames(classes.divFullWidth, classes.feedTimeStyle)}>
            <span className={classes.currTime}>10:05AM</span>
            <LensIcon className={classNames(classes.timeStatus, classes.successStatusColor)} />
          </p>
          <p className={classes.notesHeader}>Notes</p>
          <TextField
            fullWidth
            id="multiline-flexible"
            label=""
            multiline
            rows="10"
            value={this.state.multiline}
            onChange={this.handleChange('multiline')}
            className={classes.notesBox}
            InputProps={{
              disableUnderline: true,
            }}
            margin="normal"
          />
        </div>
      </form>
    );
  }
}

SidebarNotes.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SidebarNotes);
