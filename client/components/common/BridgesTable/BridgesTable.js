/**
 * Renders table component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// ICON DEPENDENCIES
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonChecked from '@material-ui/icons/RadioButtonChecked';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
// COMPONENT DEPENDENCIES
import BridgesPagination from 'client/components/common/BridgesPagination';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
// EXTERNAL DEPENDENCIES
import keyBy from 'lodash/keyBy';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 3,
    height: '0 !important',
  },
  table: {
    width: '99%',
    minWidth: 500,
  },
  tableHeader: {
    backgroundColor: palette.primary.dark,
  },
  tableHeaderFont: {
    textAlign: 'center',
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    fontSize: size.display3,
    lineHeight: lineH.textfield,
    color: palette.white.base,
    textalign: 'center',
    paddingLeft: '45px',
  },
  tableHeaderColor: {
    color: `${palette.white.base}!important`,
  },
  tableBodyFont: {
    textAlign: 'center',
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    fontFamily: family.secondary,
    fontSize: size.display3,
    lineHeight: lineH.textfield,
    padding: '17px 8px',
    '& span': {
      height: '23px',
    },
  },
  iconColorChecked: {
    color: palette.primary.brand,
  },
  radioLabelUnChecked: {
    color: palette.grey.dark,
  },
  radioLabelChecked: {
    color: palette.primary.brand,
  },
  dataTableRow: {
    height: '0',
    borderRight: 'none',
    borderLeft: 'none',
    '&[aria-checked="true"]': {
      outline: 'none',
      borderStyle: 'hidden',
      borderRight: 'none',
      borderLeft: 'none',
      // borderRight: '1px solid ' + palette.primary.brand,
      // borderLeft: '1px solid ' + palette.primary.brand,
      // borderTop: '1px solid ' + palette.primary.brand,
      // borderBottom: '1px solid ' + palette.primary.brand,
      backgroundColor: palette.primary.lightcyan,
      color: palette.grey.darker,
      padding: 6,
    },
    '&[aria-checked="true"] > td': {
      color: palette.primary.brand,
    },
  },
  dataTableRowEmpty: {
    position: 'relative',
    '&:before': {
      content: 'attr(data-nocontentmessage)',
      fontWeight: weight.semi,
      fontFamily: family.secondary,
      fontSize: size.body2,
      color: palette.grey.dark,
      textAlign: 'center',
      position: 'absolute',
      left: '50%',
      margin: '12px 0',
    },
  },
  tablePaginateStyle: {
    float: 'right',
  },
  button: {
    margin: theme.spacing.unit,
    marginLeft: 400,
    border: `1px solid${palette.primary.brand}`,
  },
  showallTextColor: {
    fontSize: '14px',
    fontWeight: '400',
    color: palette.primary.lightest,
    textAlign: 'right',
    paddingTop: '35px',
    cursor: 'pointer',
    '&span>a': {
      textDecoration: 'none',
    },
  },
  customCheckbox: {
    padding: 0,
    textAlign: 'left',
    display: 'inherit',
    '& > span': {
      display: 'initial',
      height: 36,
    },
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
  showAll: {
    margin: '0 10px 0 auto',
  },
  pagination: {
    margin: '0 0 0 0',
  },
});

class BridgesTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selected1: 0,
      meta: props.tableData.meta,
      data: props.tableData.data,
      page: 0,
      rowsPerPage: 10,
      checkedOpt: false,
      order: 'asc',
      orderBy: 'Score',
      selected: [],
    };
  }

  /*   handleClick = (event, id) => {
    this.setState({ selected: id });

    console.log('this.state.selected' + this.state.selected);
    this.props.updateEnableDisableBtn(true);
  }; */

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
    const chkTicked = event.target.checked;
    if (this.state.selected.length === 0) {
      this.props.updateEnableDisableBtn(true);
    } else if (this.state.selected.length === 1 && chkTicked === true) {
      this.props.updateEnableDisableBtn(true);
    } else if (this.state.selected.length === 1 && chkTicked === false) {
      this.props.updateEnableDisableBtn(false);
    } else {
      this.props.updateEnableDisableBtn(true);
    }
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChange = name => event => {
    this.setState({ checkedOpt: !this.state.checkedOpt });
  };

  handleLoadMore = event => {
    event.preventDefault();
    // const newState = { address: { ...state.address, postalCode: 1000 }};
    this.setState({
      rowsPerPage: this.state.rowsPerPage + 5, // update the state with the new rows
    });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  /*  isSelected = id => {
    this.state.selected === id;
  } */

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  renderRadio = (classes, value) => (
    <Radio
      checked={this.state.selected === value}
      icon={<RadioButtonUnchecked className={classes.radioLabelUnChecked} />}
      checkedIcon={<RadioButtonChecked className={classes.radioLabelChecked} />}
      value={value.toString()}
      name="table-radio-button"
      aria-label={`user-${value}`}
    />
  );

  renderCheckbox = (classes, value, isSelected) => (
    <Checkbox
      checked={isSelected}
      icon={<CheckBoxOutlineBlankIcon color="action" className={classes.iconStyle} />}
      checkedIcon={<CheckBoxIcon nativeColor="#4AA564" className={classes.iconStyle} />}
    />
  );

  /**
   * Render table cell
   */
  renderTableCell = (n, item, i, classes, sortedMeta, value, isSelected) => {
    const meta = sortedMeta[item];
    if (meta && !meta.hidden) {
      if (meta.type === 'radio') {
        return (
          <TableCell key={i} className={classes.tableBodyFont}>
            {this.renderRadio(classes, value)}
          </TableCell>
        );
      }
      if (meta.type === 'checkbox') {
        return (
          <TableCell key={i} className={classes.tableBodyFont}>
            {this.renderCheckbox(classes, value, isSelected)}
          </TableCell>
        );
      }
      return (
        <TableCell key={i} className={classes.tableBodyFont}>
          {n[item]}
        </TableCell>
      );
    }
  };

  createSortHandler = (event, property) => {
    // const orderBy = property;
    // let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      // order = 'asc';
    }
  };

  render() {
    const { classes } = this.props;
    const { order } = this.state;

    const data = this.props.tableData.data;
    const meta = this.props.tableData.meta;
    const { rowsPerPage, page } = this.state;
    const sortedMeta = keyBy(meta, 'name');

    return (
      <div>
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <Table className={classes.table}>
              <TableHead className={classes.tableHeader}>
                <TableRow>
                  {meta.map((o, i) => {
                    if (!o.hidden) {
                      return (
                        <TableCell
                          key={i}
                          className={classes.tableHeaderFont}
                          active={!!o.header}
                          direction={order}
                          onClick={this.createSortHandler(o.id)}
                        >
                          {o.header}
                        </TableCell>
                      );
                    }
                  })}
                </TableRow>
              </TableHead>
              <TableBody
                data-nocontentmessage="No Results Found"
                className={{
                  [classes.dataTableRowEmpty]: data.length === 0,
                }}
              >
                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n, i) => {
                  const value = parseInt(i + rowsPerPage * page);
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      key={n.id}
                      onClick={event => this.handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      selected={isSelected}
                      className={classes.dataTableRow}
                    >
                      {Object.keys(n).map((item, i) =>
                        this.renderTableCell(n, item, i, classes, sortedMeta, value, isSelected),
                      )}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Grid>

          <Grid item className={classes.showAll}>
            <Typography className={classes.showallTextColor} component="p">
              <span>Show All </span>
            </Typography>
          </Grid>
          <Grid item className={classes.pagination}>
            <BridgesPagination
              page={page}
              rowsPerPage={rowsPerPage}
              count={data.length}
              onChangePage={this.handleChangePage}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

BridgesTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableData: PropTypes.object.isRequired,
  updateEnableDisableBtn: PropTypes.any,
};

BridgesTable.defaultProps = {
  tableData: {
    data: [],
    meta: [],
  },
};

export default withStyles(styles)(BridgesTable);
