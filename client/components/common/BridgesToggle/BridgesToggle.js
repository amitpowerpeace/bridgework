/**
 * Renders bridges toggle component
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import ToggleButton, { ToggleButtonGroup } from '@material-ui/lab/ToggleButton';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, lineH } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  toggleContainer: {
    height: 'auto',
    padding: 0,
    display: 'flex',
    alignItems: 'left',
    justifyContent: 'flex-start',
    margin: 0,
    backgroundColor: 'none',
  },
  toggleButtonGroup: {
    width: "fit-content",
    backgroundColor: 'none',
    border: `1px solid ${palette.grey.darkest}`,
    overflow: "hidden",
    borderRadius: 5,
    boxShadow: "none"
  },
  buttonGroupSelected: {
    '& $toggleButton':{
      "&:nth-of-type(1)":{
        borderRight: "1px solid transparent"
      }
    }
  },
  toggleButton: {
    width: "auto",
    minWidth: 40,
    height: 42,
    padding: "0px 10px",
    backgroundColor: palette.white.base,
    color: palette.grey.darkest,
    fontFamily: family.secondary,
    fontSize: size.display3,
    lineHeight: lineH.lh_18,
    textTransform: "capitalize",
    boxShadow: "none",
    "&:hover": {
      backgroundColor: palette.white.base,
      color: palette.grey.darkest,
    },
    "&:nth-of-type(1)":{
      borderRight: `1px solid ${palette.grey.darkest}`
    }
  },
  highlightBtnFirst: {
    backgroundColor: palette.notification.success,
    color: palette.white.base,
    '&:after': {
      content: '""',
      display: 'none',
    },
    '&:hover': {
      backgroundColor: palette.notification.success,
      color: palette.white.base,
    }
  },
  highlightBtnSecond: {
    backgroundColor: palette.formField.orange,
    color: palette.white.base,
    '&:after': {
      content: '""',
      display: 'none',
    },
    '&:hover': {
      backgroundColor: palette.formField.orange,
      color: palette.white.base,
    }
  }
});

// borderTop: `1px solid ${palette.grey.darkest}`,

class BridgesToggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleValue: props.input.value,
      toggleOptions: [],
    };
  }

  handleToggle = data => {
    this.setState({ toggleOptions: data });
    if (data === null) {
      data = this.props.field.toggleStack[1].value;
    }
    typeof this.props.input.onChange !== 'undefined' && this.props.input.onChange(data);
    typeof this.props.input.onChange === 'undefined' && this.props.onChangeTrack(data);
  };

  render() {
    const { classes, field } = this.props;
    const { toggleOptions } = this.state;

    return (
      <FormControl fullWidth>
        <div className={classes.toggleContainer}>
          <ToggleButtonGroup
            value={toggleOptions}
            exclusive
            onChange={this.handleToggle}
            classes={{
              root: classes.toggleButtonGroup,
              selected: classes.buttonGroupSelected,
            }}
          >
            <ToggleButton
              value={field.toggleStack[0].value}
              classes={{
                selected: classes.highlightBtnFirst,
                root: classes.toggleButton
              }}
            >
              {field.toggleStack[0].title}
            </ToggleButton>
            <ToggleButton
              value={field.toggleStack[1].value}
              classes={{
                selected: classes.highlightBtnSecond,
                root: classes.toggleButton
              }}
            >
              {field.toggleStack[1].title}
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
      </FormControl>
    );
  }
}

BridgesToggle.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  onChangeTrack: PropTypes.func,
};

BridgesToggle.defaultProps = {
  input: {},
  field: {
    toggleStack: [
      {
        title: '',
        value: '',
      },
      {
        title: '',
        value: '',
      },
    ],
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesToggle);
