import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import formStyles from 'client/styles/common/forms';
import pageStyles from 'client/styles/common/pages';
import palette from 'client/styles/base/palette';
import Grid from '@material-ui/core/Grid';
import { family, size, weight, lineH } from 'client/styles/base/font';
import ClearIcon from '@material-ui/icons/Clear';
import DoneIcon from '@material-ui/icons/Done';
import classNames from 'classnames';

const styles = theme => ({
  ...formStyles,
  ...pageStyles,

  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 4,
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    borderStyle: 'hidden',
    borderRadius: '5px',
    outline: 'None',
  },
  btnNo: {
    color: 'red',
    textTransform: 'none',
    border: `1px solid${palette.notification.danger}`,
    marginRight: 10,
  },
  btnYes: {
    color: palette.primary.lightest,
    textTransform: 'none',
    border: `1px solid${palette.primary.lightest}`,
  },
  checked: {},
  size: {
    width: 40,
    height: 40,
  },
  sizeIcon: {
    fontSize: 20,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
  btnAlignRgt: {
    justifyContent: 'flex-end',
    display: 'flex',
  },
  description: {
    fontFamily: family.primary,
    color: palette.grey.darkest,
    fontWeight: weight.medium,
    paddingBottom: '6px',
    marginTop: '-10px',
    fontSize: size.display2,
    lineHeight: lineH.lh_18,
  },
  descriptionText: {
    fontFamily: family.secondary,
    color: palette.grey.darkest,
    fontWeight: weight.semi,
    fontSize: size.display3,
    lineHeight: lineH.lh_18,
  },
});

class BridgesModalPopup extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      open: false,
      checkedOpt: false,
    };
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = () => {
    this.setState({ checkedOpt: !this.state.checkedOpt });
  };

  render() {
    const { classes, title, desc, desctext } = this.props;

    return (
      <div>
        <Button onClick={this.handleOpen}>Modal Window</Button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div className={classes.paper}>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Typography variant="display4" className={classes.subHeading}>
                  {title}
                  <span className={classes.headerUnderline} />
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="display2" className={classes.description}>
                  <div>{desc}</div>
                </Typography>
                <Typography variant="display1" className={classes.descriptionText}>
                  <div>{desctext}</div>
                </Typography>
              </Grid>
              <Grid item xs={12} className={classes.btnAlignRgt}>
                <Button className={classes.btnNo} onClick={this.handleClose}>
                  <ClearIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                  No
                </Button>
                <Button variant="outlined" className={classes.btnYes} onClick={this.handleClose}>
                  <DoneIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                  Yes
                </Button>
              </Grid>
            </Grid>
          </div>
        </Modal>
      </div>
    );
  }
}

BridgesModalPopup.propTypes = {
  classes: PropTypes.object.isRequired,
  desc: PropTypes.any,
  desctext: PropTypes.any,
  title: PropTypes.any,
};

const SimpleModalWrapped = withStyles(styles)(BridgesModalPopup);

export default SimpleModalWrapped;
