/**
 * Renders Selectbox
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
// ICON DEPENDENCIES
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, lineH } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  selectBoxStyle: {
    border: `1px solid ${palette.grey.darkest}`,
    height: 42,
    borderRadius: borderRadius.selectBoxStyle,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    fontFamily: family.secondary,
    fontSize: size.body2,
    color: palette.grey.darkest,
    '& > div > div': {
      paddingLeft: 10,
      paddingRight: 10,
      height: `${100}%`,
      padding: 0,
      lineHeight: lineH.lh_42,
      backgroundColor: 'transparent!important',
    },
    '& > div > svg': {
      marginRight: 10,
      fontSize: 25,
    },
  },
});

class BridgesSelect extends React.Component {
  state = {
    value: 'none',
  };

  render() {
    const { classes, field, input } = this.props;
    return (
      <FormControl className={classes.formControl} fullWidth>
        <Select
          value={this.state.value}
          placeholder={field.placeholder}
          onChange={event => {
            this.setState({ value: event.target.value });
            return input.onChange(event.target.value);
          }}
          className={classes.selectBoxStyle}
          disableUnderline
          IconComponent={props => <KeyboardArrowDown {...props} />}
        >
          <option value="none" disabled>
            {field && field.placeholder}
          </option>
          <MenuItem value="">
            <em>Select</em>
          </MenuItem>
          {field.options.map((item, i) => (
            <MenuItem key={i} value={item.value}>
              {item.label}
            </MenuItem>
          ))}
        </Select>
        {/* <FormHelperText></FormHelperText> */}
      </FormControl>
    );
  }
}

BridgesSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
};

BridgesSelect.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesSelect);
