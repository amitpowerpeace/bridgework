/**
 * THIS COMPONENT USED COMMONLY IN FORMS TO PROVIDE SLIDE DOWN EFFECT FOR DROPDOWN
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import palette from 'client/styles/base/palette';

const styles = theme => ({
  ...formStyles,
  dividerStyle: {
    margin: '10px 0px 5px!important',
    borderBottom: `1px solid ${palette.grey.light}`,
  },
  formEleCollapseRoot: {
    paddingLeft: 0,
    paddingRight: 3,
  },
});

const BridgesFormEleCollapse = props => {
  const { classes, children, stack, needle, dividerFlag } = props;
  // SWITCH TO HOLD TRUE OR FALSE TO OPERATE COLLAPSE
  let open = false;

  if (typeof stack === 'string' && stack.indexOf(needle) !== -1) {
    open = true;
  } else if (typeof stack === 'boolean') {
    open = stack;
  } else if (typeof stack === 'object') {
    open = stack.length > 0;
  }

  return (
    <Collapse
      className={classNames(classes.formEleCollapseRoot, classes.divFullWidth)}
      in={open}
      timeout={500}
    >
      {open && children}
      {open &&
        dividerFlag && (
          <Divider className={classNames(classes.divFullWidth, classes.dividerStyle)} />
        )}
    </Collapse>
  );
};

BridgesFormEleCollapse.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
  // COLLECTION OR INDIVIUAL VALUE
  dividerFlag: PropTypes.bool,
  // SPECIFIC VALUE FOR WHICH COLLAPSE HAS TO OPEN
  needle: PropTypes.any,
  //  FLAG FOR DIVIDER LINE EXISTANCE
  stack: PropTypes.any,
};

// DEFAULT VALUES
BridgesFormEleCollapse.defaultProps = {
  stack: '',
  needle: '',
};

export default withStyles(styles)(BridgesFormEleCollapse);
