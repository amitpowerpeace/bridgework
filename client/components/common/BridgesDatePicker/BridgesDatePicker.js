/**
 * Renders Datepicker component
 */
// REACT DEPENDENCIES
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import { DatePicker } from 'material-ui-pickers';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
// ICON DEPENDENCIES
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import DateRange from '@material-ui/icons/DateRange';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';
import { borderRadius, boxShadow } from 'client/styles/base/custom';

const styles = theme => ({
  ...formStyles,
  customTextBox: {
    width: '100%',
    border: `1px solid ${palette.grey.darkest}`,
    borderRadius: borderRadius.datepickerField,
    paddingTop: 5,
    paddingBottom: 5,
    position: 'relative',
    '& > div > input + div': {
      width: 35,
      height: 35,
    },
    '& > div button': {
      position: 'absolute',
      right: 0,
      color: palette.grey.dark,
    },
    '&:hover': {
      boxShadow: boxShadow.datePicker,
    },
  },
  dateInput: {
    fontFamily: family.secondary,
    padding: '0 10px',
    fontSize: size.display2,
    color: palette.grey.darker,
  },
  dateIcon: {
    color: palette.grey.dark,
  },
  hightlightTextBox: {
    border: `1px solid ${palette.primary.brand}`,
    '& > div button': {
      color: palette.primary.brand,
    },
  },
});

class BridgesDatePicker extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedDate: props.input.value !== '' ? props.input.value : new Date() };
  }

  handleDateChange = date => {
    this.setState({ selectedDate: date });
  };

  render() {
    const {
      classes,
      meta: { touched, error, warning },
    } = this.props;
    const { selectedDate } = this.state;

    return (
      <div>
        <DatePicker
          keyboard
          clearable
          disableFuture
          InputProps={{
            disableUnderline: true,
            className: classes.dateInput,
          }}
          className={classes.customTextBox}
          format="DD/MM/YYYY"
          placeholder="DD/MM/YYYY"
          mask={value => (value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : [])}
          value={selectedDate}
          invalidDateMessage={''}
          maxDateMessage={''}
          onChange={this.handleDateChange}
          animateYearScrolling={false}
          rightArrowIcon={<KeyboardArrowRight />}
          leftArrowIcon={<KeyboardArrowLeft />}
        />
        {touched &&
          ((error && <span className={classes.error}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    );
  }
}

BridgesDatePicker.propTypes = {
  classes: PropTypes.object.isRequired,
  input: PropTypes.any,
  meta: PropTypes.any,
};

BridgesDatePicker.defaultProps = {
  input: {},
  field: {
    startIcon: '',
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesDatePicker);
