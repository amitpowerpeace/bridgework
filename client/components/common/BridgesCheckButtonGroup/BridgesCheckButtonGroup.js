/**
 * Renders group checkbox component
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
// ICON DEPENDENCIES
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
// EXTERNAL DEPENDENCIES
import renderHTML from 'react-render-html';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  checkboxButtonNormal: {
    color: palette.grey.darkest,
    height: 40,
    float: 'left',
    marginLeft: 0,
    padding: '3px 0',
    '& > span:nth-of-type(1)': {
      marginRight: 0,
      width: 'unset',
      height: 'unset',
      padding: '2px 6px 0 12px',
    },
  },
  checkboxButtonTableLabel: {
    color: palette.grey.darkest,
    height: 40,
    float: 'left',
    marginLeft: 0,
    marginTop: 6,
    marginBottom: 6,
    '& > span:nth-of-type(1)': {
      marginRight: 0,
      width: 'unset',
      height: 'unset',
      padding: '0 7px 0 18px',
    },
  },
  checkboxButtonTableGroup: {
    borderBottom: `1px solid ${palette.grey.dark}`,
  },
  checkboxButtonChecked: {
    background: palette.primary.lighter,
  },
  checkboxLabel: {
    fontSize: size.display3,
    paddingRight: 15,
    color: palette.grey.darkest,
  },
  iconColorChecked: {
    color: palette.notification.success,
  },
  iconColorUnChecked: {
    color: palette.grey.darkest,
  },
  divShortWidth: {
    float: 'left',
    width: 'auto',
    marginBottom: 12,
  },
  bridgesCheckButtonHeading: {
    color: palette.white.base,
    backgroundColor: palette.primary.dark,
    minHeight: 48,
    lineHeight: lineH.lh_48,
    paddingTop: 0,
    paddingRight: 4,
    paddingBottom: 0,
    paddingLeft: 24,
    fontFamily: family.secondary,
    fontSize: size.display3,
    fontWeight: weight.semi,
  },
  showMoreCheckButton: {
    paddingTop: 10,
    paddingBottom: 10,
    textAlign: 'right',
  },
  closeButton: {
    float: 'right',
  },
  closeIcon: {
    color: palette.white.base,
  },
});
const checkedItems = [];
class BridgesCheckButtonGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: props.field.options,
      inputValue: props.input.value || "",
    };
  }
  
  selectedValues = (data) => {
    this.props.getCheckedValues(data)
  }

  componentWillMount(props) {
    this.state.fields.map((o, index) => {
      this.setState({ [o.value]: this.state.inputValue.indexOf(o.value) !== -1 });
    });
  }

  render() {
    const {
      classes,
      input,
      field,
      meta: { touched, error, warning },
    } = this.props;

    return (
      <div className={classes.divFullWidth}>
        {field.viewType === 'table' &&
          field.viewTypeTitle && (
            <Typography
              variant="title"
              className={classes.bridgesCheckButtonHeading}
              gutterBottom={false}
            >
              {field.viewTypeTitle}
              {/* {
              DO NOT DELETE BELOW ICON, SINCE IT WILL BE USED ONCE COMPONENT WORKS FULLFLEDGE DYNAMICALLY
            } */}
              {/* <IconButton className={classes.closeButton} aria-label="Close">
              <Icon className={classes.closeIcon}>{'close'}</Icon>
            </IconButton> */}
            </Typography>
          )}
        <FormControl fullWidth>
          <FormGroup row>
            {field.options.map((o, index) => (
              <div
                key={index}
                className={classNames(
                  field.spreadType === 'vertical' ? classes.divFullWidth : classes.divShortWidth,
                  this.state[`${o.value}`] ? classes.checkboxButtonChecked : null,
                  {
                    [classes.checkboxButtonTableGroup]: field.viewType === 'table',
                  },
                )}
              >
                <FormControlLabel
                  className={classNames({
                    [classes.checkboxButtonNormal]: field.viewType === 'normal',
                    [classes.checkboxButtonTableLabel]: field.viewType === 'table',
                  })}
                  classes={{
                    label: classNames(
                      classes.checkboxLabel,
                      field.compact === true ? classes.checkboxLabelCompact : null,
                    ),
                  }}
                  control={
                    <Checkbox
                      onChange={(event, value) => {
                        if (event.target.checked) {
                          checkedItems.push(o.value);
                        } else {
                          checkedItems.splice(checkedItems.indexOf(o.value), 1);
                        }
                        this.setState({ [o.value]: event.target.checked });
                        const collectData = checkedItems.join(','); 

                        typeof(input.onChange) === 'undefined' && this.selectedValues(collectData);
                        typeof(input.onChange) !== 'undefined' && input.onChange(collectData)
                      }}
                      checked={this.state[`${o.value}`]}
                      value={o.value}
                      icon={<CheckBoxOutlineBlankIcon className={classes.iconColorUnChecked} />}
                      checkedIcon={<CheckBoxIcon className={classes.iconColorChecked} />}
                    />
                  }
                  label={renderHTML(o.label)}
                />
              </div>
            ))}
            {touched &&
              ((error && <span className={classes.error}>{error}</span>) ||
                (warning && <span>{warning}</span>))}
          </FormGroup>
        </FormControl>
        {field.showLimitedTableData && (
          <div className={classNames(classes.divFullWidth, classes.showMoreCheckButton)}>
            <a href="javascript:void(0)" className={classes.showAllFont}>
              Show all Data Collection records
            </a>
          </div>
        )}
      </div>
    );
  }
}

BridgesCheckButtonGroup.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  input: PropTypes.object.isRequired,
  getCheckedValues:  PropTypes.func,
};

BridgesCheckButtonGroup.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesCheckButtonGroup);