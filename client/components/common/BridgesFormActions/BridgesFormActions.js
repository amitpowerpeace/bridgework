/**
 * Renders form action button component
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size } from 'client/styles/base/font';
import { withRouter } from 'react-router-dom';

const styles = {
  squareButton: {
    background: palette.white.base,
    borderRadius: 0,
    border: `1px solid ${palette.primary.lightest}`,
    marginBottom: 0,
    color: palette.primary.lightest,
    height: 60,
    padding: '0 25px 0 25px',
    textTransform: 'Capitalize',
    fontFamily: family.secondary,
    fontSize: size.body2,
    '&:hover': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:active': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:focus': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:disabled': {
      border: `1px solid${palette.grey.lightgrey}`,
      backgroundColor: palette.grey.lightgrey,
      color: palette.grey.dimgrey,
    },
  },
  buttonPositionLeft: {
    float: 'left',
    marginRight: 20,
  },
  buttonPositionRight: {
    float: 'right',
    marginLeft: 20,
  },
  formActionBar: {
    height: 60,
    backgroundColor: 'white',
    bottom: 0,
    boxShadow: '4px 0 5px 0 rgba(213,213,213,0.1), 1px 0 10px 0 rgba(0,0,0,0.05)',
  },
  highlightColor: {
    backgroundColor: palette.primary.lightest,
    color: palette.white.base,
  },
  marginIndividualID: {
    margin: '0px 10px',
  },
};

class BridgesFormActions extends React.Component {
  navigateByUrl(url) {
    this.props.history.push(url);
  }

  /**
   * Render back navigation button
   */
  createBackNavButton(classes, action) {
    return (
      <Button
        onClick={() => this.navigateByUrl(action.url)}
        className={classNames(classes.squareButton, classes.buttonPositionLeft)}
      >
        <ArrowBack /> Back
      </Button>
    );
  }

  renderButtons = (action, classes) => {
    switch (action.type) {
      case 'back':
        return (
          <Button
            onClick={() => this.navigateByUrl(action.url)}
            className={classNames(classes.squareButton, classes.buttonPositionLeft)}
          >
            <ArrowBack /> {action.title}
          </Button>
        );

      case 'next':
        return (
          <Button
            onClick={() => this.navigateByUrl(action.url)}
            className={classNames(
              classes.highlightColor,
              classes.squareButton,
              classes.buttonPositionRight,
            )}
          >
            {action.title} <ArrowForward />
          </Button>
        );

      case 'navigate':
        return (
          <Button
            onClick={() => this.navigateByUrl(action.url)}
            className={classNames(classes.highlightColor, classes.squareButton, {
              [classes.buttonPositionLeft]: action.align === 'left',
              [classes.buttonPositionRight]: action.align === 'right',
            })}
          >
            {action.title}
          </Button>
        );
    }
  };

  render() {
    const { classes, actions } = this.props;
    const formActionWrapper = classNames(classes.formActionBar);
    return (
      <div className={formActionWrapper}>
        {actions.map((action, i) => (
          <React.Fragment key={i}>{this.renderButtons(action, classes)}</React.Fragment>
        ))}
      </div>
    );
  }
}

BridgesFormActions.propTypes = {
  actions: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

BridgesFormActions.defaultProps = {
  actions: [],
};

export default withRouter(withStyles(styles)(BridgesFormActions));
