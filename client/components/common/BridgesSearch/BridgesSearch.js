// REACT DEPENDENCIES
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
// COMPONENT DEPENDENCIES
import TextMaskField from '../BridgesTextMask';
import BridgesToggle from '../BridgesToggle';
import BridgesTextField from '../BridgesTextField';
// ICON DEPENDENCIES
import SearchIcon from '@material-ui/icons/Search';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight, lineH } from 'client/styles/base/font';
import { borderRadius, boxShadow } from 'client/styles/base/custom';

const styles = {
  root: {
    height: 45,
    display: 'flex',
    justifyContent: 'space-between',
    margin: '0 10px 0 0',
    width: 'auto',
    border: 0,
  },
  input: {
    width: '100%',
    fontSize: 14,
    backgroundColor: `${palette.background.lightest}!important`,
    borderRight: `1px solid ${palette.grey.darkest}`,
    borderTop: `1px solid ${palette.grey.darkest}`,
    borderBottom: `1px solid ${palette.grey.darkest}`,
    borderLeft: `1px solid ${palette.grey.darkest}`,
    borderTopLeftRadius: borderRadius.textField,
    borderBottomLeftRadius: borderRadius.textField,
    borderTopRightRadius: borderRadius.textField,
    borderBottomRightRadius: borderRadius.textField,
    height: 42,
    position: 'relative',
    overflow: 'hidden',
    '&:hover': {
      boxShadow: boxShadow.headerSearchHover,
    },
    '& > input': {
      fontFamily: family.secondary,
      fontSize: size.display3,
      color: palette.grey.darkest,
      padding: '14px 12px',
      boxShadow: boxShadow.headerSearchInput,
    },
  },
  selectBoxStyle: {
    height: 42,
    width: '100%',
    borderRadius: borderRadius.selectBoxStyle,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    fontFamily: family.primary,
    fontSize: size.body2,
    color: palette.grey.darkest,
    '& > div > div': {
      paddingLeft: 7,
      paddingRight: 10,
      paddingTop: 1,
      height: `${100}%`,
      padding: 0,
      lineHeight: lineH.lh_36,
      backgroundColor: 'transparent!important',
    },
    '& > div > svg': {
      marginRight: 10,
      fontSize: 25,
    },
  },
  bridgesSelectbox: {
    paddingTop: '0!important',
    paddingBottom: '0!important',
    height: 42,
    minWidth: 55,
    float: 'left',
    borderRight: `1px solid ${palette.grey.darkest}`,
    borderTop: `1px solid ${palette.grey.darkest}`,
    borderBottom: `1px solid ${palette.grey.darkest}`,
    borderLeft: `1px solid ${palette.grey.darkest}`,
    borderTopLeftRadius: borderRadius.textField,
    borderBottomLeftRadius: borderRadius.textField,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    backgroundColor: palette.white.base,
    '& > div > div': {
      color: palette.grey.darkest,
      border: 0,
      paddingTop: 3,
      paddingLeft: 0,
      paddingRight: 0,
      textAlign: 'left',
      fontFamily: family.primary,
      fontSize: size.display2,
      fontWeight: weight.semi,
    },
    '& svg:nth-of-type(1)': {
      color: palette.grey.darkest,
    },
  },
  nullifyPadding: {
    paddingTop: '0!important',
    paddingRight: '0!important',
    paddingLeft: '0!important',
    paddingBottom: '0!important',
  },
  buttonLabelCss: {
    width: '100%',
  },
  searchBoxInput: {
    paddingLeft: 6,
  },
  selectBoxSearchBy: {
    lineHeight: lineH.lh_42,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: family.primary,
    fontSize: size.display2,
    fontWeight: weight.semi,
    color: palette.grey.darkest,
  },
  searchInput: {
    '& input': {
      height: 40,
      width: 180,
      padding: '0px 0 0 12px',
      fontFamily: family.secondary,
      fontSize: size.display2,
      fontWeight: weight.semi,
      color: palette.grey.darkest,
    },
  },
  orBetweenSearch: {
    lineHeight: lineH.lh_42,
    textAlign: 'center',
    fontFamily: family.primary,
    fontSize: size.display2,
    fontWeight: weight.semi,
    color: palette.grey.darkest,
  },
  searchButton: {
    background: palette.primary.lightest,
    borderRadius: 5,
    height: 42,
    padding: '0 11px',
    marginLeft: 20,
    fontFamily: family.secondary,
    fontSize: size.body2,
    color: palette.white.base,
    '& svg': {
      marginTop: 3,
    },
    '&:hover': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:active': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
    '&:focus': {
      background: palette.primary.lightest,
      color: palette.white.base,
    },
  },
  searchByGrid: {
    paddingBottom: 6,
  },
  searchByLabel: {
    fontFamily: family.primary,
    fontSize: size.display2,
    fontWeight: weight.semi,
    color: palette.grey.darkest,
  },
  searchByGridParent: {
    paddingBottom: 10,
  },
  searchIcon: {
    width: 22,
    height: 22,
    paddingRight: 6,
  },
};

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selValue: '',
      toggleValue: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ ...this.state, value: nextProps.value });
    }
  }

  getSearchValue = data => {
    this.setState({ selValue: data.target.value });
  };

  handleSearch = event => {
    this.props.onSearch(this.state.selValue);
  };

  getToggleValue = data => {
    this.setState({ toggleValue: data });
  };

  render() {
    const { value, selValue, toggleValue } = this.state;
    const { classes, disabled, ...inputProps } = this.props;

    return (
      <Grid
        container
        alignItems={'flex-start'}
        direction={'row'}
        justify={'flex-start'}
        className={classes.searchByGridParent}
      >
        <Grid item xs={12} className={classes.searchByGrid}>
          <label htmlFor="searchBy" className={classes.searchByLabel}>
            {'Search By'}
          </label>
        </Grid>

        <Grid item>
          <BridgesToggle
            field={{
              name: 'searchBy',
              toggleStack: [
                {
                  title: 'SSN',
                  value: 'ssn',
                },
                {
                  title: 'Individual ID',
                  value: 'id',
                },
              ],
            }}
            onChangeTrack={this.getToggleValue}
          />
        </Grid>

        <Grid item>
          <Typography component="div" className={classes.searchBoxInput}>
            <BridgesTextField
              field={{
                name: 'searchBy',
                type: 'text',
                startIcon: '',
                label: '',
                value: '',
                validate: [''],
                maskType: (toggleValue === 'ssn' && toggleValue) || '',
              }}
              onChange={this.getSearchValue}
            />
          </Typography>
        </Grid>

        <Grid item>
          <Button
            className={classes.searchButton}
            classes={{
              label: classes.buttonLabelCss,
            }}
            onClick={this.handleSearch}
          >
            <SearchIcon className={classes.searchIcon} />
            {'Search'}
          </Button>
        </Grid>
      </Grid>
    );
  }
}

SearchBar.defaultProps = {
  disabled: false,
  placeholder: 'Search..',
  style: null,
  value: '',
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onKeyUp: PropTypes.func,
  onRequestSearch: PropTypes.func,
  onSearch: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.string, // passes search string to parent
};

export default withStyles(styles)(SearchBar);
