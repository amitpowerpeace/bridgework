/**
 * Renders single radiobutton component
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes, { func } from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
// ICON DEPENDENCIES
import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonChecked from '@material-ui/icons/RadioButtonChecked';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  radioLabelStyle: {
    fontFamily: family.secondary,
    fontWeight: weight.semi,
    fontSize: size.display3,
    color: palette.grey.darkest,
    '& > span:nth-of-type(1)': {
      width: 34,
      height: 'unset',
    },
  },
  radioLabelStyleVert: {
    flexDirection: 'column',
  },
  radioLabelStyleHor: {
    flexDirection: 'row',
  },
  radioSeperation: {
    margin: 0,
  },
  radioLabelCover: {
    height: 35,
    marginTop: 3,
    marginRight: 18,
    marginBottom: 0,
    marginLeft: 0,
    '& > span:nth-of-type(1)': {
      marginRight: 8,
      width: 'unset',
      height: 'unset',
    },
  },
  radioLabelUnChecked: {
    color: palette.grey.darkest,
  },
  radioLabelChecked: {
    color: palette.notification.success,
  },
});

class BridgesRadioGroup extends React.Component {
  constructor() {
    super();
    this.state = {
      radioVal: '',
    };
  }

  handleChange = event => {
    this.setState({ radioVal: event.target.value });
  };

  render() {
    const { classes, input, field } = this.props;
    const { radioVal } = this.state;
    return (
      <FormControl fullWidth={field.spreadType === 'horizontal'} component="fieldset">
        <RadioGroup
          className={classNames(
            field.spreadType === 'vertical' ? classes.radioLabelStyleVert : null,
            field.spreadType === 'horizontal' ? classes.radioLabelStyleHor : null,
            classes.radioSeperation,
          )}
        >
          {field.options.map(o => (
            <FormControlLabel
              key={Math.random()}
              className={classes.radioLabelCover}
              classes={{ label: classes.radioLabelStyle }}
              value={o.value}
              control={
                <Radio
                  icon={<RadioButtonUnchecked className={classes.radioLabelUnChecked} />}
                  checkedIcon={<RadioButtonChecked className={classes.radioLabelChecked} />}
                  onChange={(event, value) => {
                    return input.onChange(o.value);
                  }}
                  checked={o.value === input.value}
                />
              }
              label={o.label}
            />
          ))}
        </RadioGroup>
      </FormControl>
    );
  }
}

BridgesRadioGroup.propTypes = {
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
};

BridgesRadioGroup.defaultProps = {
  input: {},
  field: {
    options: [],
  },
  meta: {},
};

export default withStyles(styles)(BridgesRadioGroup);
