/**
 * Overrides default paper component with theme styles
 * THIS COMPONENT SUPPORT EXISTANCE / ALIGNMENT OF PAPER
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// MATERIAL DEPENDENCIES
import Paper from '@material-ui/core/Paper';
// THEME STYLES
import { borderRadius, boxShadow } from 'client/styles/base/custom';

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 22,
    flexGrow: 1,
    paddingBottom: 40,
    marginBottom: 24,
    marginTop: theme.spacing.unit * 0,
    boxShadow: boxShadow.paper,
  }),
  round: {
    borderRadius: borderRadius.paper,
  },
});

const BridgesPaper = ({ children, classes, elevate, minHeight, roundcorner, ...others }) => {
  return (
    <Paper
      className={classNames(classes.root, {
        [classes.round]: roundcorner,
      })}
      elevation={elevate || 0}
      style={{ minHeight: 0 }}
      {...others}
    >
      {children}
    </Paper>
  );
};

BridgesPaper.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
  elevate: PropTypes.number,
  minHeight: PropTypes.number,
  roundcorner: PropTypes.any,
};

export default withStyles(styles)(BridgesPaper);
