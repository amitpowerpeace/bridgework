// REACT DEPENDENCIES
import React from 'react';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

function NumberFormatCustom(props) {
  const { inputRef, onChange, userDefinedprefix, ...other } = props;

  return (
    <NumberFormat
      {...other}
      ref={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      decimalSeparator={'.'}
      decimalScale={2}
      fixedDecimalScale
      prefix={userDefinedprefix}
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  userDefinedprefix: PropTypes.string,
};

export default NumberFormatCustom;
