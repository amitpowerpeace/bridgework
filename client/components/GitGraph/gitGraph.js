import React from 'react';
import GitGraph from './gitgraph.core'
import { withRouter } from 'react-router-dom';

class GitGraphComponent extends React.Component {

  myTemplateConfig = {
    colors: ["#028DEB", "#A6A6A6"],
    branch: {
      lineWidth: 1,
      spacingX: 20,
      labelRotation: 0
    },
    commit: {
      spacingY: -40,
      dot: {
        size: 4
      },
      message: {
        font: "normal 15px Lato",
        displayAuthor: false,
        displayBranch: false,
        displayHash: false
      }
    }
  };

  myTemplate = new GitGraph.Template(this.myTemplateConfig);

  componentDidMount(){
    this.canvas = this.refs.canvas

    this.config = {
      template: this.myTemplate, // could be: "blackarrow" or "metro" or `myTemplate` (custom Template object)
      reverseArrow: false, // to make arrows point to ancestors, if displayed
      orientation: "vertical",
      initCommitOffsetY: -20,
      canvas: this.canvas
    };

    var gitGraph = new GitGraph(this.config);

    // Create branch named "master"
    var master = gitGraph.branch("master");

    // Commit on HEAD Branch which is "master"
    master.commit({
      message: "Application Type",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/')
      }
    },);
    master.commit({
      message: "Basic Individual Details",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register')
      }
    });
    master.commit({
      message: "Contact Information",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/contact-information')
      }
    });
    master.commit({
      message: "Program Selection",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/program-selection')
      }
    });
    master.commit({
      message: "Household Members",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/house-member')
      }
    });


    // Create a new "dev" branch from "master" with some custom configuration
    var relationships = master.branch({
      name: "Relationship",
      color: "#A6A6A6",
      commitDefaultOptions: {
        color: "#A6A6A6"
      }
    });
    relationships.commit({
      message: "Relationships",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/house-details')
      }
    });

    master.checkout();
    relationships.merge(master, 'Household Details');

    master.commit({
      message: "Assets",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/assets')
      }
    });
    master.commit({
      message: "Income",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/income-details')
      }
    });
    master.commit({
      message: "Expenses",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/expense')
      }
    });
    master.commit({
      message: "Final Details",
      onClick: (commit, isOverCommit, event) => {
        this.navigateByUrl('/register/final-details')
      }
    });


    /***********************
     *       EVENTS        *
     ***********************/

    gitGraph.canvas.addEventListener("graph:render", function (event) {
      console.log(event.data.id, "has been rendered with a scaling factor of", gitGraph.scalingFactor);
    });

    // gitGraph.canvas.addEventListener("commit:mouseover", function (event) {
    //     console.log("You're over a commit.", "Here is a bunch of data ->", event.data);
    //     this.style.cursor = "pointer";
    // });
    //
    // gitGraph.canvas.addEventListener("commit:mouseout", function (event) {
    //     console.log("You just left this commit ->", event.data);
    //     this.style.cursor = "auto";
    // });

  }

  navigateByUrl(url) {
    return this.props.history.push(url);
  }

  render() {
    return (
      <div>
        <canvas id='gitGraph' ref="canvas"></canvas>
      </div>
    );
  }
}

export default withRouter(GitGraphComponent)
