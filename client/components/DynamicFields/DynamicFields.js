/**
 * Generates form fields dynamically from json array
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// MATERIAL DEPENDENCIES
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Divider from '@material-ui/core/Divider';
import Input from '@material-ui/core/Input';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
// REDUX DEPENDENCIES
import { Field } from 'redux-form';
// EXTERNAL DEPENDENCIES
import getFieldComponent from 'client/utils/getFieldComponent';
// TODO: Import all validators
import { required } from 'client/utils/validators';
// THEME STYLES
import formStyles from 'client/styles/common/forms';
import BridgesButton from 'client/components/common/BridgesButton/index';
import { borderRadius } from 'client/styles/base/custom';
import palette from 'client/styles/base/palette';
import { family, size } from 'client/styles/base/font';

const styles = theme => ({
  ...formStyles,
  bridgesDynamicWrapper: {
    paddingLeft: 2,
  },
});

class BridgesDynamicFields extends React.Component {
  /**
   * Reads row information from fields array &
   * creates new array accordingly
   */
  arrangeFields = fields => {
    const rows = [];
    let row = [];
    fields.forEach(field => {
      if (field.grid.row) {
        row = [];
        row.push(field);
        rows.push(row);
      } else {
        row.push(field);
      }
    });
    return rows;
  };

  /**
   * Reads validation string array for form field &
   * applies corresponding validation check
   */
  getValidateRules = (validate = []) => {
    const rules = [];
    if (!validate.length) {
      return [];
    }
    validate.forEach(rule => {
      switch (rule) {
        case 'required':
          rules.push(required);
          break;
      }
    });
    return rules;
  };

  handleDelete = () => {
    console.log('You clicked the delete icon.'); // eslint-disable-line no-alert
  };

  /**
   * Renders Form Field based on field type
   */
  renderField = (field, classes, prefix) => {
    switch (field.type) {
      case 'hr':
        return <Divider className={classes.dividerStyle} />;

      case 'subHeading':
        return (
          <Typography variant="display4" className={classes.dynamicSubHeading}>
            {field.label}
            {field.underline && <span className={classes.headerUnderline} />}
          </Typography>
        );

      case 'readonlyField':
        return (
          <FormControl
            className={classNames({
              [classes.readOnlyInline]: field.inline,
            })}
            fullWidth
          >
            <label htmlFor="name" className={classes.customReadonlyLabel}>
              {field.label}
              {field.inline && ':'}
              {!field.inline &&
                field.validate.includes('required') && (
                  <sup className={classes.supTextStyle}>*</sup>
                )}
            </label>
            <Input
              value={field.value}
              className={classNames(classes.customReadonlyInput)}
              disableUnderline
            />
          </FormControl>
        );

      case 'chip':
        return (
          <FormControl fullWidth>
            {!!field.label && (
              <label htmlFor="chip" className={classes.customLabelData}>
                {field.label} <input type="hidden" />
                {field.validate.includes('required') && (
                  <sup className={classes.supTextStyle}>*</sup>
                )}
              </label>
            )}
            <div>
              {field.options.map(item => (
                <Chip
                  label={item.label}
                  className={classes.chip}
                  onDelete={this.handleDelete}
                  deleteIcon={
                    item && item.headerTagShort ? (
                      <p className={classes.chipColor}>{item.headerTagShort}</p>
                    ) : (
                      <p />
                    )
                  }
                />
              ))}
            </div>
          </FormControl>
        );

      case 'bridgesbutton':
        return <BridgesButton field={field} />;

      default:
        return (
          <FormControl fullWidth>
            {!!field.label && (
              <label htmlFor="default" className={classes.customLabel}>
                <input type="hidden" />
                {field.label}{' '}
                {field.validate.includes('required') && (
                  <sup className={classes.supTextStyle}>*</sup>
                )}
                {field.showMoreOption && (
                  <a href="javascript:void(0)" className={classes.showAllFont}>
                    {' '}
                    Show All{' '}
                  </a>
                )}
              </label>
            )}
            <Field
              name={prefix ? `${prefix}:${field.name}` : field.name}
              component={getFieldComponent(field.type)}
              validate={this.getValidateRules(field.validate)}
              field={field}
            />
          </FormControl>
        );
    }
  };

  render() {
    const { classes, fields, prefix } = this.props;
    const arrangedFieldStack = this.arrangeFields(fields);
    const fieldContainerLength = this.arrangeFields(fields).length;
    return (
      <div className={classNames(classes.root, classes.bridgesDynamicWrapper)}>
        {arrangedFieldStack.map((fields, index) => (
          <Grid
            container
            spacing={24}
            key={index}
            className={classNames({
              [classes.gridContainerGrouped]: fields.filter(o => o.grouped).length > 0,
            })}
          >
            {fields.map((field, i) => (
              <Grid
                item
                md={parseInt(field.grid.col)}
                key={i}
                className={classNames(
                  field.type === 'readonlyField' && field.stringOnly && classes.stringOnly,
                  {
                    [classes.cGrids]: field.type !== 'bridgesbutton',
                    [classes.singleCheckboxHighlight]:
                      field.type === 'singlecheckbox' && field.hightlight,
                    [classes.brdgsBtnResetGridPadding]:
                      field.type === 'bridgesbutton' &&
                      !field.grid.row &&
                      this.arrangeFields(fields).length > 1,
                    [classes.groupedlineBar]: field.grouped && field.grid.row,
                  },
                )}
              >
                {this.renderField(field, classes, prefix)}
              </Grid>
            ))}
          </Grid>
        ))}
      </div>
    );
  }
}

BridgesDynamicFields.propTypes = {
  classes: PropTypes.any,
  fields: PropTypes.array.isRequired,
  prefix: PropTypes.any,
};

export default withStyles(styles)(BridgesDynamicFields);
