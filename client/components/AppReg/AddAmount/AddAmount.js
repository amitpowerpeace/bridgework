/**
 *  ADD AMOUNT COMPONENT
 *
 */
// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
// MATERIAL DEPENDENCIES
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// ICON DEPENDENCIES
import ErrorOutline from '@material-ui/icons/ErrorOutline';
import Add from '@material-ui/icons/Add';
import Close from '@material-ui/icons/Close';
// EXTERNAL DEPENDENCIES
import renderHTML from 'react-render-html';
// COMPONENT DEPENDENCIES
import BridgesTextField from 'client/components/common/BridgesTextField';
import BridgesButton from 'client/components/common/BridgesButton';
// THEME STYLES
import palette from 'client/styles/base/palette';
import { borderRadius, boxShadow } from 'client/styles/base/custom';
import { family, lineH, size, weight } from 'client/styles/base/font';
import formStyles from 'client/styles/common/forms';

const styles = theme => ({
  ...formStyles,
  textFieldInputCover: {
    border: `1px solid ${palette.grey.darkest}`,
    borderRadius: borderRadius.textField,
    height: 42,
    width: 150,
    position: 'relative',
    overflow: 'hidden',
    '&:hover': {
      boxShadow: boxShadow.textfieldCover,
    },
  },
  textFieldInput: {
    borderRadius: borderRadius.textField,
    fontFamily: family.secondary,
    fontSize: size.display3,
    color: palette.grey.darkest,
    padding: '14px 12px',
    boxShadow: boxShadow.textfieldInput,
  },
  textFieldRoot: {
    padding: 0,
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  hightlightTextBox: {
    border: `1px solid ${palette.primary.brand}`,
    '& > div button': {
      color: palette.primary.brand,
    },
  },
  startIconCss: {
    height: '42px',
    marginTop: '5px',
    marginLeft: '8px',
    '& p': {
      color: palette.grey.dark,
      fontSize: size.display3,
    },
  },
  iconButton: {
    width: 25,
    height: 25,
  },
  amountIncrementLabel: {
    marginTop: 6,
    display: 'block',
  },
  addAmountBtn: {
    marginTop: 12,
  },
  clearAmountBtn: {
    marginTop: 12,
  },
  addMoreAmountBtn: {
    marginTop: 18,
  },
  totalAmount: {
    lineHeight: lineH.lh_42,
    fontWeight: weight.semi,
    whiteSpace: 'nowrap',
  },
});

class AddAmount extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentAmount: 0,
    };
  }

  numberWithCommas = numberValue => {
    const partsOfNum = numberValue.toString().split('.');
    partsOfNum[0] = partsOfNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return partsOfNum.join('.');
  };

  getCurrentAmountValue = currVal => {
    this.setState({ currentAmount: currVal });
  };

  addMoreAmount = () => {
    if (this.state.currentAmount !== 0 && this.state.currentAmount.trim().length > 0) {
      this.props.onTrack(this.state.currentAmount);
      this.setState(prevState => ({
        clearOldVal: !prevState.clearOldVal,
      }));
    }
  };

  clearIndex = data => {
    this.props.onClearAmount(data);
  };

  resetAmount = () => {
    this.props.onReset();
  };

  getTotalAmt = () => {
    let amountSumValue = 0;
    this.props.amountStack.map((amount, i) => {
      amountSumValue += parseFloat(amount.value);
      if (i === this.props.amountStack.length - 1) {
        amountSumValue = Math.round(amountSumValue * 100) / 100;
        amountSumValue = this.numberWithCommas(amountSumValue);
      }
    });
    return amountSumValue;
  };

  render() {
    const { classes, onTrack, field, amountStack } = this.props;

    return (
      <FormControl fullWidth>
        <Grid
          container
          spacing={24}
          alignItems={'flex-start'}
          direction={'row'}
          justify={'flex-start'}
        >
          <Grid item xs={3}>
            {amountStack.map((field, i) => (
              <React.Fragment key={i}>
                <label
                  htmlFor={`amount_${i}`}
                  className={classNames(classes.customReadonlyLabel, classes.amountIncrementLabel)}
                >
                  <Typography
                    variant="display2"
                    className={classes.masterHeading}
                    gutterBottom={false}
                  >
                    {`Amount #${i + 1}`}
                  </Typography>
                </label>
                <BridgesTextField
                  field={field}
                  getClearIndex={this.clearIndex}
                  componentindex={i}
                />
              </React.Fragment>
            ))}
            <div>
              <label
                htmlFor="amount"
                className={classNames(classes.customReadonlyLabel, classes.amountIncrementLabel)}
              >
                <Typography
                  variant="display2"
                  className={classes.masterHeading}
                  gutterBottom={false}
                >
                  Amount
                </Typography>
              </label>
              <BridgesTextField
                field={field.assetsRecord && field.assetsRecord[0]}
                onChange={this.getCurrentAmountValue}
              />
            </div>
          </Grid>
        </Grid>
        <Grid
          container
          spacing={24}
          alignItems={'flex-start'}
          direction={'row'}
          justify={'flex-start'}
          className={classes.addMoreAmountBtn}
        >
          <Grid item>
            <BridgesButton
              className={classes.addAmountBtn}
              title="Add Amount Field"
              iconAlign="left"
              buttonType="squarebtn"
              onClick={() => {
                this.addMoreAmount();
              }}
            />
          </Grid>
        </Grid>

        <Grid
          container
          spacing={24}
          alignItems={'flex-start'}
          direction={'row'}
          justify={'flex-start'}
          className={classes.clearAmountBtn}
        >
          <Grid item xs={3}>
            <Typography variant="display2" className={classes.totalAmount} gutterBottom={false}>
              Total Amount: {`$ ${this.getTotalAmt()}`}
            </Typography>
          </Grid>
          <Grid item xs={4}>
            <BridgesButton
              title="Clear Amount"
              iconAlign="left"
              buttonType="squarebtn"
              buttonColor="red"
              onClick={() => {
                this.resetAmount();
              }}
            />
          </Grid>
        </Grid>
      </FormControl>
    );
  }
}

AddAmount.propTypes = {
  amountStack: PropTypes.array,
  classes: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  onClearAmount: PropTypes.func,
  onReset: PropTypes.func,
  onTrack: PropTypes.func,
};

AddAmount.defaultProps = {
  input: {},
  field: [],
  amountStack: [
    {
      assetsRecord: [],
    },
  ],
};

export default withStyles(styles)(AddAmount);
