/*eslint-disable*/
import React from 'react'
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

import Icon from '@material-ui/core/Icon';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import { family, size , weight , lineH } from 'client/styles/base/font';
import { borderRadius } from 'client/styles/base/custom';
import palette from 'client/styles/base/palette';
import formStyles from 'client/styles/common/forms';
import paths from 'client/routes/urls'
import arPaths from 'client/routes/AppReg/urls';
import GitGraph from 'client/components/GitGraph'

let sidebarItems = [
  {
    id: paths.appInfo.id,
    title: paths.appInfo.title,
    url: paths.appInfo.url,
    icon: 'event_note',
    drawer: [],
  },
  {
    id: paths.appReg.id,
    title: paths.appReg.title,
    url: paths.appReg.url,
    icon: 'description',
    drawer: [
      {
        title: arPaths.indvInfo.title,
        url: arPaths.indvInfo.url
      },
      {
        title: arPaths.houseMember.title,
        url: arPaths.houseMember.url
      },
      {
        title: arPaths.houseDetail.title,
        url: arPaths.houseDetail.url
      },
      {
        title: arPaths.assets.title,
        url: arPaths.assets.url
      },
      {
        title: arPaths.incomeDetails.title,
        url: arPaths.incomeDetails.url
      },
      {
        title: arPaths.expense.title,
        url: arPaths.expense.url
      },
      {
        title: arPaths.finalDetails.title,
        url: arPaths.finalDetails.url
      },
      {
        title: arPaths.additionalGroupDetails.title,
        url: arPaths.additionalGroupDetails.url
      },
      {
        title: arPaths.foodAssist.title,
        url: arPaths.foodAssist.url
      },
    ],
  },
  {
    id: paths.frontDesk.id,
    title: paths.frontDesk.title,
    url: paths.frontDesk.url,
    icon: 'event_note',
    drawer: [],
  },
  {
    id: paths.scheduling.id,
    title: paths.scheduling.title,
    url: paths.scheduling.url,
    icon: 'date_range',
    drawer: [],
  },
  {
    id: paths.dataCollect.id,
    title: paths.dataCollect.title,
    url: paths.dataCollect.url,
    icon: 'insert_chart',
    drawer: [],
  },
  {
    id: paths.eligibility.id,
    title: paths.eligibility.title,
    url: paths.eligibility.url,
    icon: 'portrait',
    drawer: [],
  },
  {
    id: paths.fapTime.id,
    title: paths.fapTime.title,
    url: paths.fapTime.url,
    icon: 'restaurant',
    drawer: [],
  },
  {
    id: paths.help.id,
    title: paths.help.title,
    url: paths.help.url,
    icon: 'help',
    drawer: [],
  },
]

const styles = {
  ...formStyles,
  listItem: {
    padding: 0,
  },
  listItemInner: {
    '& h3': {
      paddingLeft: 38,
    },
    '& a': {
      paddingLeft: 0,
      '&:hover': {
        backgroundColor: 'transparent',
      },

    },
    '& li': {
      listStyle: 'circle',
    },
    '& span:nth-of-type(2)': {
      borderLeft: '1px solid' + palette.grey.darkest,
      height: '100%',
      top: '26px',
      width: '1px',
      left: '-27px',
      zIndex: '-100',
    },
  },
  navLinkButton: {
    justifyContent: 'left',
    width: '100%',
    paddingTop: 9,
    paddingBottom: 9,
    paddingRight: 8,
    paddingLeft: 12,
    borderRadius: 0,
    color: palette.grey.darkest,
    fontFamily: family.secondary,
    fontSize: size.display3,
    textTransform: 'capitalize',
    "&:hover": {
      color: palette.primary.lightest,
      backgroundColor: palette.primary.lightcyan,
      "& svg,& $listIcon": {
        color: palette.primary.lightest
      }
    },
    '& svg': {
      padding: 0,
      marginLeft: 'auto',
      color: palette.grey.darkest,
      width: 22,
      height: 22
    }
  },
  activeButton: {
    color: palette.primary.lightest,
    backgroundColor: palette.primary.lightcyan,
    "& svg,& $listIcon": {
      color: palette.primary.lightest
    }
  },
  listIcon: {
    color: palette.grey.dark,
    fontSize: 20,
    padding: 0,
    marginTop: 0,
    marginRight: 12,
    marginBottom: 0,
    marginLeft: 0,
    width: 22,
    height: 22,
    lineHeight: lineH.lh_23
  },
  activeListIcon:{
    color: palette.primary.lightest,
    fontSize: 20,
    padding: 0,
    marginTop: 0,
    marginRight: 12,
    marginBottom: 0,
    marginLeft: 0,
    width: 22,
    height: 22,
    lineHeight: lineH.lh_23
  },
  listTreeIcon: {
    fontSize: 11,
    marginLeft: 12,
    width: 12,
    marginRight: 20,
    color: palette.grey.dark,
  },
  activeListTreeIcon: {
    color: palette.primary.lightest,
    width: 14,
    fontSize: 14,
    marginLeft: 10,
    marginRight: 19,
    paddingLeft: 1
  },
  listItemText: {
    whiteSpace: 'nowrap',
    '& h3': {
      paddingLeft: 20,
      color: palette.grey.dark,
      fontFamily: family.secondary,
      fontSize: size.display2,
      textTransform: 'capitalize',
    },
  },
  nestedMenu: {
    padding: 0,
    '& h3': {
      color: palette.grey.darkest,
      '&:hover': {
        color: palette.primary.brand,
      },
    },
    '& a': {
      '&:hover': {
        color: palette.primary.brand,
      },
    },
  },
  displayBullet: {},
  activeButtonInner: {
    color: palette.primary.lightest,
  },
  dividerStyle: {
    marginBottom: 5,
  },
  headerSideBarTopText: {
    padding: '12px 18px 12px 20px',
  },
  headerApplicationTitle: {
    fontSize: size.display1,
    fontWeight: weight.semi,
    color: palette.grey.darkest
  },
  applicationLabel: {
    fontSize: size.display3,
    fontWeight: weight.semi,
    color: palette.grey.darkest,
    fontFamily: family.secondary
  },
  applicationValue: {
    fontSize: size.display1,
    fontWeight: weight.semi,
    color: palette.grey.darker,
    fontFamily: family.primary
  },
  applicationNameHighlight: {
    color: palette.primary.lightest
  },
  applicationUserRoleHighlight: {
    margin: '0 0 0 10px',
    padding: '5px 12px',
    display: 'inline-block',
    borderRadius: borderRadius.headingTag,
    backgroundColor: palette.primary.lightest,
    color: palette.white.base,
    fontFamily: family.primary,
    lineHeight: lineH.lh_15,
    fontSize: size.body1,
    fontWeight: weight.regular
  },
  statusTextColor: {
    color: palette.notification.pending,
  },
  navLink: {
    justifyContent: 'left',
  }
}

class LeftDrawerItems extends React.Component {
  state = {
    open: false,
  }

  application = {
    id: 'T29999999',
    name: 'John Doe',
    designation: 'HOH',
    statusText: 'Pending',
    statusColor: '#ff9988'
  }

  resetMenuState = data => {
    Object.keys(this.state).forEach((element, i) => {
      data !== element ? (this.state[element] = false) : null
    })
  }

  handleClick = data => {
    let currVal = !this.state[data]
    this.setState({[data]: currVal})
    this.state.open = currVal
  }

  getRegisterApplicationDetails(classes){
    return (
      <Typography className={classes.headerSideBarTopText} component="div">
        <Typography className={classes.headerApplicationTitle} component="p">
          <span className={classes.applicationLabel}>Application No</span>
           {' : '}
           <span className={classes.applicationValue}>{this.application.id}</span>
        </Typography>
        <Typography className={classes.headerApplicationTitle} component="p">
          <span className={classes.applicationLabel}>Status</span>
          {' : '}
          <span className={classNames(classes.applicationValue,classes.statusTextColor)}>{this.application.statusText}</span>
        </Typography>
        <Typography className={classes.headerApplicationTitle} component="p">
          <span className={classes.applicationLabel}>Name</span>
          {' : '}
          <span className={classNames(classes.applicationValue,classes.applicationNameHighlight)}>{this.application.name}</span>
          <span className={classes.applicationUserRoleHighlight}>{this.application.designation}</span>
        </Typography>
      </Typography>
    )
  }

  render () {
    const {classes} = this.props

    return (
      <div className={classes.divFullWidth}>
        {sidebarItems.map((item, index) => (
          <div
            className={classes.divFullWidth}
            key={index}
          >
            <ListItem className={classes.listItem}>
              {item.drawer.length === 0 && (
                <Button
                  component={props => (
                    <NavLink
                      className={classes.navLink}
                      exact
                      activeClassName={classes.activeButton}
                      to={item.url}
                      {...props}
                    />
                  )}
                  className={classNames(classes.button, classes.navLinkButton)}
                >
                  {item.icon && (
                    <Icon className={classes.listIcon}>{item.icon}</Icon>
                  )}
                  {item.title}
                </Button>
              )}

              {item.drawer.length > 0 && (
                <Button
                  classes={{
                    label: classes.divFullWidth
                  }}
                  className={classNames(
                    classes.button,
                    classes.navLinkButton,
                    {
                      [classes.activeButton]: this.state[`${item.title.toLowerCase().replace(/ /g, '-')}`]
                    }
                  )}
                  onClick={() => {
                    this.handleClick(item.title.toLowerCase().replace(/ /g, '-'))
                  }}
                >
                  {item.icon !== '' && (
                    <Icon className={
                      classNames({
                        [classes.listIcon]: !this.state.open,
                        [classes.activeListIcon]: this.state.open
                      })

                    }>{item.icon}</Icon>
                  )}
                  {item.title}
                  {this.state.open && <ExpandLess />}
                  {!this.state.open && <ExpandMore />}
                </Button>
              )}
            </ListItem>
            {!this.state[item.title.toLowerCase().replace(/ /g, '-')] &&
              <Divider />
            }
            <Collapse in={this.state.open} timeout={400} unmountOnExit>
              <List component="div" disablePadding>
                {item.id=== 'application-register' && (
                  <div>
                    {this.getRegisterApplicationDetails(classes)}
                    <Divider />
                    <GitGraph/>
                  </div>

                  )

                }

              </List>
            </Collapse>
          </div>
        ))}
      </div>
    )
  }
}

LeftDrawerItems.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}

export default withStyles(styles, {withTheme: true})(LeftDrawerItems)
