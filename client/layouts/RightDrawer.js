// REACT DEPENDENCIES
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import HelpIcon from '@material-ui/icons/LiveHelp';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// UTILITY DEPENDENCIES
import { makeRequest } from 'client/utils/fetchUtils';
// THEME STYLES
import palette from 'client/styles/base/palette';
import formStyles from 'client/styles/common/forms';
import { family, size, weight, lineH } from 'client/styles/base/font';

const drawerWidth = 210;

const styles = theme => ({
  ...formStyles,
  drawerPaper: {
    position: 'relative',
    height: '100%',
    width: drawerWidth,
  },
  feedListContainerParent: {
    paddingTop: 10,
    paddingBottom: 0,
  },
  feedListContainer: {
    '& > p:nth-of-type(2)': {
      float: 'left',
    },
  },
  feedStatus: {
    width: 8,
    float: 'right',
    marginTop: 4,
    marginRight: 4,
    height: 8,
  },
  feedCurrTime: {
    float: 'right',
    color: palette.grey.darkest,
    fontFamily: family.secondary,
    fontSize: size.body1,
    paddingRight: 10,
  },
  feedListFont: {
    fontFamily: family.secondary,
    fontSize: size.display3,
    color: palette.grey.darkest,
    textAlign: 'left',
    margin: 0,
    paddingLeft: 10,
    paddingRight: 10,
  },
  feedListLink: {
    color: palette.notification.default,
    paddingLeft: 10,
    marginTop: 8,
    paddingRight: 10,
    textDecoration: 'none',
    fontFamily: family.secondary,
    fontSize: size.body2,
    float: 'left',
  },
  helpIconStyle: {
    float: 'left',
    color: palette.primary.lightest,
    marginRight: 0,
    fontSize: 100,
    width: 24,
    height: 24,
  },
  feedTimeStyle: {
    margin: 0,
    position: 'relative',
    paddingTop: 2,
    paddingBottom: 12,
    float: 'right',
  },
  helpPromptsLink: {
    marginTop: 18,
    marginBottom: 42,
    '& > a': {
      margin: '0 auto',
      maxWidth: 125,
      display: 'block',
      minWidth: 122,
      height: 24,
      textDecoration: 'none',
      width: 'auto',
    },
    '& > a > span': {
      float: 'left',
      margin: 0,
      paddingTop: 2,
      fontFamily: family.primary,
      fontSize: size.display2,
      fontWeight: weight.semi,
      textDecoration: 'none',
      paddingLeft: 6,
      color: palette.grey.darkest,
    },
  },
  dividerStyle: {
    marginTop: 13,
    borderTop: 0,
    borderRight: 0,
    borderBottom: '0.2px solid #CDCDCD',
    borderLeft: 0,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  defaultStatusColor: {
    color: palette.notification.default,
  },
  successStatusColor: {
    color: palette.notification.success,
  },
  warningStatusColor: {
    color: palette.notification.warning,
  },
  dangerStatusColor: {
    color: palette.notification.danger,
  },
  documentList: {
    margin: '24px 12px 0',
    paddingLeft: 12,
    float: 'left',
    borderLeft: `3px solid ${palette.primary.lightest}`,
  },
  documentHeader: {
    marginBottom: '-10px',
    fontWeight: weight.semi,
    color: palette.grey.darkest,
  },
  documentLink: {
    fontFamily: family.secondary,
    fontWeight: weight.semi,
    fontSize: size.body2,
    color: palette.primary.lightest,
    backgroundColor: palette.white.base,
    display: 'unset',
    textTransform: 'unset',
    minWidth: 'auto',
    minHeight: 'auto',
    width: '100%',
    float: 'left',
    '&:not(:last-child)': {
      padding: '2px 0',
    },
    '&:last-child': {
      padding: '2px 0 0',
      lineHeight: lineH.lh_17,
    },
    '&:hover': {
      backgroundColor: palette.white.base,
    },
    '&:focus': {
      backgroundColor: palette.white.base,
    },
  },
  formInformationContainer: {
    margin: '24px 12px 0',
    paddingLeft: 12,
    float: 'left',
    borderLeft: `3px solid ${palette.notification.warning}`,
  },
  formInformationText: {
    fontFamily: family.secondary,
    fontWeight: weight.semi,
    fontSize: size.body2,
    color: palette.grey.darkest,
  },
});

class RightDrawer extends React.Component {
  state = {
    open: true,
    statusColor: {},
    feeds: [],
  };

  componentWillMount() {
    makeRequest('mock/feeds').then(data => {
      this.setState({ feeds: data });
    });
  }

  render() {
    const { classes } = this.props;
    const { feeds } = this.state;

    return (
      <Drawer
        variant="permanent"
        anchor="right"
        classes={{
          paper: classNames(classes.drawerPaper),
        }}
        open
      >
        <div className={classes.helpPromptsLink}>
          <a href="/">
            <HelpIcon className={classes.helpIconStyle} />
            <span>{'Smart Help'}</span>
          </a>

          <Divider className={classNames(classes.divFullWidth, classes.dividerStyle)} />

          <Typography component="div" className={classes.documentList}>
            <Typography variant="display3" className={classes.documentHeader}>
              {' '}
              Documents{' '}
            </Typography>
            <Divider className={classNames(classes.divFullWidth, classes.dividerStyle)} />
            <Button disableRipple className={classes.documentLink} component={Link} to="/">
              Self Service Application
            </Button>

            <Button disableRipple className={classes.documentLink} component={Link} to="/">
              Driver’s License
            </Button>

            <Button disableRipple className={classes.documentLink} component={Link} to="/">
              Bank Statement
            </Button>
          </Typography>

          <Typography component="div" className={classes.formInformationContainer}>
            <Typography variant="body1" className={classes.formInformationText}>
              The type of application you select determines the information you are asked to fill
              out.
            </Typography>
          </Typography>
        </div>
      </Drawer>
    );
  }
}

RightDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(RightDrawer);
