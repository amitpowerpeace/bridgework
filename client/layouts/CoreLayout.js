/**
 * Define the page layout
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
// THEME STYLES
import palette from 'client/styles/base/palette';
// COMPONENT DEPENDENCIES
import GlobalHeader from 'client/layouts/GlobalHeader';
import LeftDrawer from 'client/layouts/LeftDrawer';
import RightDrawer from 'client/layouts/RightDrawer';
import NoMatch from 'client/components/NoMatch/';
// PAGES
import AppInfo from 'client/routes/AppInfo';
import AppReg from 'client/routes/AppReg';
import FrontDesk from 'client/routes/FrontDesk';
import Scheduling from 'client/routes/Scheduling';
import DataCollect from 'client/routes/DataCollect';
import Eligibility from 'client/routes/Eligibility';
import FapTime from 'client/routes/FapTime';
import Help from 'client/routes/Help/containers/CounterContainer';
import paths from 'client/routes/urls';

const styles = theme => ({
  root: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    minHeight: '100%',
    flexDirection: 'column',
    overflowX: 'hidden',
  },
  main: {
    zIndex: 1,
    flex: 1,
    position: 'relative',
    display: 'flex',
  },
  content: {
    backgroundColor: palette.background.lightest,
    padding: '24px 24px 0',
    display: 'flex',
    flexGrow: 1,
  },
});

class CoreLayout extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <GlobalHeader />
        <div className={classes.main}>
          <LeftDrawer />
          <div className={classes.content}>
            <Switch>
              <Route path={paths.appInfo.url} exact component={AppInfo} />
              <Route path={paths.appReg.url} component={AppReg} />
              <Route path={paths.frontDesk.url} component={FrontDesk} />
              <Route path={paths.scheduling.url} component={Scheduling} />
              <Route path={paths.dataCollect.url} component={DataCollect} />
              <Route path={paths.eligibility.url} component={Eligibility} />
              <Route path={paths.fapTime.url} component={FapTime} />
              <Route path={paths.help.url} component={Help} />
              <Route component={NoMatch} />
            </Switch>
          </div>
          <RightDrawer />
        </div>
      </div>
    );
  }
}

CoreLayout.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CoreLayout);
