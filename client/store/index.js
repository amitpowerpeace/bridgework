import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from 'client/store/rootReducer';

// Middleware
const middleware = [thunk];

// Store Enhancers
const enhancers = [];
let composeEnhancers = compose;
if (__DEV__) {
  if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  }
}

// Store Instantiation
const store = createStore(
  combineReducers(rootReducer),
  composeEnhancers(applyMiddleware(...middleware), ...enhancers),
);

export default store;

/**
 * Register async loaded reducers in store and replace
 * current state-reducer with the a new reducer
 *
 * @export
 * @param {Object} store - the store object
 * @param {Object} asyncReducer - async reducer modules
 */
store.asyncReducers = {};

function replaceReducers(defaultReducers) {
  const merged = Object.assign({}, defaultReducers, store.asyncReducers);
  const combined = combineReducers(merged);
  store.replaceReducer(combined);
}

export const injectAsyncReducers = asyncReducers => {
  const injectReducers = Object.keys(asyncReducers).reduce((all, item) => {
    if (store.asyncReducers[item]) {
      delete all[item];
    }

    return all;
  }, asyncReducers);

  store.asyncReducers = Object.assign({}, store.asyncReducers, injectReducers);
  replaceReducers(rootReducer);
};

// hot reloading for reducers
if (module.hot) {
  module.hot.accept('./rootReducer', () => {
    const nextReducer = require('./rootReducer').default;
    replaceReducers(nextReducer);
  });
}
