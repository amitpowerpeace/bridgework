import { reducer as formReducer } from 'redux-form';
import loaderReducer from 'client/components/common/BridgesLoader/loaderModule';

export default {
  form: formReducer,
  loader: loaderReducer,
};
