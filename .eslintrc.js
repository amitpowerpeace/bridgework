module.exports = {
  root: true,
  extends: [
    'plugin:import/recommended',
    'airbnb'
  ],
  plugins: [
    "babel",
    "react",
    "promise",
    "jsx-a11y",
    "prettier"
  ],
  env: {
    browser: true,
    jest: true,
    es6: true,
    node: true
  },
  globals: {
    "__DEV__": false,
    "__TEST__": false,
    "__PROD__": false,
    "__COVERAGE__": false
  },
  rules: {
    'linebreak-style': 'off', // Don't play nicely with Windows
    'arrow-body-style': 'off', // Incompatible with prettier
    'arrow-parens': 'off', // Incompatible with prettier
    'object-curly-newline': 'off', // Incompatible with prettier
    'function-paren-newline': 'off', // Incompatible with prettier
    indent: 'off', // Incompatible with prettier
    'implicit-arrow-linebreak': 'off', // Incompatible with prettier
    'space-before-function-paren': 'off', // Incompatible with prettier
    'no-confusing-arrow': 'off', // Incompatible with prettier
    'no-mixed-operators': 'off', // Incompatible with prettier
    'consistent-this': ['error', 'self'],
    'max-len': [
      'error',
      100,
      2,
      {
        ignoreUrls: true,
      },
    ], // airbnb is allowing some edge cases
    'no-console': 'error', // airbnb is using warn
    'prefer-destructuring': 'off', // airbnb is using error. destructuring harm grep potential.
    'no-alert': 'error', // airbnb is using warn
    'no-param-reassign': 'off', // airbnb use error
    'no-prototype-builtins': 'off', // airbnb use error
    'operator-linebreak': 'off', // airbnb use error

    // It would be better to enable this rule, but it might slow us down.
    'import/no-extraneous-dependencies': 'off',
    'import/namespace': ['error', { allowComputed: true }],
    'import/no-unresolved': 'off',
    'import/order': [
      'error',
      {
        groups: [['index', 'sibling', 'parent', 'internal', 'external', 'builtin']],
        'newlines-between': 'never',
      },
    ],

    "react/jsx-no-bind": [2, {}],
    'react/jsx-indent': 'off', // Incompatible with prettier
    'react/jsx-closing-bracket-location': 'off', // Incompatible with prettier
    'react/jsx-wrap-multilines': 'off', // Incompatible with prettier
    'react/jsx-indent-props': 'off', // Incompatible with prettier
    'react/jsx-one-expression-per-line': 'off', // Incompatible with prettier
    'react/jsx-handler-names': [
      'error',
      {
        // airbnb is disabling this rule
        eventHandlerPrefix: 'handle',
        eventHandlerPropPrefix: 'on',
      },
    ],
    'react/jsx-curly-brace-presence': 'off', // airbnb use error, it's buggy
    'react/forbid-prop-types': 'off', // airbnb use error
    'react/require-default-props': 'off', // airbnb use error, it's buggy
    'react/destructuring-assignment': 'off', // airbnb use error
    'react/jsx-filename-extension': ['error', { extensions: ['.js'] }], // airbnb is using .jsx
    'react/no-danger': 'error', // airbnb is using warn
    'react/no-direct-mutation-state': 'error', // airbnb is using off
    'react/no-find-dom-node': 'off', // airbnb use error
    'react/sort-prop-types': 'error', // airbnb use off

    // 'flowtype/define-flow-type': 'error',
    // 'flowtype/require-valid-file-annotation': 'off',
    // 'flowtype/require-parameter-type': 'off',
    // 'flowtype/require-return-type': 'off',
    // 'flowtype/space-after-type-colon': 'off',
    // 'flowtype/space-before-type-colon': 'off',
    // 'flowtype/type-id-match': 'off',
    // 'flowtype/use-flow-type': 'error',

    'prettier/prettier': 'error',

    'jsx-a11y/label-has-associated-control': 'off',
    'jsx-a11y/label-has-for': 'off',
    'jsx-a11y/no-autofocus': 'off', // We are a library, people do what they want.
  },
}
