import React from 'react';

import Button from 'docs/src/button/button';
import Head from 'next/head';
import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Button</title>
      </Head>
      <Button />
    </div>
  );
};

export default withRoot(Page);
