import React from 'react';

import Datepicker from 'docs/src/datepicker/datepicker';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Datepicker</title>
      </Head>
      <Datepicker />
    </div>
  );
};

export default withRoot(Page);
