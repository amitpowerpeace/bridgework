import React from 'react';

import RadioboxGroup from 'docs/src/radioboxgroup/radioboxgroup';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Radiobox Group</title>
      </Head>
      <RadioboxGroup />
    </div>
  );
};

export default withRoot(Page);
