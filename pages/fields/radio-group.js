import React from 'react';

import Radiogroup from 'docs/src/radiogroup/radiogroup';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Radiogroup</title>
      </Head>
      <Radiogroup />
    </div>
  );
};

export default withRoot(Page);
