import React from 'react';

import ToggleField from 'docs/src/toggle/toggle';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Toggle Button</title>
      </Head>
      <ToggleField />
    </div>
  );
};

export default withRoot(Page);
