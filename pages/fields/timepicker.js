import React from 'react';

import Timepicker from 'docs/src/timepicker/timepicker';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Timepicker</title>
      </Head>
      <Timepicker />
    </div>
  );
};

export default withRoot(Page);
