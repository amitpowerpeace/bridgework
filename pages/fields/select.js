import React from 'react';

import SelectField from 'docs/src/select/select';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Selectbox</title>
      </Head>
      <SelectField />
    </div>
  );
};

export default withRoot(Page);
