import React from 'react';

import Actionbar from 'docs/src/action-bar/action-bar';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Action Bar</title>
      </Head>
      <Actionbar />
    </div>
  );
};

export default withRoot(Page);
