import React from 'react';

import CheckboxGroup from 'docs/src/checkboxgroup/checkboxgroup';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Checkbox Group</title>
      </Head>
      <CheckboxGroup />
    </div>
  );
};

export default withRoot(Page);
