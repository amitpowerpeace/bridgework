import React from 'react';

import Checkbox from 'docs/src/checkbox/checkbox';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Checkbox</title>
      </Head>
      <Checkbox />
    </div>
  );
};

export default withRoot(Page);
