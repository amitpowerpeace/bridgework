import React from 'react';

import TextField from 'docs/src/textfield/textfield';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Textfield</title>
      </Head>
      <TextField />
    </div>
  );
};

export default withRoot(Page);
