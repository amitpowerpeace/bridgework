/**
 * Home Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import compose from 'recompose/compose';
import withRoot from '../docs/modules/components/withRoot';

const styles = theme => ({
  root: {
    flex: '1 0 100%',
    backgroundColor: theme.palette.background.paper,
  },
  hero: {
    minHeight: '80vh',
    flex: '0 0 auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.type === 'light' ? theme.palette.primary.dark : theme.palette.primary.main,
  },
  text: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    letterSpacing: '.7rem',
    textIndent: '.7rem',
    fontSize: 20,
    fontWeight: theme.typography.fontWeightRegular,
    [theme.breakpoints.only('xs')]: {
      fontSize: 28,
    },
    whiteSpace: 'nowrap',
  },
  headline: {
    paddingLeft: theme.spacing.unit * 4,
    paddingRight: theme.spacing.unit * 4,
    marginTop: theme.spacing.unit,
    maxWidth: 400,
    textAlign: 'center',
    fontSize: 15,
    letterSpacing: '.1rem',
    color: '#9c9c9c',
  },
  contentHome: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: theme.spacing.unit * 8,
    paddingTop: theme.spacing.unit * 8,
    [theme.breakpoints.up('sm')]: {
      paddingTop: theme.spacing.unit * 12,
    },
  },
  button: {
    marginTop: theme.spacing.unit * 3,
  },
  logo: {
    maxWidth: 192,
    margin: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 4}px`,
  },
  steps: {
    maxWidth: theme.spacing.unit * 130,
    margin: 'auto',
  },
  step: {
    padding: `${theme.spacing.unit * 3}px ${theme.spacing.unit * 2}px`,
  },
  stepIcon: {
    marginBottom: theme.spacing.unit,
  },
  markdownElement: {},
});

class HomePage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Head>
          <title>Bridges Docs</title>
        </Head>
        <div className={classes.hero}>
          <div className={classes.contentHome}>
            <img alt="Bridges Logo" className={classes.logo} src="/static/img/logo.png" />
            <div className={classes.text}>
              <Typography
                variant="display2"
                align="center"
                component="h1"
                color="inherit"
                gutterBottom
                className={classes.title}
              >
                {'BRIDGES DOCS'}
              </Typography>
              <Typography component="h2" color="inherit" gutterBottom className={classes.headline}>
                {'Architecture, Style Guide and Component Documentation of Bridges Application.'}
              </Typography>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withRoot,
  withStyles(styles),
)(HomePage);
