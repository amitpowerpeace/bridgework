/**
 * Data Collect Component
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import withRoot from 'docs/modules/components/withRoot';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
import Head from 'next/head';
import Inspector from 'react-inspector';
import packageJson from 'root/package.json';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
  paper: {
    maxWidth: 700,
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 2,
  },
});

class Packages extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.divFullWidth}>
        <Head>
          <title>Packages</title>
        </Head>
        <Grid container className={classes.content}>
          <Grid item xs={12}>
            <Typography variant="headline" className={classes.componentName} gutterBottom>
              Packages
            </Typography>
          </Grid>

          <Paper className={classes.paper}>
            <Grid item spacing={16} xs={12}>
              <Typography variant="subheading" gutterBottom>
                Dependencies
              </Typography>
              <Inspector theme={'chromeLight'} expandLevel={1} data={packageJson.dependencies} />
            </Grid>
          </Paper>

          <Paper className={classes.paper}>
            <Grid item spacing={16} xs={12}>
              <Typography variant="subheading" gutterBottom>
                Developer Dependencies
              </Typography>
              <Inspector theme={'chromeLight'} expandLevel={1} data={packageJson.devDependencies} />
            </Grid>
          </Paper>
        </Grid>
      </div>
    );
  }
}

Packages.propTypes = {
  classes: PropTypes.object.isRequired,
};

Packages.contextTypes = {
  activePage: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRoot(withStyles(styles)(Packages));
