/**
 * Data Collect Component
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import StarIcon from '@material-ui/icons/Star';
import withRoot from 'docs/modules/components/withRoot';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
import Head from 'next/head';

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
});

class Architecture extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.divFullWidth}>
        <Head>
          <title>Bridges Docs: Overview</title>
        </Head>
        <Grid container className={classes.content}>
          <Grid item xs={12}>
            <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
              Architecture
            </Typography>
          </Grid>
          <img height="800" alt="architecture" src="/static/img/architecture.png" />

          <List>
            <ListItem>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText
                primary="Backend has REST based architecture where server is stateless and provides
                data is json format."
              />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText
                primary="NodeJS server is used as a middleware between
               web client and backend API server."
              />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText primary="Redis is used by the middleware for storage, cache and as a message broker." />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText primary="Frontend is a SPA. React is used as frontend client." />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText primary="Redux pattern has been used to maintain state in frontend." />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <StarIcon />
              </ListItemIcon>
              <ListItemText
                primary="Client requests data from node server which in turn fetches data from the backend,
              parses it in the required format and returns the view."
              />
            </ListItem>
          </List>
        </Grid>
      </div>
    );
  }
}

Architecture.propTypes = {
  classes: PropTypes.object.isRequired,
};

Architecture.contextTypes = {
  activePage: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRoot(withStyles(styles)(Architecture));
