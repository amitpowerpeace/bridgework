/**
 * Data Collect Component
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import withRoot from 'docs/modules/components/withRoot';
// THEME STYLES
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
import Head from 'next/head';
import MarkdownElement from 'docs/modules/components/MarkdownElement';

const readmeFile = preval`module.exports = require('fs').readFileSync(require.resolve('../../README.md'), 'utf8')`;

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
  paper: {
    maxWidth: 700,
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 2,
  },
});

class Readme extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.divFullWidth}>
        <Head>
          <title>Readme</title>
        </Head>
        <Grid container className={classes.content}>
          <Grid item spacing={16} xs={12}>
            <MarkdownElement className={classes.markdownElement} key={'readme'} text={readmeFile} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

Readme.propTypes = {
  classes: PropTypes.object.isRequired,
};

Readme.contextTypes = {
  activePage: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRoot(withStyles(styles)(Readme));
