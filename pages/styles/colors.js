/**
 * Data Collect Component
 */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import { copytoClipboard } from 'docs/helper/customScript';
// THEMES STYLES
import palette from 'client/styles/base/palette';
import { weight } from 'client/styles/base/font';
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';
import withRoot from 'docs/modules/components/withRoot';
import Head from 'next/head';

const colors = [
  {
    name: 'Primary',
    types: [
      {
        name: 'Dark',
        code: '#112E51',
        color: 'blueDark',
      },
      {
        name: 'Brand',
        code: '#0071BC',
        color: 'blueBrand',
      },
      {
        name: 'Light',
        code: '#02BFE7',
        color: 'blueLight',
      },
      {
        name: 'Lighter',
        code: '#D9FAFF',
        color: 'blueLighter',
      },
    ],
  },
  {
    name: 'Notification',
    types: [
      {
        name: 'Green',
        code: '#4AA564',
        color: 'green',
      },
      {
        name: 'Orange',
        code: '#ED7626',
        color: 'orange',
      },
      {
        name: 'Purple',
        code: '#882AE9',
        color: 'purple',
      },
      {
        name: 'Red',
        code: '#E31C3D',
        color: 'red',
      },
    ],
  },
  {
    name: 'Greyscale',
    types: [
      {
        name: 'Black',
        code: '#555555',
        color: 'blackDarkest',
      },
      {
        name: 'Darker',
        code: '#191919',
        color: 'blackDarker',
      },
      {
        name: 'Dark',
        code: '#A6A6A6',
        color: 'blackDark',
      },
      {
        name: 'Semidark',
        code: '#6F6F6F',
        color: 'blackSemidark',
      },
      {
        name: 'Light',
        code: '#E4E4E4',
        color: 'blackLight',
      },
      {
        name: 'Lighter',
        code: '#F2F2F2',
        color: 'blackLightest',
      },
    ],
  },
  {
    name: 'Background',
    types: [
      {
        name: 'Background dark',
        code: '#F4F8F9',
        color: 'backgroundDark',
      },
      {
        name: 'Background light',
        code: '#D9FAFF',
        color: 'backgroundLight',
      },
      {
        name: 'Background lighter',
        code: '#F4F8F9',
        color: 'backgroundLighter',
      },
      {
        name: 'Background lightest',
        code: '#F0F0F0',
        color: 'backgroundLightest',
      },
    ],
  },
];

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  colorGrid: {
    // maxWidth: 180,
    height: 100,
    marginBottom: 90,
  },
  paper: {
    cursor: 'pointer',
    padding: theme.spacing.unit * 2,
    height: '100%',
    color: theme.palette.text.secondary,
  },
  subHeading: {
    marginBottom: 30,
  },
  paletteGroup: {
    marginBottom: 100,
  },
  paletteTitle: {
    fontWeight: weight.bold,
    paddingTop: 10,
    paddingBottom: 10,
  },
  paletteHexColor: {
    fontWeight: weight.bold,
  },
  blueDark: {
    backgroundColor: palette.primary.dark,
  },
  blueBrand: {
    backgroundColor: palette.primary.brand,
  },
  blueLight: {
    backgroundColor: palette.primary.light,
  },
  blueLighter: {
    backgroundColor: palette.primary.lighter,
  },
  blackDarkest: {
    backgroundColor: palette.grey.darkest,
  },
  blackDarker: {
    backgroundColor: palette.grey.darker,
  },
  blackDark: {
    backgroundColor: palette.grey.dark,
  },
  blackSemidark: {
    backgroundColor: palette.grey.semidark,
  },
  blackLight: {
    backgroundColor: palette.grey.light,
  },
  blackLightest: {
    backgroundColor: palette.grey.lightest,
  },
  green: {
    backgroundColor: palette.notification.success,
  },
  orange: {
    backgroundColor: palette.notification.warning,
  },
  purple: {
    backgroundColor: palette.notification.default,
  },
  red: {
    backgroundColor: palette.notification.danger,
  },
  backgroundDark: {
    backgroundColor: palette.background.dark,
  },
  backgroundLight: {
    backgroundColor: palette.background.light,
  },
  backgroundLighter: {
    backgroundColor: palette.background.lighter,
  },
  backgroundLightest: {
    backgroundColor: palette.background.lightest,
  },
});

class BridgesColor extends React.Component {
  state = {
    open: false,
    colorCode: null,
  };

  handleColorClick = colorCode => {
    copytoClipboard(colorCode);
    this.setState({
      colorCode: colorCode,
      open: true,
    });
  };

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  getPalettecolor = (data, classes) => {
    let colorClass = null;
    switch (data) {
      case 'blueDark':
        colorClass = classes.blueDark;
        break;
      case 'blueBrand':
        colorClass = classes.blueBrand;
        break;
      case 'blueLight':
        colorClass = classes.blueLight;
        break;
      case 'blueLighter':
        colorClass = classes.blueLighter;
        break;
      case 'blackDarkest':
        colorClass = classes.blackDarkest;
        break;
      case 'blackDarker':
        colorClass = classes.blackDarker;
        break;
      case 'blackDark':
        colorClass = classes.blackDark;
        break;
      case 'blackSemidark':
        colorClass = classes.blackSemidark;
        break;
      case 'blackLight':
        colorClass = classes.blackLight;
        break;
      case 'blackLightest':
        colorClass = classes.blackLightest;
        break;
      case 'green':
        colorClass = classes.green;
        break;
      case 'orange':
        colorClass = classes.orange;
        break;
      case 'purple':
        colorClass = classes.purple;
        break;
      case 'red':
        colorClass = classes.red;
        break;
      case 'backgroundDark':
        colorClass = classes.backgroundDark;
        break;
      case 'backgroundLight':
        colorClass = classes.backgroundLight;
        break;
      case 'backgroundLighter':
        colorClass = classes.backgroundLighter;
        break;

      case 'backgroundLightest':
        colorClass = classes.backgroundLightest;
        break;
    }
    return colorClass;
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.divFullWidth}>
        <Head>
          <title>Bridges Docs: Colors</title>
        </Head>
        <Grid container className={classes.content}>
          <Grid item xs={12}>
            <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
              Colors
            </Typography>
          </Grid>

          {colors.map((color, index) => (
            <Grid container className={classes.paletteGroup} key={Math.random()}>
              <Typography component="div" className={classes.divFullWidth}>
                <Typography variant="display4" gutterBottom={false} className={classes.subHeading}>
                  {color.name}
                </Typography>
              </Typography>
              <Grid item xs={12}>
                <Grid container spacing={16}>
                  {color.types.map((type, index) => (
                    <Grid
                      item
                      xs={12}
                      sm={3}
                      md={2}
                      lg={2}
                      className={classes.colorGrid}
                      key={Math.random()}
                    >
                      <Paper
                        onClick={() => this.handleColorClick(type.code)}
                        className={classNames(
                          classes.paper,
                          this.getPalettecolor(type.color, classes),
                        )}
                      />
                      <Typography
                        variant="display1"
                        align="center"
                        gutterBottom={false}
                        className={classes.paletteTitle}
                      >
                        {type.name}
                      </Typography>

                      <Typography
                        variant="display1"
                        align="center"
                        gutterBottom={false}
                        className={classes.paletteHexColor}
                      >
                        {type.code}
                      </Typography>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          ))}

          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            open={this.state.open && this.state.colorCode}
            autoHideDuration={2000}
            onClose={this.handleClose}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.state.colorCode} copied!</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                className={classes.close}
                onClick={this.handleClose}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />
        </Grid>
      </div>
    );
  }
}

BridgesColor.propTypes = {
  classes: PropTypes.object.isRequired,
};

BridgesColor.contextTypes = {
  activePage: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRoot(withStyles(styles)(BridgesColor));
