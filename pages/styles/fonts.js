/**
 * Bridges fonts
 *
 */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
// MATERIAL DEPENDENCIES
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// THEMES STYLES
import palette from 'client/styles/base/palette';
import { family, size, weight } from 'client/styles/base/font';
import DocLayoutCss from 'docs/styles/base/docLayout';
import DocPagesCss from 'docs/styles/base/docPages';

import withRoot from 'docs/modules/components/withRoot';
import Head from 'next/head';

const fonts = {
  montserrat: [
    {
      grid: 5,
      variant: 'headline',
      tagname: 'H1',
      weight: 'bold',
      types: [
        {
          text: 'H1_B32/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'H1_B32/Greyscale_black',
          color: 'blackDarkest',
        },
        {
          text: 'H1_B32/Greyscale_dark',
          color: 'blackDark',
        },
      ],
    },
    {
      grid: 3,
      variant: 'display4',
      tagname: 'H1',
      weight: 'medium',
      types: [
        {
          text: 'H1_M18/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'H1_M18/Greyscale_black',
          color: 'blackDarkest',
        },
      ],
    },
    {
      grid: 4,
      variant: 'title',
      tagname: 'H2',
      weight: 'regular',
      types: [
        {
          text: 'H2_R24/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'H2_R24/Greyscale_black',
          color: 'blackDarkest',
        },
      ],
    },
    {
      grid: 5,
      variant: 'display3',
      tagname: 'H1',
      weight: 'bold',
      types: [
        {
          text: 'H1_B16/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'H1_B16/Greyscale_black',
          color: 'blackDarkest',
        },
      ],
    },
    {
      grid: 3,
      variant: 'display2',
      tagname: 'H1',
      weight: 'medium',
      types: [
        {
          text: 'H1_R14/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'H1_R14/Greyscale_brand',
          color: 'blueBrand',
        },
        {
          text: 'H1_R14/Greyscale_black',
          color: 'blackDarkest',
        },
      ],
    },
    {
      grid: 4,
      variant: 'display1',
      tagname: 'H1',
      weight: 'regular',
      types: [
        {
          text: 'H1_B14/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'H1_B14/Primary_brand',
          color: 'blackDarkest',
        },
        {
          text: 'H1_B14/Greyscale_black',
          color: 'blackDark',
        },
      ],
    },
  ],
  lato: [
    {
      grid: 5,
      variant: 'body2',
      tagname: 'body',
      weight: 'bold',
      types: [
        {
          text: 'Body_R14/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'Body_R14/Primary_brand',
          color: 'blueBrand',
        },
        {
          text: 'Body_R14/Primary_light',
          color: 'blueLight',
        },
        {
          text: 'Body_R14/Greyscale_black',
          color: 'blackDarkest',
        },
        {
          text: 'Body_R14/Greyscale_dark',
          color: 'blackLight',
        },
      ],
    },
    {
      grid: 4,
      variant: 'body1',
      tagname: 'Body small',
      weight: 'medium',
      types: [
        {
          text: 'Body_R12/Primary_dark',
          color: 'blueDark',
        },
        {
          text: 'Body_R12/Primary_brand',
          color: 'blueBrand',
        },
        {
          text: 'Body_R12/Primary_light',
          color: 'blueLight',
        },
        {
          text: 'Body_R12/Greyscale_black',
          color: 'blackDarkest',
        },
        {
          text: 'Body_R12/Greyscale_dark',
          color: 'blackLight',
        },
      ],
    },
  ],
};

const styles = theme => ({
  ...DocLayoutCss,
  ...DocPagesCss,
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  fontGrid: {
    marginBottom: 60,
  },
  fontHeading: {
    width: '100%',
    marginBottom: 30,
  },
  fontWeight: {
    width: '100%',
    marginBottom: 30,
  },
  fontVariant: {
    width: '100%',
    marginBottom: 30,
    color: palette.notification.success,
  },
  montserrat: {
    fontFamily: family.primary,
  },
  lato: {
    fontFamily: family.secondary,
  },
  blueDark: {
    color: palette.primary.dark,
  },
  blueBrand: {
    color: palette.primary.brand,
  },
  blueLight: {
    color: palette.primary.light,
  },
  blueLighter: {
    color: palette.primary.lighter,
  },
  blackDarkest: {
    color: palette.grey.darkest,
  },
  blackDarker: {
    color: palette.grey.darker,
  },
  blackDark: {
    color: palette.grey.dark,
  },
  blackSemidark: {
    color: palette.grey.semidark,
  },
  blackLight: {
    color: palette.grey.light,
  },
  blackLighter: {
    color: palette.grey.lighter,
  },
  subHeading: {
    fontWeight: weight.bold,
  },
  fontBold: {
    fontWeight: weight.bold,
    lineHeight: '32px',
  },
  fontMedium: {
    fontWeight: weight.medium,
    lineHeight: '34px',
  },
  fontRegular: {
    fontWeight: weight.semi,
    lineHeight: '32px',
  },
  fontTagLg: {
    fontFamily: family.primary,
    fontWeight: weight.bold,
    fontSize: size.display1,
    color: palette.primary.light,
    float: 'left',
  },
  fontTagSm: {
    fontFamily: family.primary,
    fontWeight: weight.bold,
    fontSize: size.body1,
    color: palette.primary.light,
    float: 'left',
  },
  fontGroup: {
    padding: '0px 10px',
    float: 'left',
  },
  eleName: {
    fontFamily: family.primary,
    fontWeight: weight.bold,
    fontSize: size.body1,
    color: palette.primary.light,
  },
  eleDetails: {
    float: 'left',
  },
});

class BridgesFont extends React.Component {
  getFontcolor = (data, classes) => {
    let colorClass = null;
    switch (data) {
      case 'blueDark':
        colorClass = classes.blueDark;
        break;
      case 'blueBrand':
        colorClass = classes.blueBrand;
        break;
      case 'blueLight':
        colorClass = classes.blueLight;
        break;
      case 'blueLighter':
        colorClass = classes.blueLighter;
        break;
      case 'blackDarkest':
        colorClass = classes.blackDarkest;
        break;
      case 'blackDarker':
        colorClass = classes.blackDarker;
        break;
      case 'blackDark':
        colorClass = classes.blackDark;
        break;
      case 'blackSemidark':
        colorClass = classes.blackSemidark;
        break;
      case ' blackLight':
        colorClass = classes.blackLight;
        break;
      case ' blackLighter':
        colorClass = classes.blackLighter;
        break;
    }
    return colorClass;
  };

  getFontweight = (data, classes) => {
    let weightClass = null;
    switch (data) {
      case 'bold':
        weightClass = classes.fontBold;
        break;
      case 'medium':
        weightClass = classes.fontMedium;
        break;
      case 'regular':
        weightClass = classes.fontRegular;
        break;
    }
    return weightClass;
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.divFullWidth}>
        <Head>
          <title>Bridges Docs: Fonts</title>
        </Head>
        <Grid container className={classes.content}>
          <Grid item xs={12}>
            <Typography variant="headline" className={classes.componentName} gutterBottom={false}>
              Fonts
            </Typography>
          </Grid>

          <Grid container>
            <Typography component="div">
              <Typography
                variant="display1"
                className={classNames(classes.subHeading, classes.fontHeading)}
                gutterBottom={false}
              >
                {family.primary}
              </Typography>
            </Typography>
            <Grid item xs={12}>
              <Grid container>
                {fonts.montserrat.map((font, index) => (
                  <Grid
                    item
                    xs={12}
                    sm={12}
                    md={font.grid}
                    lg={font.grid}
                    className={classes.fontGrid}
                    key={Math.random()}
                  >
                    <Typography
                      variant="display1"
                      className={classNames(classes.subHeading, classes.fontWeight)}
                      gutterBottom={false}
                    >
                      {index < 3 && font.weight}
                    </Typography>

                    <Typography
                      variant="title"
                      className={classNames(classes.subHeading, classes.fontVariant)}
                      gutterBottom={false}
                    >
                      {font.variant}
                    </Typography>

                    <div>
                      <div
                        className={classNames(
                          this.getFontweight(font.weight, classes),
                          classes.fontTagLg,
                        )}
                      >
                        {font.tagname !== '' && font.tagname}
                      </div>
                      <div className={classes.fontGroup}>
                        {font.types.map((type, index) => (
                          <Typography
                            key={Math.random()}
                            variant={font.variant}
                            className={classNames(
                              this.getFontweight(font.weight, classes),
                              classes.blueDark,
                            )}
                            gutterBottom={false}
                          >
                            <span className={this.getFontcolor(type.color, classes)}>
                              {type.text}
                            </span>
                          </Typography>
                        ))}
                      </div>
                    </div>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>

          <Grid container>
            <Typography component="div">
              <Typography
                variant="display1"
                className={classNames(classes.subHeading, classes.fontHeading)}
                gutterBottom={false}
              >
                {family.secondary}
              </Typography>
            </Typography>
            <Grid item xs={12}>
              <Grid container>
                {fonts.lato.map((font, index) => (
                  <Grid
                    item
                    xs={12}
                    sm={12}
                    md={font.grid}
                    lg={font.grid}
                    className={classes.fontGrid}
                    key={Math.random()}
                  >
                    <Typography
                      variant="title"
                      className={classNames(classes.subHeading, classes.fontVariant)}
                      gutterBottom={false}
                    >
                      {font.variant}
                    </Typography>
                    <div>
                      <div
                        className={classNames(
                          this.getFontweight(font.weight, classes),
                          classes.fontTagSm,
                        )}
                      >
                        {font.tagname !== '' && font.tagname}
                      </div>
                      <div className={classes.fontGroup}>
                        {font.types.map((type, index) => (
                          <Typography
                            key={Math.random()}
                            variant={font.variant}
                            className={classNames(
                              this.getFontweight(font.weight, classes),
                              classes.blueDark,
                            )}
                            gutterBottom={false}
                          >
                            <span className={this.getFontcolor(type.color, classes)}>
                              {type.text}
                            </span>
                          </Typography>
                        ))}
                      </div>
                    </div>
                  </Grid>
                ))}
                <Grid xs={12} sm={12} md={3} lg={3} className={classes.fontGrid}>
                  <div className={classNames(classes.divFullWidth)}>
                    <div className={classNames(classes.eleName, classes.fontTagSm)}>{'Footer'}</div>
                    <div className={classNames(classes.eleDetails, classes.divFullWidth)}>
                      <Typography
                        variant={'body2'}
                        className={classNames(classes.blueDark)}
                        gutterBottom={false}
                      >
                        <span className={classes.blackDark}>{'Lato Light 12px'}</span>
                      </Typography>
                    </div>
                  </div>
                  <div className={classNames(classes.divFullWidth)}>
                    <div className={classNames(classes.eleName, classes.fontTagSm)}>{'Link'}</div>
                    <div className={classNames(classes.eleDetails, classes.divFullWidth)}>
                      <Typography
                        variant={'body2'}
                        className={classNames(classes.blueDark)}
                        gutterBottom={false}
                      >
                        <span className={classes.blueBrand}>{'Lato #01BBF0, variable size'}</span>
                      </Typography>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

BridgesFont.propTypes = {
  classes: PropTypes.object.isRequired,
};

BridgesFont.contextTypes = {
  activePage: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

export default withRoot(withStyles(styles)(BridgesFont));
