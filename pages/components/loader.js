import React from 'react';

import Loader from 'docs/src/loader/loader';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Loader</title>
      </Head>
      <Loader />
    </div>
  );
};

export default withRoot(Page);
