import React from 'react';

import DynamicField from 'docs/src/dynamic-field/dynamic-field';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Dynamic field</title>
      </Head>
      <DynamicField />
    </div>
  );
};

export default withRoot(Page);
