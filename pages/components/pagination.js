import React from 'react';

import Pagination from 'docs/src/pagination/pagination';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Pagination</title>
      </Head>
      <Pagination />
    </div>
  );
};

export default withRoot(Page);
