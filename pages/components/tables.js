import React from 'react';

import Tables from 'docs/src/table/table';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Table</title>
      </Head>
      <Tables />
    </div>
  );
};

export default withRoot(Page);
