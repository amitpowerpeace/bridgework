import React from 'react';

import Tabs from 'docs/src/tabs/tabs';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: tabs</title>
      </Head>
      <Tabs />
    </div>
  );
};

export default withRoot(Page);
