import React from 'react';

import BridgesPapeComp from 'docs/src/paper/paper';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: paper</title>
      </Head>
      <BridgesPapeComp />
    </div>
  );
};

export default withRoot(Page);
