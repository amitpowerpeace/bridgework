import React from 'react';

import Typography from 'docs/src/typography/typography';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: Typography</title>
      </Head>
      <Typography />
    </div>
  );
};

export default withRoot(Page);
