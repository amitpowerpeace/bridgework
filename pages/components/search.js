import React from 'react';

import BridgesSearchComp from 'docs/src/search/search';
import Head from 'next/head';

import withRoot from 'docs/modules/components/withRoot';

const Page = () => {
  return (
    <div style={{ width: '100%' }}>
      <Head>
        <title>Bridges Docs: search</title>
      </Head>
      <BridgesSearchComp />
    </div>
  );
};

export default withRoot(Page);
